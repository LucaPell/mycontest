﻿using System;
using System.Collections.Generic;
using System.IO;
using Amazon.DynamoDBv2.DataModel;
using DynamoDBTest.ViewModels;

namespace DynamoDBTest.Models
{
    class Comment : AbstractSortableByDateItem
    {
        [DynamoDBHashKey]
        public string Id { get; set; }
        public string CreatorUser { get; set; }
        public string CreatorUserName { get; set; }
        public string Content { get; set; }
        [DynamoDBGlobalSecondaryIndexHashKey]
        public string ItemCommented { get; set; }
        public string ItemCommentedType { get; set; }
        public List<string> Votes { get; set; }
        [DynamoDBIgnore]
        public string Avatar { get; set; }

        public Comment()
        {
            Votes = new List<string>();
        }
        public Comment(string content, User user, string itemId, string itemType) : this()
        {
            base.SortingDate = DateTime.Now;
            Content = content;
            CreatorUser = user.Email;
            CreatorUserName = user.Username;
            Id = itemId + SortingDate + new Random().Next().ToString();
            ItemCommented = itemId;
            ItemCommentedType = itemType;
        }
    }
}
