﻿using System;
using System.Collections.Generic;
using System.IO;
using Amazon.DynamoDBv2.DataModel;
using DynamoDBTest.ViewModels;

namespace DynamoDBTest.Models
{
    public class Share : AbstractSortableByDateItem
    {
        [DynamoDBHashKey]
        public string Id { get; set; }
        //chi condivide il contenuto
        [DynamoDBGlobalSecondaryIndexHashKey]
        public string CreatorUser { get; set; }
        public string CreatorUserName { get; set; }
        //creatore del contenuto
        public string OriginalUser { get; set; }
        public string OriginalUserName { get; set; }
        public DateTime OriginalDate { get; set; }
        [DynamoDBIgnore]
        public string Media { get; set; }
        public List<string> Votes { get; set; }
        public string Description { get; set; }
        public List<string> Comments { get; set; }
        public string PartecipationId { get; set; }
        public string ContestId { get; set; }
        public string OriginalContent { get; set; }
        public bool MultipleShares { get; set; }
        public DateTime ContestEndDate { get; set; }
        public string ContestName { get; set; }
        public int Shares { get; set; }
        public string Avatar { get; set; }
        public string OriginalAvatar { get; set; }

        public Share()
        {
            Comments = new List<string>();
            Votes = new List<string>();
            MultipleShares = false;

        }

        public Share(string description, User creatorUser, Contest contest) : this()
        {
            base.SortingDate = DateTime.Now;
            Description = description;
            CreatorUser = creatorUser.Email;
            CreatorUserName = creatorUser.Username;
            ContestId = contest.Id;
            ContestName = contest.Name;
            ContestEndDate = contest.EndDate;
            Media = contest.Banner;
            //creatore del contest
            OriginalUser = contest.CreatorUser;
            OriginalUserName = contest.CreatorUserName;
            OriginalContent = contest.Description;
            OriginalDate = contest.SortingDate;
            Id = CreatorUser + ContestId + DateTime.Now.ToString() + (new Random().Next().ToString());
            Shares = contest.Shares;
        }

        public Share(string description, User creatorUser, Partecipation partecipation) : this()
        {

            base.SortingDate = DateTime.Now;
            Description = description;
            CreatorUser = creatorUser.Email;
            CreatorUserName = creatorUser.Username;
            PartecipationId = partecipation.Id;
            Media = partecipation.Media;
            //creatore della partecipazione
            OriginalUser = partecipation.CreatorUser;
            OriginalUserName = partecipation.CreatorUserName;
            OriginalDate = partecipation.SortingDate;
            OriginalContent = partecipation.Content;
            //contest a cui si riferisce
            ContestId = partecipation.PartecipatingToContest;
            ContestEndDate = partecipation.ContestEndDate;
            ContestName = partecipation.PartecipatingToContestName;
            Shares = partecipation.Shares;

            Id = CreatorUser + PartecipationId + DateTime.Now.ToString() + (new Random().Next().ToString());
        }

        public Share(string description, User creatorUser, Share share) : this()
        {
            base.SortingDate = DateTime.Now;
            Description = description;
            CreatorUser = creatorUser.Email;
            CreatorUserName = creatorUser.Username;
            OriginalDate = share.OriginalDate;
            ContestName = share.ContestName;
            ContestEndDate = share.ContestEndDate;
            Media = share.Media;
            PartecipationId = share.PartecipationId;
            OriginalUser = share.OriginalUser;
            OriginalUserName = share.OriginalUserName;
            OriginalContent = share.OriginalContent;
            ContestId = share.ContestId;
            Shares = share.Shares;
            Id = CreatorUser + share.Id + DateTime.Now.ToString() + (new Random().Next().ToString());
        }

        public override string getCreatorUser()
        {
            return CreatorUser;
        }

        public override void setAvatar(string avatar)
        {
            Avatar = avatar;
        }
    }
}
