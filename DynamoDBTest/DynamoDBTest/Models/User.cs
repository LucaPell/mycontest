﻿using System.Collections.Generic;
using System.IO;
using Amazon.DynamoDBv2.DataModel;
using DynamoDBTest.Helpers;

namespace DynamoDBTest.Models
{
    [DynamoDBTable("User")]
    public class User
    {
        public string Username { get; set; }
        public string EncryptedPassword { get; set; }
        [DynamoDBHashKey]
        public string Email { get; set; }
        public bool EmailConfirmed { get; set; }

        public List<string> CreatedContests { get; set; }
        public List<string> Partecipations { get; set; }
        public List<string> Shares { get; set; }

        public Dictionary<string, string> FollowsContests { get; set; }
        public Dictionary<string, string> FollowsUsers { get; set; }
        public Dictionary<string, string> FollowedByUsers { get; set; }
        [DynamoDBIgnore]
        public string Bio { get; set; }
        public string Avatar { get; set; }
        public string DeleteHashAvatar { get; set; }
        public string Banner { get; set; }
        public string DeleteHashBanner { get; set; }

        public User()
        {
            EmailConfirmed = false;
            CreatedContests = new List<string>();
            Partecipations = new List<string>();
            Shares = new List<string>();
            FollowsContests = new Dictionary<string, string>();
            FollowsUsers = new Dictionary<string, string>();
            FollowedByUsers = new Dictionary<string, string>();
        }

        public User(string email, string username, string password) : this()
        {
            Email = email; 
            Username = username;
            FollowsUsers.Add(email, username);
            EncryptedPassword = new Crypto().Encrypt(password);
        }

        public void changePassword(string password)
        {
            EncryptedPassword = new Crypto().Encrypt(password);
        }

        public bool CheckPassword(string passwordToVerify)
        {
            return CheckEncryptedPassword(new Crypto().Encrypt(passwordToVerify));
        }

        public bool CheckEncryptedPassword(string encryptedPasswordToVerify)
        {
            return EncryptedPassword.Equals(encryptedPasswordToVerify);
        }
    }
}
