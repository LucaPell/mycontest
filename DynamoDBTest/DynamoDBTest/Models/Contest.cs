﻿using System;
using System.Collections.Generic;
using System.IO;
using Amazon.DynamoDBv2.DataModel;
using DynamoDBTest.ViewModels;

namespace DynamoDBTest.Models
{
    public class Contest : AbstractSortableByDateItem
    {
        [DynamoDBHashKey]
        public string Id { get; set; }
        [DynamoDBGlobalSecondaryIndexHashKey]
        public string Status { get; set; }
        public string Name { get; set; }
        public DateTime EndDate { get; set; }
        [DynamoDBGlobalSecondaryIndexHashKey]
        public string CreatorUser { get; set; }
        public string CreatorUserName { get; set; }
        public List<string> HasPartecipations { get; set; }
        public Dictionary<string, string> FollowedByUsers { get; set; }
        public List<string> Comments { get; set; }
        public List<string> Votes { get; set; }
        public int Shares { get; set; }
        [DynamoDBIgnore]
        public string Avatar { get; set; }
        public MemoryStream TempBanner { get; set; }
        public int VotesNumber { get; set; }
        public string Banner { get; set; }
        public string DeleteHash { get; set; }
        public string Description { get; set; }
        public string VictoryPartecipation { get; set; }

        public Contest()
        {
            Status = "OK";
            HasPartecipations = new List<string>();
            FollowedByUsers = new Dictionary<string, string>();
            Comments = new List<string>();
            Banner = null;
            DeleteHash = null;
            Votes = new List<string>();
            Shares = 0;
            VotesNumber = 0;
        }

        public Contest(string name, DateTime endDate, string description, User user) : this()
        {
            Id = DateTime.Now.ToString() + (new Random().Next().ToString());
            Name = name;
            base.SortingDate = DateTime.Now;
            EndDate = endDate;
            Description = description;
            CreatorUser = user.Email;
            CreatorUserName = user.Username;

        }

        public override string getCreatorUser()
        {
            return CreatorUser;
        }

        public override void setAvatar(string avatar)
        {
            Avatar = avatar;
        }

    }
}
