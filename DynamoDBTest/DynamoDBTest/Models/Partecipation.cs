﻿using System;
using System.Collections.Generic;
using System.IO;
using Amazon.DynamoDBv2.DataModel;
using DynamoDBTest.ViewModels;

namespace DynamoDBTest.Models
{
    public class Partecipation : AbstractSortableByDateItem
    {
        [DynamoDBHashKey]
        public string Id { get; set; }
        [DynamoDBGlobalSecondaryIndexHashKey]
        public string Status { get; set; }

        public string Media { get; set; }
        public string DeleteHash { get; set; }
        [DynamoDBGlobalSecondaryIndexHashKey]
        public string CreatorUser { get; set; }
        public string CreatorUserName { get; set; }
        public int Winner { get; set; }
        public DateTime ContestEndDate { get; set; }

        [DynamoDBGlobalSecondaryIndexHashKey]
        public string PartecipatingToContest { get; set; }
        public string PartecipatingToContestName { get; set; }

        public List<string> Votes { get; set; }
        [DynamoDBGlobalSecondaryIndexRangeKey]
        public int VotesNumber { get; set; }
        public int Shares { get; set; }
        public List<string> Comments { get; set; }
        [DynamoDBIgnore]
        public MemoryStream TempMedia { get; set; }
        public string Avatar { get; set; }
        public string Content { get; set; }

        public Partecipation()
        {
            Votes = new List<string>();
            Comments = new List<string>();
            VotesNumber = 0;
            Shares = 0;
            Winner = 0;
            Status = "OK";
        }

        public Partecipation(string content, User user, Contest contest) : this()
        {
            CreatorUser = user.Email;
            CreatorUserName = user.Username;
            PartecipatingToContest = contest.Id;
            PartecipatingToContestName = contest.Name;
            Id = CreatorUser + PartecipatingToContest;
            ContestEndDate = contest.EndDate;
            base.SortingDate = DateTime.Now;
            Content = content;
        }

        public Partecipation(string content, User user, Partecipation partecipation) : this()
        {
            CreatorUser = user.Email;
            CreatorUserName = user.Username;
            PartecipatingToContest = partecipation.PartecipatingToContest;
            PartecipatingToContestName = partecipation.PartecipatingToContestName;
            Id = CreatorUser + PartecipatingToContest;
            ContestEndDate = partecipation.ContestEndDate;
            base.SortingDate = DateTime.Now;
            Content = content;
        }

        public Partecipation(string content, User user, Share share) : this()
        {
            CreatorUser = user.Email;
            CreatorUserName = user.Username;
            PartecipatingToContest = share.ContestId;
            PartecipatingToContestName = share.ContestName;
            Id = CreatorUser + PartecipatingToContest;
            ContestEndDate = share.ContestEndDate;
            base.SortingDate = DateTime.Now;
            Content = content;
        }

        public override string getCreatorUser()
        {
            return CreatorUser;
        }

        public override void setAvatar(string avatar)
        {
            Avatar = avatar;
        }
    }
}
