﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DynamoDBTest.Models
{
    public class StringWrapper
    {
        public string String1 { get; set; }
        public string String2 { get; set; }
        public string String3 { get; set; }

        public StringWrapper(string s1, string s2)
        {
            String1 = s1;
            String2 = s2;
        }

        public StringWrapper(string s1, string s2, string s3)
        {
            String1 = s1;
            String2 = s2;
            String3 = s3;
        }

    }
}
