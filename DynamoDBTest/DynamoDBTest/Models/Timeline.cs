﻿using System;
using System.Collections.Generic;
using System.Text;
using Amazon.DynamoDBv2.DataModel;
using DynamoDBTest.ViewModels;

namespace DynamoDBTest.Models
{
    public class Timeline
    {
        [DynamoDBHashKey]
        public string Id { get; set; }
        [DynamoDBGlobalSecondaryIndexHashKey]
        public string Status { get; set; }
        public string UserId { get; set; }
        [DynamoDBGlobalSecondaryIndexRangeKey]
        public DateTime Date { get; set; }
        public ItemType Type { get; set; }

        public Timeline()
        {

        }

        public Timeline(string id, string userId, DateTime date, ItemType type)
        {
            Id = id;
            UserId = userId;
            Date = date;
            Type = type;
            Status = "OK";
        }
    }

    public enum ItemType { Contest, Partecipation, Share};
}
