﻿using System.Collections.ObjectModel;
using DynamoDBTest.Helpers;
using System;
using DynamoDBTest.Services;
using System.Collections.Generic;
using System.Diagnostics;

namespace DynamoDBTest.ViewModels
{
    public interface ICollectioViewModel<T> : IDisposable
    {
        ObservableCollection<T> Items { get; set; }
        RetryCommand RetryLoadItemsCommand { get; set; }
        bool RefreshOnNextCommand { get; set; }
    }

    public abstract class ICollectionViewModel<T, B> : BaseViewModel, ICollectioViewModel<T> where B : new()
    {
        public ObservableCollection<T> Items { get; set; }
        public RetryCommand RetryLoadItemsCommand { get; set; }
        public B service;
        public bool RefreshOnNextCommand { get; set; }
        public UserService userService;

        public ICollectionViewModel()
        {
            RefreshOnNextCommand = true;
        }

        public virtual void Dispose()
        {
            GlobalCollections globalItems = GlobalCollections.GetInstance();
            foreach (var item in Items)
            {
                globalItems.Remove(item);
            }
        }

        public virtual void UpdateCollection(IEnumerable<T> items)
        {
            if (IsBusy)
                return;

            if(!items.GetEnumerator().MoveNext())
            {
                return;
            }

            IsBusy = true;

            items.GetEnumerator().Reset();

            if(RefreshOnNextCommand)
            {
                Items.Clear();
            }

            try
            {
                GlobalCollections globalItems = GlobalCollections.GetInstance();
                foreach (var item in items)
                {
                    Items.Add(item);
                    globalItems.Add(item);
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
            }
            finally
            {
                IsBusy = false;
                RefreshOnNextCommand = false;
            }
        }
    }
}
