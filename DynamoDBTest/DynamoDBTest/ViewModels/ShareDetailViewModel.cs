﻿using DynamoDBTest.Models;
using DynamoDBTest.Helpers;
using DynamoDBTest.Services;
using DynamoDBTest.Controllers;

using System.Threading;
using System.Threading.Tasks;
using System;

namespace DynamoDBTest.ViewModels
{
    class ShareDetailViewModel : BaseViewModel
    {
        public Share share;
        public RetryCommand RetryAddVoteCommand;
        public RetryCommand RetryRemoveVoteCommand;
        public RetryCommand RetryLoadItemCommand;

        public ShareService service;

        public event EventHandler ItemLoaded;

        public ShareDetailViewModel(Share share = null)
        {
            if (share != null)
            {
                this.share = share;
            }

            RetryAddVoteCommand = new RetryCommand(AddVote, 2, 3000, UpdateServiceWithNewToken);
            RetryRemoveVoteCommand = new RetryCommand(RemoveVote, 2, 3000, UpdateServiceWithNewToken);
            RetryLoadItemCommand = new RetryCommand<string>(LoadItem, 3, 4000, UpdateServiceWithNewToken);
        }

        private void UpdateServiceWithNewToken(CancellationToken token)
        {
            service.UpdateToken(token);
        }

        async Task AddVote()
        {
            StringWrapper idTypeEmail;
            User user = LoginController.GetInstance().CurrentUser;
            idTypeEmail = new StringWrapper(share.PartecipationId, "Partecipation", user.Email);
            share.Votes.Add(user.Email);
            if (!await service.AddVote(idTypeEmail))
            {
                share.Votes.Remove(user.Email);
            }
        }

        async Task RemoveVote()
        {
            StringWrapper idTypeEmail;
            User user = LoginController.GetInstance().CurrentUser;
            idTypeEmail = new StringWrapper(share.PartecipationId, "Partecipation", user.Email);
            share.Votes.Remove(user.Email);
            if (!await service.RemoveVote(idTypeEmail))
            {
                share.Votes.Add(user.Email);
            }
        }


        async Task LoadItem(string itemId)
        {
            this.share = await service.Get(itemId);
            ItemLoaded(this, new EventArgs());
        }
    }
}
