﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using System.Threading;
using DynamoDBTest.Models;
using DynamoDBTest.Helpers;
using DynamoDBTest.Services;
using DynamoDBTest.Controllers;
using System.Diagnostics;

namespace DynamoDBTest.ViewModels
{
    class ShareViewModel : ICollectionViewModel<Share, ShareService>
    {
        public Command LoadItemsByUserCommand { get; set; }
        public RetryCommand RetryLoadItemsByUserCommand { get; }
        public Command AddShareCommand { get; set; }
        public RetryCommand RetryAddShareCommand;
        public RetryCommand RetryAddVoteCommand;
        public RetryCommand RetryRemoveVoteCommand;

        public ShareViewModel()
        {
            Title = "Browse";
            Items = new ObservableCollection<Share>();
            RetryLoadItemsCommand = new RetryCommand(LoadAll, 3, 4000, UpdateServiceWithNewToken);
            LoadItemsByUserCommand = new Command<string>(async (string user) => await LoadAllByUser(user));
            RetryLoadItemsByUserCommand = new RetryCommand<string>(LoadAllByUser, 3, 4000, UpdateServiceWithNewToken);
            AddShareCommand = new Command<Share>(async (Share item) => await AddShare(item));
            RetryAddShareCommand = new RetryCommand<Share>(AddShare, 3, 4000, UpdateServiceWithNewToken);
            RetryAddVoteCommand = new RetryCommand<int>(AddVote, 3, 4000, UpdateServiceWithNewToken);
            RetryRemoveVoteCommand = new RetryCommand<int>(RemoveVote, 3, 4000, UpdateServiceWithNewToken);
        }

        private void UpdateServiceWithNewToken(CancellationToken token)
        {
            if (service == null || base.RefreshOnNextCommand)
                service = new ShareService(token);
            else
                service.UpdateToken(token);
        }

        async Task LoadAll()
        {
            IEnumerable<Share> items = new List<Share>();
            try
            {
                items = await service.GetAll();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                UpdateCollection(items);
            }
        }

              async Task LoadAllByUser(string user)
        {
            IEnumerable<Share> items = new List<Share>();
            try
            {
                items = await service.GetAllByUser(user);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                UpdateCollection(items);
            }
        }

        async Task AddShare(Share item)
        {
            await service.Add(item);
            Items.Add(item);
        }
        async Task AddVote(int position)
        {
            Share share = Items[position];
            StringWrapper idTypeEmail;
            User user = LoginController.GetInstance().CurrentUser;
            idTypeEmail = new StringWrapper(share.PartecipationId, "Partecipation", user.Email);
            share.Votes.Add(user.Email);
            if (!await service.AddVote(idTypeEmail))
            {
                share.Votes.Remove(user.Email);
            }
        }

        async Task RemoveVote(int position)
        {
            Share share = Items[position];
            StringWrapper idTypeEmail;
            User user = LoginController.GetInstance().CurrentUser;
            idTypeEmail = new StringWrapper(share.PartecipationId, "Partecipation", user.Email);
            share.Votes.Remove(user.Email);
            if (!await service.RemoveVote(idTypeEmail))
            {
                share.Votes.Add(user.Email);
            }
        }

        public void PutItem(Share item)
        {
            Items.Insert(0, item);
        }

        public void RemoveItem(int position)
        {
            Items.RemoveAt(position);
        }
    }
}
