﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using System.Threading;
using DynamoDBTest.Models;
using DynamoDBTest.Helpers;
using DynamoDBTest.Services;
using DynamoDBTest.Controllers;
using System.Linq;
using Amazon.DynamoDBv2.Model;

namespace DynamoDBTest.ViewModels
{
    public class PartecipationViewModel : ICollectionViewModel<Partecipation, PartecipationService>
    {
        public Command LoadItemsByContestCommand { get; set; }
        public Command LoadItemsByUserCommand { get; set; }
        public RetryCommand RetryLoadItemsByContestCommand { get; }
        public RetryCommand RetryLoadItemsByUserCommand { get; }
        public Command AddPartecipationCommand { get; set; }
        public RetryCommand RetryDeletePartecipationCommand { get; set; }

        public RetryCommand RetryAddPartecipationCommand;
        public RetryCommand RetryAddVoteCommand;
        public RetryCommand RetryRemoveVoteCommand;

        public PartecipationViewModel()
        {
            Title = "Browse";
            Items = new ObservableCollection<Partecipation>();
            RetryLoadItemsCommand = new RetryCommand(LoadAll, 3, 6000, UpdateServiceWithNewToken);
            LoadItemsByContestCommand = new Command<string>(async (string contestId) => await LoadAllByContest(contestId));
            RetryLoadItemsByContestCommand = new RetryCommand<string>(LoadAllByContest, 3, 5000, UpdateServiceWithNewToken);
            LoadItemsByUserCommand = new Command<string>(async (string user) => await LoadAllByUser(user));
            RetryLoadItemsByUserCommand = new RetryCommand<string>(LoadAllByUser, 3, 4000, UpdateServiceWithNewToken);
            AddPartecipationCommand = new Command<Partecipation>(async (Partecipation item) => await AddPartecipation(item));
            RetryAddPartecipationCommand = new RetryCommand<Partecipation>(AddPartecipation, 1, 8000, UpdateServiceWithNewToken);
            RetryAddVoteCommand = new RetryCommand<int>(AddVote, 2, 3000, UpdateServiceWithNewToken);
            RetryRemoveVoteCommand = new RetryCommand<int>(RemoveVote, 2, 3000, UpdateServiceWithNewToken);
            RetryDeletePartecipationCommand = new RetryCommand<string>(RemovePartecipation, 1, 8000, UpdateServiceWithNewToken);
        }

        private void UpdateServiceWithNewToken(CancellationToken token)
        {
            if (service == null || base.RefreshOnNextCommand)
                service = new PartecipationService(token);
            else
                service.UpdateToken(token);
            if (userService == null || base.RefreshOnNextCommand)
                userService = new UserService(token);
            else
                userService.UpdateToken(token);
        }

        async Task LoadAll()
        {
            IEnumerable<Partecipation> items = new List<Partecipation>();
            try
            {
                items = await service.GetAll();

                var avatarIds = new HashSet<string>();
                var avatars = new Dictionary<string, AttributeValue>();
                AttributeValue value;
                foreach (Partecipation item in items)
                {
                    avatarIds.Add(item.getCreatorUser());
                }

                avatars = await userService.GetAvatars(avatarIds);

                foreach (Partecipation item in items)
                {
                    if (avatars.TryGetValue(item.getCreatorUser(), out value))
                    {
                        item.setAvatar(value.S);
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                UpdateCollection(items);
            }
        }

        async Task RemovePartecipation(string partecipationId)
        {
            await service.Delete(partecipationId);
        }

        async Task LoadAllByContest(string contestId)
        {
            IEnumerable<Partecipation> items = new List<Partecipation>();
            try
            {
                items = await service.GetAllByContest(contestId);

                var avatarIds = new HashSet<string>();
                var avatars = new Dictionary<string, AttributeValue>();
                AttributeValue value;
                foreach (Partecipation item in items)
                {
                    avatarIds.Add(item.getCreatorUser());
                }

                avatars = await userService.GetAvatars(avatarIds);

                foreach (Partecipation item in items)
                {
                    if (avatars.TryGetValue(item.getCreatorUser(), out value))
                    {
                        item.setAvatar(value.S);
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                UpdateCollection(items);
            }
        }

        async Task LoadAllByUser(string user)
        {
            IEnumerable<Partecipation> items = new List<Partecipation>();
            try
            {
                items = await service.GetAllByUser(user);

                var avatarIds = new HashSet<string>();
                var avatars = new Dictionary<string, AttributeValue>();
                AttributeValue value;
                foreach (Partecipation item in items)
                {
                    avatarIds.Add(item.getCreatorUser());
                }

                avatars = await userService.GetAvatars(avatarIds);

                foreach (Partecipation item in items)
                {
                    if (avatars.TryGetValue(item.getCreatorUser(), out value))
                    {
                        item.setAvatar(value.S);
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                UpdateCollection(items);
            }
        }

        async Task AddPartecipation(Partecipation item)
        {
            if (item.TempMedia != null)
            {
                ImgurRepository repo = new ImgurRepository();
                var urls = await repo.UploadImage(item.TempMedia.ToArray());
                string value;
                if(urls.TryGetValue("link", out value))
                {
                    item.Media = value;
                }
                else
                {
                    return;
                    
                }
                if(urls.TryGetValue("deletehash", out value))
                {
                    item.DeleteHash = value;
                }
                else
                {
                    return;
                }

            }
            await service.Add(item);
        }

        async Task AddVote(int position)
        {
            User user = LoginController.GetInstance().CurrentUser;
            Partecipation i = Items[position];

            StringWrapper idAndEmail = new StringWrapper(i.Id, user.Email);
            i.Votes.Add(user.Email);
            if (!await service.AddVote(idAndEmail))
            {
                i.Votes.Remove(user.Email);
            }
        }

        async Task RemoveVote(int position)
        {
            User user = LoginController.GetInstance().CurrentUser;
            Partecipation i = Items[position];

            StringWrapper idAndEmail = new StringWrapper(i.Id, user.Email);
            i.Votes.Remove(user.Email);
            if (!await service.RemoveVote(idAndEmail))
            {
                i.Votes.Add(user.Email);
            }
        }

        public void PutPartecipationInCollection(Partecipation p)
        {
            Items.Add(p);
        }

        public void RemovePartecipationInCollection(String partecipationId)
        {
            Items.Remove(Items.Where(partecipationInCollection => partecipationInCollection.Id == partecipationId).Single());
        }

        public void PutItem(Partecipation item)
        {
            Items.Insert(0, item);
        }

        public void RemoveItem(int position)
        {
            Items.RemoveAt(position);
        }
    }
}
