﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Threading;
using System.Collections.ObjectModel;
using System.Diagnostics;

using DynamoDBTest.Helpers;
using DynamoDBTest.Models;
using DynamoDBTest.Services;
using DynamoDBTest.Controllers;
using Amazon.DynamoDBv2.Model;
using System.Linq;
using System.Collections;

namespace DynamoDBTest.ViewModels
{
    public class HomeViewModel : ICollectionViewModel<AbstractSortableByDateItem, object>
    {
        public Command LoadItemsCommand { get; }
        public RetryCommand RetryAddVoteCommand;
        public RetryCommand RetryRemoveVoteCommand;
        public ContestService contestService;
        public PartecipationService partecipationService;
        public ShareService shareService;
        public UserService userService;
        public TimelineService timelineService;

        public HomeViewModel()
        {
            Title = "Browse";
            Items = new ObservableCollection<AbstractSortableByDateItem>();
            LoadItemsCommand = new Command(async () => await LoadAll());
            RetryLoadItemsCommand = new RetryCommand(LoadAll, 1, 10000, UpdateServiceWithNewToken); 
            RetryAddVoteCommand = new RetryCommand<int>(AddVote, 1,3000, UpdateServiceWithNewToken);
            RetryRemoveVoteCommand = new RetryCommand<int>(RemoveVote, 1, 3000, UpdateServiceWithNewToken);
        }

        private void UpdateServiceWithNewToken(CancellationToken token)
        {
            if (contestService == null || base.RefreshOnNextCommand)
                contestService = new ContestService(token);
            else
                contestService.UpdateToken(token);

            if (partecipationService == null || base.RefreshOnNextCommand)
                partecipationService = new PartecipationService(token);
            else
                partecipationService.UpdateToken(token);

            if (shareService == null || base.RefreshOnNextCommand)
                shareService = new ShareService(token);
            else
                shareService.UpdateToken(token);

            if (userService == null || base.RefreshOnNextCommand)
                userService = new UserService(token);
            else
                userService.UpdateToken(token);

            if (timelineService == null || base.RefreshOnNextCommand)
                timelineService = new TimelineService(token);
            else
                timelineService.UpdateToken(token);
        }

        public override void Dispose()
        {
            GlobalCollections globalItems = GlobalCollections.GetInstance();
            foreach (var item in Items)
            {
                if (item.GetType().Equals(typeof(Contest)))
                {
                    globalItems.Remove((Contest)item);
                }
                else if (item.GetType().Equals(typeof(Partecipation)))
                {
                    globalItems.Remove((Partecipation)item);
                }
                else
                {
                    globalItems.Remove((Share)item);
                }
            }
        }

        public void UpdateCollection(AbstractSortableByDateItem item)
        {
            if (IsBusy)
                return;

            //if(items.Count<AbstractSortableByDateItem>() == 0)
            //{
            //    return;
            //}

            IsBusy = true;

            if (RefreshOnNextCommand)
            {
                Items.Clear();
            }

            try
            {
                GlobalCollections globalItems = GlobalCollections.GetInstance();

                Items.Add(item);

                globalItems.Add(item);
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
            }
            finally
            {
                IsBusy = false;
                RefreshOnNextCommand = false;
            }
        }

        public override void UpdateCollection(IEnumerable<AbstractSortableByDateItem> items)
        {
            if (IsBusy)
                return;

            if(items.Count<AbstractSortableByDateItem>() == 0)
            {
                return;
            }

            IsBusy = true;

            if (RefreshOnNextCommand)
            {
                Items.Clear();
            }

            try
            {
                GlobalCollections globalItems = GlobalCollections.GetInstance();
                foreach (var item in items)
                {
                    Items.Add(item);
                    globalItems.Add(item);
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
            }
            finally
            {
                IsBusy = false;
                RefreshOnNextCommand = false;
            }
        }

        async Task LoadAll()
        {
            User user = LoginController.GetInstance().CurrentUser;
            IEnumerable<AbstractSortableByDateItem> items = new List<AbstractSortableByDateItem>();
            try
            {
                items = await timelineService.GetAllItemsFromTimeline(user.FollowsUsers.Keys.ToArray());

                var avatarIds = new HashSet<string>();
                var avatars = new Dictionary<string, AttributeValue>();
                AttributeValue value;
                foreach (AbstractSortableByDateItem item in items)
                {
                    avatarIds.Add(item.getCreatorUser());
                    if (item.GetType().Equals(typeof(Share)))
                    {
                        var i = item as Share;
                        avatarIds.Add(i.OriginalUser);
                    }
                }

                avatars = await userService.GetAvatars(avatarIds);

                foreach (AbstractSortableByDateItem item in items)
                {
                    if (avatars.TryGetValue(item.getCreatorUser(), out value))
                    {
                        item.setAvatar(value.S);
                    }
                    if (item.GetType().Equals(typeof(Share)))
                    {
                        var i = item as Share;
                        if (avatars.TryGetValue(i.OriginalUser, out value))
                        {
                            i.OriginalAvatar = value.S;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                UpdateCollection(items);
            }
            /*
            //var items = new List<AbstractSortableByDateItem>();
            AbstractSortableByDateItem item = null;
            IEnumerable<Timeline> timelineIds;
            var incompleteShares = new List<Share>();
            User user = LoginController.GetInstance().CurrentUser;
            var avatarIds = new HashSet<string>();
            var avatars = new Dictionary<string, AttributeValue>();
            AttributeValue value;

            //prendo gli Id dei post nella timeline

            timelineIds = await timelineService.GetAllByUser(user.FollowsUsers.Keys.ToArray<string>());

            foreach(Timeline timeline in timelineIds)
            {
                if(timeline.Type == ItemType.Contest)
                {
                    item = await contestService.Get(timeline.Id);
                }
                else
                if (timeline.Type == ItemType.Partecipation)
                {
                    item = await partecipationService.Get(timeline.Id);
                }
                else
                if (timeline.Type == ItemType.Share)
                {
                    item = await shareService.Get(timeline.Id);
                    try
                    {
                        item = (await shareService.CompleteShares(new List<Share>() { (Share)item })).First<Share>(); //Fa schifo, ma almeno non ho dovuto creare un altro metodo CompleteShare(Share item)
                    }
                    catch(Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                    }
                }

                avatarIds.Add(item.getCreatorUser());

                try
                {
                    avatars = await userService.GetAvatars(avatarIds);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }

                if (avatars.TryGetValue(item.getCreatorUser(), out value))
                {
                    item.setAvatar(value.S);
                }

                UpdateCollection(item);
            }

            if(timelineIds.Count() < 10)
            {
                await LoadAll();
            }

            //if (incompleteShares != null && incompleteShares.Count != 0)
            //{
            //    items.AddRange( await shareService.CompleteShares(incompleteShares));
            //}

            //creo lista degli id di SOLO gli utenti di cui ho post
            //lo faccio perchè se altrimenti avessi 1000 followers ma 30 posts mi caricherei 970+ avatar per nessun motivo
            //foreach(var item in items) {
            //    avatarIds.Add(item.getCreatorUser());
            //}

            //prendo gli avatar da associare ai vari oggetti
            //try
            //{
            //    avatars = await userService.GetAvatars(avatarIds);
            //}
            //catch (Exception ex)
            //{
            //    Console.WriteLine(ex.Message);
            //}

            //li collego agli oggetti
            //foreach (var item in items)
            //{
            //    if(avatars.TryGetValue(item.getCreatorUser(), out value))
            //    {
            //        item.setAvatar(value.B);
            //    }
            //}

            //items.Sort();
            //UpdateCollection(items);
            */
        }



        async Task AddVote(int position)
        {
            User user = LoginController.GetInstance().CurrentUser;
            AbstractSortableByDateItem item = Items[position];

            if (item.GetType().Equals(typeof(Partecipation)))
            {
                Partecipation i = item as Partecipation;
                StringWrapper idAndEmail = new StringWrapper(i.Id, user.Email);
                i.Votes.Add(user.Email);
                if (!await partecipationService.AddVote(idAndEmail))
                {
                    i.Votes.Remove(user.Email);
                }

            }
            else if (item.GetType().Equals(typeof(Share)))
            {
                Share i = item as Share;

                StringWrapper idTypeEmail;
                if (i.PartecipationId != null)
                {
                    idTypeEmail = new StringWrapper(i.PartecipationId, "Partecipation", user.Email);
                }
                else 
                {
                    idTypeEmail = new StringWrapper(i.ContestId, "Contest", user.Email);
                }

                i.Votes.Add(user.Email);
                if (!await shareService.AddVote(idTypeEmail))
                {
                    i.Votes.Remove(user.Email);
                }
            }
            else if (item.GetType().Equals(typeof(Contest)))
            {
                Contest i = item as Contest;

                StringWrapper idTypeEmail;
                idTypeEmail = new StringWrapper(i.Id, "Contest", user.Email);
                i.Votes.Add(user.Email);
                if (!await contestService.AddVote(idTypeEmail))
                {
                    i.Votes.Remove(user.Email);
                }
            }
        }

        async Task RemoveVote(int position)
        {
            User user = LoginController.GetInstance().CurrentUser;
            AbstractSortableByDateItem item = Items[position];

            if (item.GetType().Equals(typeof(Partecipation)))
            {
                Partecipation i = item as Partecipation;
                StringWrapper idAndEmail = new StringWrapper(i.Id, user.Email);
                i.Votes.Remove(user.Email);
                if (!await partecipationService.RemoveVote(idAndEmail))
                {
                    i.Votes.Add(user.Email);
                }
            }
            else if (item.GetType().Equals(typeof(Share)))
            {
                Share i = item as Share;
                StringWrapper idTypeEmail;

                if(i.PartecipationId != null)
                {
                    idTypeEmail = new StringWrapper(i.PartecipationId, "Partecipation", user.Email);
                }
                else 
                {
                    idTypeEmail = new StringWrapper(i.ContestId, "Contest", user.Email);
                }

                i.Votes.Remove(user.Email);
                if (!await shareService.RemoveVote(idTypeEmail))
                {
                    i.Votes.Add(user.Email);
                }
            }
            else if (item.GetType().Equals(typeof(Contest)))
            {
                Contest i = item as Contest;
                StringWrapper idTypeEmail;
                idTypeEmail = new StringWrapper(i.Id, "Contest", user.Email);
                i.Votes.Remove(user.Email);
                if (!await contestService.RemoveVote(idTypeEmail))
                {
                    i.Votes.Add(user.Email);
                }
            }
        }

        public void PutItem(AbstractSortableByDateItem item)
        {
            Items.Insert(0, item);
        }

        public void RemoveItem(int position)
        {
            Items.RemoveAt(position);
        }

        //public void RemoveById(String itemId)
        //{
        //    Items.Remove(Items.Where(item => item.Id == partecipationId).Single());
        //}
    }
}
