﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Threading;
using System.Collections.ObjectModel;
using System.Diagnostics;

using DynamoDBTest.Helpers;
using DynamoDBTest.Models;
using DynamoDBTest.Services;
using DynamoDBTest.Controllers;
using Amazon.DynamoDBv2.Model;

namespace DynamoDBTest.ViewModels
{
    public class ContestViewModel : ICollectionViewModel<Contest, ContestService>
    {
        public Command LoadItemsCommand { get; }
        public Command AddContestCommand { get; }
        public RetryCommand RetryAddContestCommand;
        public Command LoadAllByCreatorUserCommand { get; }
        public RetryCommand RetryLoadAllByCreatorUserCommand;
        public Command LoadAllByFollowsUserCommand { get; }
        public RetryCommand RetryLoadAllByFollowsUserCommand;
        public RetryCommand RetryAddVoteCommand;
        public RetryCommand RetryRemoveVoteCommand;

        public ContestViewModel()
        {
            Title = "Browse";
            Items = new ObservableCollection<Contest>();
            LoadItemsCommand = new Command(async () => await LoadAll());
            RetryLoadItemsCommand = new RetryCommand(LoadAll, 3, 4000, UpdateServiceWithNewToken);
            AddContestCommand = new Command<Contest>(async (Contest contest) => await AddContest(contest));
            RetryAddContestCommand = new RetryCommand<Contest>(AddContest, 1, 5000, UpdateServiceWithNewToken);
            LoadAllByCreatorUserCommand = new Command<string>(async (string user) => await LoadAllByCreatorUser(user));
            RetryLoadAllByCreatorUserCommand = new RetryCommand<string>(LoadAllByCreatorUser, 3, 4000, UpdateServiceWithNewToken);
            LoadAllByFollowsUserCommand = new Command(async () => await LoadAllByFollowsUser());
            RetryLoadAllByFollowsUserCommand = new RetryCommand(LoadAllByFollowsUser, 3, 4000, UpdateServiceWithNewToken);
            RetryAddVoteCommand = new RetryCommand<int>(AddVote, 1, 3000, UpdateServiceWithNewToken);
            RetryRemoveVoteCommand = new RetryCommand<int>(RemoveVote, 1, 3000, UpdateServiceWithNewToken);
        }

        private void UpdateServiceWithNewToken(CancellationToken token)
        {
            if (service == null || base.RefreshOnNextCommand)
                service = new ContestService(token);
            else
                service.UpdateToken(token);
            if (userService == null || base.RefreshOnNextCommand)
                userService = new UserService(token);
            else
                userService.UpdateToken(token);
        }

        async Task LoadAll()
        {
            IEnumerable<Contest> items = new List<Contest>();
            try
            {
                items = await service.GetAll();

                var avatarIds = new HashSet<string>();
                var avatars = new Dictionary<string, AttributeValue>();
                AttributeValue value;
                foreach (Contest item in items)
                {
                    avatarIds.Add(item.getCreatorUser());
                }

                avatars = await userService.GetAvatars(avatarIds);

                foreach (Contest item in items)
                {
                    if (avatars.TryGetValue(item.getCreatorUser(), out value))
                    {
                        item.setAvatar(value.S);
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                UpdateCollection(items);
            }
        }

        async Task LoadAllByCreatorUser(string user)
        {
            IEnumerable<Contest> items = new List<Contest>();
            try
            {
                items = await service.GetAllByCreatorUser(user);

                var avatarIds = new HashSet<string>();
                var avatars = new Dictionary<string, AttributeValue>();
                AttributeValue value;
                foreach (Contest item in items)
                {
                    avatarIds.Add(item.getCreatorUser());
                }

                avatars = await userService.GetAvatars(avatarIds);

                foreach (Contest item in items)
                {
                    if (avatars.TryGetValue(item.getCreatorUser(), out value))
                    {
                        item.setAvatar(value.S);
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                UpdateCollection(items);
            }
        }

        async Task LoadAllByFollowsUser()
        {
            var items = new List<Contest>();
            User user = LoginController.GetInstance().CurrentUser;
            foreach (string userId in user.FollowsUsers.Keys)
            {
                try
                {
                    items.AddRange(await service.GetAllByCreatorUser(userId));

                    var avatarIds = new HashSet<string>();
                    var avatars = new Dictionary<string, AttributeValue>();
                    AttributeValue value;
                    foreach (Contest item in items)
                    {
                        avatarIds.Add(item.getCreatorUser());
                    }

                    avatars = await userService.GetAvatars(avatarIds);

                    foreach (Contest item in items)
                    {
                        if (avatars.TryGetValue(item.getCreatorUser(), out value))
                        {
                            item.setAvatar(value.S);
                        }
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
                finally
                {
                    items.Sort();
                    UpdateCollection(items);
                }
            }
        }

        async Task AddContest(Contest contest)
        {
            if (contest.TempBanner != null)
            {
                ImgurRepository repo = new ImgurRepository();
                var urls = await repo.UploadImage(contest.TempBanner.ToArray());

                string value;
                if (urls.TryGetValue("link", out value))
                {
                    contest.Banner = value;
                }
                else
                {
                    return;
                }

                if (urls.TryGetValue("deletehash", out value))
                {
                    contest.DeleteHash = value;
                }
                else
                {
                    return;
                }

                contest.TempBanner = null;
            }
            Items.Add(contest);
            await service.Add(contest);
        }

        async Task AddVote(int position)
        {
            User user = LoginController.GetInstance().CurrentUser;
            Contest i = Items[position];

            StringWrapper idTypeEmail;
            idTypeEmail = new StringWrapper(i.Id, "Contest", user.Email);
            i.Votes.Add(user.Email);
            if (!await service.AddVote(idTypeEmail))
            {
                i.Votes.Remove(user.Email);
            }
        }

        async Task RemoveVote(int position)
        {
            User user = LoginController.GetInstance().CurrentUser;
            Contest i = Items[position];

            StringWrapper idTypeEmail;
            idTypeEmail = new StringWrapper(i.Id, "Contest", user.Email);
            i.Votes.Remove(user.Email);
            if (!await service.RemoveVote(idTypeEmail))
            {
                i.Votes.Add(user.Email);
            }
        }

        public void PutItem(Contest item)
        {
            Items.Insert(0, item);
        }

        public void RemoveItem(int position)
        {
            Items.RemoveAt(position);
        }
    }
}
