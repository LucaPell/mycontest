﻿using System.Threading.Tasks;
using System.Threading;

using DynamoDBTest.Models;
using DynamoDBTest.Services;
using DynamoDBTest.Helpers;
using System.IO;

namespace DynamoDBTest.ViewModels
{
    class UserViewModel
    {
        public User LoadedUser { get; set; }
        public Command LoadUserCommand { get; set; }
        public RetryCommand RetryLoadUserCommand;
        public Command UpdateUserCommand { get; set; }
        public RetryCommand RetryUpdateUserCommand;
        public event UserLoadedHandler UserLoaded;
        public delegate void UserLoadedHandler(bool loaded);
        public RetryCommand RetryUpdateAvatarCommand { get; set; }
        public RetryCommand RetryUpdateBannerCommand { get; set; }
        public RetryCommand RetryDeleteAvatarCommand { get; set; }
        public RetryCommand RetryDeleteBannerCommand { get; set; }
        public RetryCommand RetryDeleteBioCommand { get; set; }
        public RetryCommand RetryUpdateBioCommand { get; set; }
        public RetryCommand RetryUpdateNameCommand { get; set; }
        public RetryCommand RetryGetSingleAvatarCommand { get; set; }
        public UserService service;
        public string avatar;

        public UserViewModel()
        {
            LoadUserCommand = new Command<string>(async (string user) => await LoadUser(user));
            RetryLoadUserCommand = new RetryCommand<string>(LoadUser, 3, 4000, UpdateServiceWithNewToken);
            UpdateUserCommand = new Command(async () => await UpdateUser());
            RetryUpdateUserCommand = new RetryCommand(UpdateUser, 3, 4000, UpdateServiceWithNewToken);
            RetryUpdateAvatarCommand = new RetryCommand<MemoryStream>(UpdateAvatar, 1, 9000, UpdateServiceWithNewToken);
            RetryUpdateBannerCommand = new RetryCommand<MemoryStream>(UpdateBanner, 1, 9000, UpdateServiceWithNewToken);
            RetryDeleteAvatarCommand = new RetryCommand(DeleteAvatar, 1, 9000, UpdateServiceWithNewToken);
            RetryDeleteBannerCommand = new RetryCommand(DeleteBanner, 1, 9000, UpdateServiceWithNewToken);
            RetryDeleteBioCommand = new RetryCommand<string>(DeleteBio, 1, 5000, UpdateServiceWithNewToken);
            RetryUpdateBioCommand = new RetryCommand<StringWrapper>(UpdateBio, 1, 5000, UpdateServiceWithNewToken);
            RetryUpdateNameCommand = new RetryCommand<StringWrapper>(UpdateName, 1, 5000, UpdateServiceWithNewToken);
            RetryGetSingleAvatarCommand = new RetryCommand<string>(GetSingleAvatar, 1, 5000, UpdateServiceWithNewToken);
            service = new UserService();
        }

        private void UpdateServiceWithNewToken(CancellationToken token)
        {
            if (service == null)
                service = new UserService(token);
            else
                service.UpdateToken(token);
        }

        private async Task LoadUser(string user)
        {
            LoadedUser = await service.Get(user);
            UserLoaded(true);
        }

        private async Task UpdateUser()
        {
            await service.Update(LoadedUser);
        }

        private async Task UpdateAvatar(MemoryStream avatar)
        {
            await service.AddAvatar(avatar);
        }

        private async Task UpdateBanner(MemoryStream banner)
        {
            await service.AddBanner(banner);
        }

        private async Task DeleteAvatar()
        {
            await service.RemoveAvatar();
        }

        private async Task DeleteBanner()
        {
            await service.RemoveBanner();
        }

        private async Task UpdateBio(StringWrapper userIdAndBio)
        {
                                           //id         descText
            await service.UpdateBio(userIdAndBio.String1, userIdAndBio.String2);
        }

        private async Task DeleteBio(string userid)
        {
            await service.DeleteBio(userid);
        }

        private async Task UpdateName(StringWrapper userIdAndName)
        {
                                        //id         descText
            await service.UpdateName(userIdAndName.String1, userIdAndName.String2);
        }

        private async Task GetSingleAvatar(string userId)
        {
            avatar = await service.GetSingleAvatar(userId);
        }
    }
}
