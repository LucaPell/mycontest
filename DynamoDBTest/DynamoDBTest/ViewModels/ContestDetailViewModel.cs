﻿using DynamoDBTest.Helpers;
using DynamoDBTest.Models;
using DynamoDBTest.Services;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace DynamoDBTest.ViewModels
{
    public class ContestDetailViewModel : BaseViewModel
    {
        public Contest contest { get; set; }
        public ContestService service;
        public RetryCommand RetryLoadItemCommand;
        public event EventHandler ItemLoaded;
        public RetryCommand RetryAddBannerCommand;
        public RetryCommand RetryDeleteBannerCommand;
        public RetryCommand RetryDeleteTextCommand;
        public RetryCommand RetryUpdateTextCommand;

        public ContestDetailViewModel(Contest contest = null)
        {
            if(contest != null)
            {
                Title = contest.Name;
                this.contest = contest;
            }
            RetryLoadItemCommand = new RetryCommand<string>(LoadItem, 3, 4000, UpdateServiceWithNewToken);
            RetryAddBannerCommand = new RetryCommand<Contest>(AddBanner, 1, 5000, UpdateServiceWithNewToken);
            RetryDeleteBannerCommand = new RetryCommand<Contest>(DeleteBanner, 1, 5000, UpdateServiceWithNewToken);
            RetryDeleteTextCommand = new RetryCommand<string>(DeleteText, 1, 5000, UpdateServiceWithNewToken);
            RetryUpdateTextCommand = new RetryCommand<StringWrapper>(UpdateText, 1, 5000, UpdateServiceWithNewToken);
        }

        private async Task UpdateText(StringWrapper contest)
        {
                                          //id         descText
            await service.UpdateText(contest.String1, contest.String2);
        }

        private async Task DeleteText(string contestid)
        {
            await service.DeleteText(contestid);
        }

        private void UpdateServiceWithNewToken(CancellationToken token)
        {
            if (service == null)
                service = new ContestService(token);
            else
                service.UpdateToken(token);
        }

        private async Task LoadItem(string itemId)
        {
            this.contest = await service.Get(itemId);
        }

        private async Task AddBanner(Contest c)
        {
            await service.AddBanner(c);
        }

        private async Task DeleteBanner(Contest c)
        {
            await service.DeleteBanner(c);
        }
    }
}
