﻿using System;
using System.IO;
using Amazon.DynamoDBv2.DataModel;
using DynamoDBTest.ViewModels;

namespace DynamoDBTest.ViewModels
{
    public abstract class AbstractSortableByDateItem : IComparable
    {
        [DynamoDBGlobalSecondaryIndexRangeKey]
        public DateTime SortingDate { get; set; }

        public int CompareTo(object obj)
        {
            if (obj == null) return 1;

            AbstractSortableByDateItem otherItem = obj as AbstractSortableByDateItem;
            if (otherItem != null)
                return otherItem.SortingDate.CompareTo(this.SortingDate);
            else
                throw new ArgumentException("Object is not a Temperature");
        }

        public virtual string getCreatorUser()
        {
            return "string";
        }

        public virtual void setAvatar(string avatar)
        {
            
        }
    }
}
