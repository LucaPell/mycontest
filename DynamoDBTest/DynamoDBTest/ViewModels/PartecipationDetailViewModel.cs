﻿using DynamoDBTest.Models;
using DynamoDBTest.Helpers;
using DynamoDBTest.Services;
using DynamoDBTest.Controllers;

using System.Threading;
using System.Threading.Tasks;
using System;

namespace DynamoDBTest.ViewModels
{
    public class PartecipationDetailViewModel
    {
        public Partecipation partecipation;
        public Partecipation victoryPartecipation;
        public RetryCommand RetryAddVoteCommand;
        public RetryCommand RetryRemoveVoteCommand;
        public RetryCommand RetryLoadItemCommand;
        public RetryCommand RetryLoadVictoryPartecipationCommand;
        public RetryCommand RetryLoadVictoryPartecipationNoSaveCommand;

        public PartecipationService service;

        public event EventHandler ItemLoaded;

        public PartecipationDetailViewModel(Partecipation partecipation = null)
        {
            if(partecipation != null)
            {
                this.partecipation = partecipation;
            }

            RetryAddVoteCommand = new RetryCommand(AddVote, 3, 4000, UpdateServiceWithNewToken);
            RetryRemoveVoteCommand = new RetryCommand(RemoveVote, 3, 4000, UpdateServiceWithNewToken);
            RetryLoadItemCommand = new RetryCommand<string>(LoadItem, 3, 5000, UpdateServiceWithNewToken);
            RetryLoadVictoryPartecipationCommand = new RetryCommand<string>(LoadVictoryPartecipation, 3, 5000, UpdateServiceWithNewToken);
            RetryLoadVictoryPartecipationNoSaveCommand = new RetryCommand<string>(LoadVictoryPartecipationNoSave, 3, 5000, UpdateServiceWithNewToken);
        }

        private void UpdateServiceWithNewToken(CancellationToken token)
        {
            if (service == null)
                service = new PartecipationService(token);
            else
                service.UpdateToken(token);
        }

        async Task AddVote()
        {
            StringWrapper idTypeEmail;
            User user = LoginController.GetInstance().CurrentUser;
            idTypeEmail = new StringWrapper(partecipation.Id, "Partecipation", user.Email);
            partecipation.Votes.Add(user.Email);
            if (!await service.AddVote(idTypeEmail))
            {
                partecipation.Votes.Remove(user.Email);
            }
        }

        async Task RemoveVote()
        {
            StringWrapper idTypeEmail;
            User user = LoginController.GetInstance().CurrentUser;
            idTypeEmail = new StringWrapper(partecipation.Id, "Partecipation", user.Email);
            partecipation.Votes.Remove(user.Email);
            if (!await service.RemoveVote(idTypeEmail))
            {
                partecipation.Votes.Add(user.Email);
            }
        }

        async Task LoadItem(string itemId)
        {
            victoryPartecipation = await service.Get(itemId);
            ItemLoaded(this, new EventArgs());
        }

        async Task LoadVictoryPartecipation(string contestId)
        {
            Partecipation temp = await service.GetVictoryPartecipation(contestId);
            temp.Id = temp.Id + "victory";
            temp.Winner = 1;
            temp.SortingDate = DateTime.Now;
            victoryPartecipation = temp;
            ItemLoaded(this, new EventArgs());
            await service.AddVictory(victoryPartecipation);
        }

        async Task LoadVictoryPartecipationNoSave(string contestId)
        {
            Partecipation temp = await service.GetVictoryPartecipation(contestId);
            temp.Id = temp.Id + "victory";
            temp.SortingDate = DateTime.Now;
            temp.Winner = 1;

            await service.AddVictory(temp);
        }
    }
}
