﻿using System.Windows.Input;

using DynamoDBTest.Helpers;

namespace DynamoDBTest.ViewModels
{
    public class GlobalViewModel : BaseViewModel
    {
        public GlobalViewModel()
        {
            Title = "About";

            OpenWebCommand = new Command(() => Plugin.Share.CrossShare.Current.OpenBrowser("https://xamarin.com/platform"));
        }

        public ICommand OpenWebCommand { get; }
    }
}