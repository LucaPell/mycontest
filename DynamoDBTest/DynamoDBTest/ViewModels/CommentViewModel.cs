﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Threading;
using System.Collections.ObjectModel;
using System.Diagnostics;

using DynamoDBTest.Helpers;
using DynamoDBTest.Models;
using DynamoDBTest.Services;
using DynamoDBTest.Controllers;
using Amazon.DynamoDBv2.Model;
using System.Linq;

namespace DynamoDBTest.ViewModels
{
    class CommentViewModel : ICollectionViewModel<Comment, CommentService>
    {
        public RetryCommand RetryAddCommentCommand;
        public RetryCommand RetryAddLikeCommand;
        public RetryCommand RetryRemoveLikeCommand;
        public UserService userService;

        public CommentViewModel()
        {
            Title = "Comments";
            Items = new ObservableCollection<Comment>();
            RetryLoadItemsCommand = new RetryCommand<string>(LoadAllByItemCommented, 3, 4000, UpdateServiceWithNewToken);
            RetryAddCommentCommand = new RetryCommand<Comment>(AddComment, 3, 4000, UpdateServiceWithNewToken);
            RetryAddLikeCommand = new RetryCommand<int>(AddLike, 3, 4000, UpdateServiceWithNewToken);
            RetryRemoveLikeCommand = new RetryCommand<int>(RemoveLike, 3, 4000, UpdateServiceWithNewToken);
        }

        private void UpdateServiceWithNewToken(CancellationToken token)
        {
            if (service == null || base.RefreshOnNextCommand)
                service = new CommentService(token);
            else
                service.UpdateToken(token);

            if (userService == null || base.RefreshOnNextCommand)
                userService = new UserService(token);
            else
                userService.UpdateToken(token);
        }

        public override void Dispose()
        {

        }

        async Task LoadAllByItemCommented(string itemId)
        {
            IEnumerable<Comment> items = new List<Comment>();
            var avatarIds = new HashSet<string>();
            var avatars = new Dictionary<string, AttributeValue>();
            AttributeValue value;
            try
            {
                items = await service.GetAllByItemCommented(itemId);
                items.Reverse();
                foreach(Comment comment in items)
                {
                    avatarIds.Add(comment.CreatorUser);
                }
                try
                {
                    avatars = await userService.GetAvatars(avatarIds);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }

                foreach (Comment comment in items)
                {
                    if (avatars.TryGetValue(comment.CreatorUser, out value))
                    {
                        comment.Avatar = value.S;
                    }
                }

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                UpdateCollection(items);
            }
        }

        async Task AddComment(Comment comment)
        {
            await service.Add(comment);
            comment.Avatar = LoginController.GetInstance().CurrentUser.Avatar;
            Items.Insert(0, comment);
        }

        async Task AddLike(int position)
        {
            User user = LoginController.GetInstance().CurrentUser;
            Comment item = Items[position];
            item.Votes.Add(user.Email);
            StringWrapper idAndEmail = new StringWrapper(item.Id, user.Email);
            if (!await service.AddVote(idAndEmail))
            {
                item.Votes.Remove(user.Email);
            }
        }

        async Task RemoveLike(int position)
        {
            User user = LoginController.GetInstance().CurrentUser;
            Comment item = Items[position];
            item.Votes.Remove(user.Email);
            StringWrapper idAndEmail = new StringWrapper(item.Id, user.Email);
            if (!await service.RemoveVote(idAndEmail))
            {
                item.Votes.Add(user.Email);
            }
        }
    }
}
