﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

using DynamoDBTest.Controllers;

namespace DynamoDBTest.Helpers
{
    class AuthenticationLoopCheck
    {
        const int BAD_CREDENTIALS = 1;
        const int LOGIN_EXCEPTION = 2;
        const int LOGIN_TIMEOUT = 3;
        const int EMAIL_NOT_CONFIRMED = 4;

        private static AuthenticationLoopCheck SingletonInstance;
        bool Continue;

        public event LoginLoopErrorHandler LoginLoopError;
        public delegate void LoginLoopErrorHandler(int errorType, string message);

        public static AuthenticationLoopCheck GetInstance()
        {
            if(SingletonInstance == null)
            {
                SingletonInstance = new AuthenticationLoopCheck();
            }
            return SingletonInstance;
        }

        private void OnLoginTerminated(LoginController controller, int result, string message, string email)
        {
            switch (result)
            {
                case LoginController.BAD_CREDENTIALS:
                    LoginLoopError(BAD_CREDENTIALS, null);
                    break;
                case LoginController.LOGIN_ERROR:
                    LoginLoopError(LOGIN_EXCEPTION, "Connessione ad internet assente");
                    break;
                case LoginController.EMAIL_NOT_CONFIRMED:
                    LoginLoopError(EMAIL_NOT_CONFIRMED, "Devi confermare la tua email");
                    break;
                default:
                    break;
            }
        }

        private void OnLoginTimeout(object sender, EventArgs a)
        {
            CommandCompletedEventArgs args = a as CommandCompletedEventArgs;
            if (args.Status == CommandStates.MaxRetryReached)
            {
                LoginLoopError(LOGIN_TIMEOUT, null);
            }
        }

        public void Start()
        {
            Continue = true;

            Task.Run(async () => {
                while (Continue)
                {
                    await Task.Delay(15000);

                    DoOne();
                }
            });
        }

        public void Stop()
        {
            Continue = false;
        }

        public void DoOne()
        {
            if (LoginController.GetInstance().CurrentUser != null)
            {
                LoginController.GetInstance().LoginTerminated += new LoginController.LoginTerminatedHandler(OnLoginTerminated);
                LoginController.GetInstance().RetryLoginCommand.OperationCompleted += OnLoginTimeout;

                LoginController.GetInstance().RetryLoginCommand.Execute(null);
            }
        }
    }
}
