﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DynamoDBTest.Helpers
{
    public class CommandCompletedEventArgs : EventArgs
    {
        public CommandStates Status;
        public List<Exception> Exceptions;
        public int NumberOfRetry;
    }

    public enum CommandStates { Completed, MaxRetryReached , Faulted};
}
