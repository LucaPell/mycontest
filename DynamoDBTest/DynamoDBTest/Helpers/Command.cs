﻿using System;
using System.Collections.Generic;
using System.Windows.Input;
using System.Threading;
using System.Threading.Tasks;
using DynamoDBTest.Models;

namespace DynamoDBTest.Helpers
{
    public class RetryCommand<T> : RetryCommand
    {
        public RetryCommand(Func<T, Task> execute, int maxNumberOfRetry, int timeoutMillis, Action<CancellationToken> updateToken) : base((object o) => execute((T)o), maxNumberOfRetry, timeoutMillis, updateToken)
        {
            if (execute == null)
                throw new ArgumentNullException(nameof(execute));
        }
    }

    public class RetryCommand : ICommand
    {
        int MaxNumberOfRetry;
        int NumberOfRetry;
        int TimeoutMillis;
        public event EventHandler OperationCompleted;
        public Func<object, Task> exe;
        public Func<object, bool> vexe;
        readonly Func<object, bool> canExecute;
        Action<CancellationToken> UpdateToken;
        bool IsBusy;
        List<Exception> Exceptions;
        CommandCompletedEventArgs Args;

        public RetryCommand(Func<Task> execute, int maxNumberOfRetry, int timeoutMillis, Action<CancellationToken> updateToken) : this((async (object o) => { await execute(); }), maxNumberOfRetry, timeoutMillis, updateToken)
        {
            if (execute == null)
                throw new ArgumentNullException(nameof(execute));
        }

        public RetryCommand(Func<object, Task> execute, int maxNumberOfRetry, int timeoutMillis, Action<CancellationToken> updateToken)
        {
            if (execute == null)
                throw new ArgumentNullException(nameof(execute));
            if (maxNumberOfRetry < 1)
                throw new ArgumentOutOfRangeException(nameof(maxNumberOfRetry));

            //MaxNumberOfRetry = maxNumberOfRetry;
            MaxNumberOfRetry = 1;
            NumberOfRetry = 0;
            //TimeoutMillis = timeoutMillis;
            TimeoutMillis = 20000;
            this.exe = execute;
            this.UpdateToken = updateToken;
            IsBusy = false;
            Exceptions = new List<Exception>();
        }

        private void CleanEventHandlers()
        {
            if (OperationCompleted != null)
            {
                foreach (Delegate d in OperationCompleted.GetInvocationList())
                {
                    OperationCompleted -= (EventHandler)d;
                }
            }
        }

        private void RetryLoop(object parameter)
        {
            if (NumberOfRetry >= MaxNumberOfRetry)
            {
                Args.Status = CommandStates.MaxRetryReached;
                EndCommand();
            }

            NumberOfRetry++;
            bool FirstTaskEnded = false;
            CancellationTokenSource source = new CancellationTokenSource();
            UpdateToken(source.Token);

            Task t = exe.Invoke(parameter);

            t.ContinueWith((Task task) => {
                if (task.Exception != null)
                {
                    Exceptions.Add(task.Exception);
                }

                if (!FirstTaskEnded)
                {
                    FirstTaskEnded = true;

                    if(task.Status == TaskStatus.RanToCompletion)
                    {
                        Args.Status = CommandStates.Completed;
                        EndCommand();
                    }
                    else
                    {
                        //RetryLoop(parameter);
                        Args.Status = CommandStates.MaxRetryReached;
                        EndCommand();
                    }
                }
            });

            Task.Delay(TimeoutMillis).ContinueWith((Task task) => {
                if (!FirstTaskEnded)
                {
                    FirstTaskEnded = true;
                    source.Cancel();

                    //RetryLoop(parameter);
                    Args.Status = CommandStates.MaxRetryReached;
                    EndCommand();
                }
            });
        }

        void EndCommand()
        {
            IsBusy = false;
            NumberOfRetry = 0;
            Args.Exceptions = Exceptions;
            Args.NumberOfRetry = NumberOfRetry;
            OperationCompleted(this, Args);

            Exceptions.Clear();
            CleanEventHandlers();
        }

        public void Execute(object parameter = null)
        {
            if (IsBusy)
                return;
            IsBusy = true;
            Args = new CommandCompletedEventArgs();
            RetryLoop(parameter);
        }

        public bool CanExecute(object parameter)
        {
            if (canExecute != null)
                return canExecute(parameter);

            return true;
        }

        public event EventHandler CanExecuteChanged;

        public void ChangeCanExecute()
        {
            EventHandler changed = CanExecuteChanged;
            if (changed != null)
                changed(this, EventArgs.Empty);
        }
    }

    public class Command<T> : Command
    {
        public Command(Action<T> execute) : base(o => execute((T)o))
        {
            if (execute == null)
                throw new ArgumentNullException(nameof(execute));
        }

        public Command(Action<T> execute, Func<T, bool> canExecute) : base(o => execute((T)o), o => canExecute((T)o))
        {
            if (execute == null)
                throw new ArgumentNullException(nameof(execute));
            if (canExecute == null)
                throw new ArgumentNullException(nameof(canExecute));
        }
    }

    public class Command : ICommand
    {
        readonly Func<object, bool> canExecute;
        public readonly Action<object> execute;

        public Command(Action<object> execute)
        {
            if (execute == null)
                throw new ArgumentNullException(nameof(execute));

            this.execute = execute;
        }

        public Command(Action execute) : this(o => execute())
        {
            if (execute == null)
                throw new ArgumentNullException(nameof(execute));
        }

        public Command(Action<object> execute, Func<object, bool> canExecute) : this(execute)
        {
            if (canExecute == null)
                throw new ArgumentNullException(nameof(canExecute));

            this.canExecute = canExecute;
        }

        public Command(Action execute, Func<bool> canExecute) : this(o => execute(), o => canExecute())
        {
            if (execute == null)
                throw new ArgumentNullException(nameof(execute));
            if (canExecute == null)
                throw new ArgumentNullException(nameof(canExecute));
        }

        public bool CanExecute(object parameter)
        {
            if (canExecute != null)
                return canExecute(parameter);

            return true;
        }

        public event EventHandler CanExecuteChanged;

        public virtual void Execute(object parameter)
        {
            execute(parameter);
        }


        public void ChangeCanExecute()
        {
            EventHandler changed = CanExecuteChanged;
            if (changed != null)
                changed(this, EventArgs.Empty);
        }
    }
}
