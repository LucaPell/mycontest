﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;

namespace DynamoDBTest.Helpers
{
    class PrettyDate
    {
        public static string GetPrettyDate(DateTime d)
        {
            //Quanto tempo è passato
            TimeSpan s = DateTime.Now.Subtract(d);

            bool sameYear = (DateTime.Now.Year == d.Year);
            int dayDiff = (int)s.TotalDays;
            int secDiff = (int)s.TotalSeconds;
            string monthName = CultureInfo.CurrentCulture.DateTimeFormat.GetAbbreviatedMonthName(d.Month);

            //Caso errore
            if (dayDiff < 0)
            {
                return "";
            }
            else if (dayDiff == 0)
            {
                //meno di un minuto, quindi conto in secondi
                if (secDiff < 60)
                {
                    return secDiff.ToString() + "s";
                }
                //meno di un'ora, quindi conto in minuti
                else if (secDiff < 3600)
                {
                    return string.Format("{0}min",
                        Math.Floor((double)secDiff / 60));
                }
                //giorno -> ore
                else if (secDiff < 86400)
                {
                    return string.Format("{0}h",
                        Math.Floor((double)secDiff / 3600));
                }
            }
            //giorni fa, va sistemato in caso di localizzazione con altre lingue
            else if (dayDiff < 7)
            {
                return string.Format("{0}g",
                    dayDiff);
            }
            else if (sameYear)
            {
                return string.Format(d.Day.ToString() + monthName);
            }
            else
            {
                return d.Date.ToShortDateString();
            }
            return "";
        }
    }
}
