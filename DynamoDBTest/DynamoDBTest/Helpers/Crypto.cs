﻿using System.Security.Cryptography;
using System.IO;

namespace DynamoDBTest.Helpers
{
    class Crypto
    {
        public string Encrypt(string value)
        {
            return System.Text.Encoding.Default.GetString(new SHA256Managed().ComputeHash(GenerateStreamFromString(value)));
        }

        private static Stream GenerateStreamFromString(string s)
        {
            MemoryStream stream = new MemoryStream();
            StreamWriter writer = new StreamWriter(stream);
            writer.Write(s);
            writer.Flush();
            stream.Position = 0;
            return stream;
        }
    }
}
