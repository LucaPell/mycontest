﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Diagnostics;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Amazon.DynamoDBv2.Model;
using DynamoDBTest.Models;
using DynamoDBTest.ViewModels;
using DynamoDBTest.Services;

namespace DynamoDBTest.Helpers
{
    public class DynamoDBUtility
    {
        private List<AbstractSortableByDateItem> Items;
        private CancellationToken Token;

        public DynamoDBUtility()
        {
            Items = new List<AbstractSortableByDateItem>();
        }

        public DynamoDBUtility(CancellationToken token) : this()
        {
            Token = token;
        }

        public async Task<IEnumerable<AbstractSortableByDateItem>> ConvertBatchResponse(Dictionary<string, List<Dictionary<string, AttributeValue>>> data)
        {
            await DoItWithReflection(data);

            Items.Sort();

            return Items;
        }

        private async Task DoItWithReflection(Dictionary<string, List<Dictionary<string, AttributeValue>>> data)
        {
            foreach(string tableName in data.Keys)
            {
                //determina il tipo della classe
                Type objType = Assembly.GetExecutingAssembly().GetType("DynamoDBTest.Models." + tableName);
                bool share = false;
                List<Share> incompletedShares;

                if (objType.Equals(typeof(Share)))
                {
                    incompletedShares = new List<Share>();
                    share = true;
                } else
                {
                    incompletedShares = null;
                    share = false;
                }

                foreach (Dictionary<string, AttributeValue> itemData in data.GetValueOrDefault(tableName))
                {
                    //istanzia l'oggetto
                    object item = Activator.CreateInstance(objType);

                    foreach (string attributeName in itemData.Keys)
                    {
                        //trova l'attributo e assegna il valore
                        PropertyInfo property = objType.GetProperty(attributeName);
                        if(property == null)
                        {;
                            property = typeof(AbstractSortableByDateItem).GetProperty(attributeName);
                        }
                        property.SetValue(item, ConvertValue(itemData.GetValueOrDefault(attributeName), property.PropertyType));
                    }
                    //aggiungi l'oggetto alla collezione
                    if (share)
                    {
                        incompletedShares.Add((Share)item);
                    }
                    else
                    {
                        Items.Add((AbstractSortableByDateItem)item);
                    }
                }

                if (share)
                {
                    ShareService service = new ShareService(Token);

                    Items.AddRange(await service.CompleteShares(incompletedShares));
                }
            }
        }

        private object ConvertValue(AttributeValue valueData, Type attributeType)
        {
            object value = null;

            if(attributeType.Equals(typeof(string)))
            {
                value = valueData.S;
            }
            else if (attributeType.Equals(typeof(DateTime)))
            {
                value = DateTime.Parse(valueData.S);
            }
            else if (attributeType.Equals(typeof(List<string>)))
            {
                value = valueData.SS;
            }
            else if (attributeType.Equals(typeof(int)))
            {
                value = Int32.Parse(valueData.N);
            }
            else if (attributeType.Equals(typeof(Dictionary<string, string>)))
            {
                var tmp = new Dictionary<string, string>();
                foreach (string keyName in valueData.M.Keys)
                {
                    tmp.Add(keyName, valueData.M.GetValueOrDefault(keyName).S);
                }
                value = tmp;
            }
            else if (attributeType.Equals(typeof(bool)))
            {
                value = valueData.BOOL;
            }

            return value;
        }

        private void DoItSimpleButWithCoupling(Dictionary<string, List<Dictionary<string, AttributeValue>>> data)
        {
            List<Contest> contests = new List<Contest>();
            List<Partecipation> partecipations = new List<Partecipation>();
            List<Share> shares = new List<Share>();

            foreach(Dictionary<string, AttributeValue> i in data.GetValueOrDefault("Contest"))
            {
                Contest contest = new Contest();

                contest.Id = i.GetValueOrDefault("Id").S;
                contest.CreatorUser = i.GetValueOrDefault("CreatorUser").S;
                contest.CreatorUserName = i.GetValueOrDefault("CreatorUserName").S;
                contest.EndDate = DateTime.Parse(i.GetValueOrDefault("EndDate").S);

                var tmp = i.GetValueOrDefault("FollowedByUsers").M;
                foreach(string followedByUserId in tmp.Keys)
                {
                    contest.FollowedByUsers.Add(followedByUserId, tmp.GetValueOrDefault(followedByUserId).S);
                }

                contest.Name = i.GetValueOrDefault("Name").S;
                contest.SortingDate = DateTime.Parse(i.GetValueOrDefault("SortingDate").S);

                AttributeValue val = i.GetValueOrDefault("Description");
                if (val != null)
                {
                    contest.Description = val.S;
                }

                val = i.GetValueOrDefault("HasPartecipations");
                if (val != null)
                {
                    contest.HasPartecipations.AddRange(val.SS);
                }

                val = i.GetValueOrDefault("Votes");
                if (val != null)
                {
                    contest.Votes.AddRange(val.SS);
                }

                val = i.GetValueOrDefault("Comments");
                if (val != null)
                {
                    contest.Comments.AddRange(val.SS);
                }

                val = i.GetValueOrDefault("Banner");
                if (val != null)
                {
                    contest.Banner = val.S;
                }

                val = i.GetValueOrDefault("DeleteHash");
                if (val != null)
                {
                    contest.DeleteHash = val.S;
                }

                val = i.GetValueOrDefault("Shares");
                if (val != null)
                {
                    contest.Shares = Int32.Parse(val.N);
                }

                val = i.GetValueOrDefault("VictoryPartecipation");
                if (val != null)
                {
                    contest.VictoryPartecipation = val.S;
                }

                contests.Add(contest);
            }

            foreach (Dictionary<string, AttributeValue> i in data.GetValueOrDefault("Partecipation"))
            {
                Partecipation partecipation = new Partecipation();

                partecipation.Id = i.GetValueOrDefault("Id").S;
                partecipation.CreatorUser = i.GetValueOrDefault("CreatorUser").S;
                partecipation.CreatorUserName = i.GetValueOrDefault("CreatorUserName").S;
                partecipation.DeleteHash = i.GetValueOrDefault("DeleteHash").S;
                partecipation.Media = i.GetValueOrDefault("Media").S;
                partecipation.PartecipatingToContest = i.GetValueOrDefault("PartecipatingToContest").S;
                partecipation.PartecipatingToContestName = i.GetValueOrDefault("PartecipatingToContestName").S;
                partecipation.SortingDate = DateTime.Parse(i.GetValueOrDefault("SortingDate").S);

                AttributeValue val = i.GetValueOrDefault("Content");
                if (val != null)
                {
                    partecipation.Content = val.S;
                }

                val = i.GetValueOrDefault("Votes");
                if (val != null)
                {
                    partecipation.Votes.AddRange(val.SS);
                }

                val = i.GetValueOrDefault("VotesNumber");
                if (val != null)
                {
                    partecipation.VotesNumber = Int32.Parse(val.N);
                }

                val = i.GetValueOrDefault("Shares");
                if (val != null)
                {
                    partecipation.Shares = Int32.Parse(val.N);
                }

                val = i.GetValueOrDefault("Comments");
                if (val != null)
                {
                    partecipation.Comments.AddRange(val.SS);
                }

                partecipations.Add(partecipation);
            }

            foreach (Dictionary<string, AttributeValue> i in data.GetValueOrDefault("Share"))
            {
                Share share = new Share();

                share.Id = i.GetValueOrDefault("Id").S;
                share.ContestId = i.GetValueOrDefault("ContestId").S;
                share.CreatorUser = i.GetValueOrDefault("CreatorUser").S;
                share.CreatorUserName = i.GetValueOrDefault("CreatorUserName").S;

                AttributeValue val = i.GetValueOrDefault("Description");
                if (val != null)
                {
                    share.Description = val.S;
                }

                share.MultipleShares = i.GetValueOrDefault("MultipleShares").BOOL;
                share.OriginalContent = i.GetValueOrDefault("OriginalContent").S;
                share.OriginalDate = DateTime.Parse(i.GetValueOrDefault("OriginalDate").S);
                share.OriginalUser = i.GetValueOrDefault("OriginalUser").S;
                share.OriginalUserName = i.GetValueOrDefault("OriginalUserName").S;
                share.SortingDate = DateTime.Parse(i.GetValueOrDefault("SortingDate").S);

                val = i.GetValueOrDefault("PartecipationId");
                if (val != null)
                {
                    share.PartecipationId = val.S;
                }

                val = i.GetValueOrDefault("ToContest");
                if (val != null)
                {
                    share.ContestName = val.S;
                }

                val = i.GetValueOrDefault("Avatar");
                if (val != null)
                {
                    share.Avatar = val.S;
                }

                val = i.GetValueOrDefault("Comments");
                if (val != null)
                {
                    share.Comments.AddRange(val.SS);
                }

                val = i.GetValueOrDefault("Votes");
                if (val != null)
                {
                    share.Votes.AddRange(val.SS);
                }

                shares.Add(share);
            }

            Items.AddRange(contests);
            Items.AddRange(partecipations);
            Items.AddRange(shares);
        }
    }
}
