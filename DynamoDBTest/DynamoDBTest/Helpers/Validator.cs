﻿using System.Collections.Generic;

namespace DynamoDBTest.Helpers
{
    class Validator
    {
        string Email;
        string Username;
        string Password;
        string PasswordConfirm;
        public List<string> ErrorMessages { get; }

        public Validator(string email, string username, string password, string passwordConfirm)
        {
            Email = email;
            Username = username;
            Password = password;
            PasswordConfirm = passwordConfirm;
            ErrorMessages = new List<string>();
        }

        public Validator(string email, string password)
        {
            Email = email;
            Password = password;
            ErrorMessages = new List<string>();
        }

        public Validator(string email)
        {
            Email = email;
            ErrorMessages = new List<string>();
        }

        public Validator(string email, string password, string passwordConfirm)
        {
            Email = email; 
            Password = password;
            PasswordConfirm = passwordConfirm;
            ErrorMessages = new List<string>();
        }

        public bool ValidatePassReset()
        {
            if (ValidateEmail() && ValidatePassword())
                return true;
            else
                return false;
        }

        public bool Validate()
        {
            if (ValidateEmail() && ValidateUsername() && ValidatePassword())
                return true;
            else
                return false;
        }

        public bool ValidateE()
        {
            if (ValidateEmail())
                return true;
            else
                return false;
        }

        public bool ValidateLogin()
        {
            if (Email != null && !Email.Equals("") && Password != null && !Password.Equals(""))
            {
                return true;
            }
            else
            {
                ErrorMessages.Add("Compila tutti i campi");
                return false;
            }
        }

        private bool ValidateEmail()
        {
            if (Email != null && !Email.Equals(""))
            {
                if(Email.Contains("@"))
                {
                    return true;
                } else
                {
                    ErrorMessages.Add("L'email inserita non è valida");
                    return false;
                }
                
            }
            else
            {
                ErrorMessages.Add("Compila tutti i campi");
                return false;
            }
        }

        private bool ValidateUsername()
        {
            if (Username != null && !Username.Equals(""))
            {
                return true;
            }
            else
            {
                ErrorMessages.Add("Compila tutti i campi");
                return false;
            }
        }

        private bool ValidatePassword()
        {
            if (Password != null && !Password.Equals(""))
            {
                if(Password.Length >= 6)
                {
                    if (Password.Equals(PasswordConfirm))
                    {
                        return true;
                    }
                    else
                    {

                        ErrorMessages.Add("Le due password non corrispondono");
                        return false;
                    }
                }
                else
                {
                    ErrorMessages.Add("La password deve essere lunga almeno 6 caratteri");
                    return false;
                }
            }
            else
            {
                ErrorMessages.Add("Compila tutti i campi");
                return false;
            }
        }
    }
}
