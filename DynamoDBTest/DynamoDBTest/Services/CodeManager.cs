﻿using System;
using System.Net;
using System.Net.Mail;
using System.Security.Cryptography.X509Certificates;

namespace DynamoDBTest.Services
{
    class CodeManager
    {
        public void EnterNewRegistrationEntry(string email)
        {
            string code = new Random().Next(100000, 999999).ToString();
            Settings.Set(email, code);
            SendRegistrationEmail(email, code);
        }

        public void EnterNewResetPassEntry(string email)
        {
            string code = new Random().Next(100000, 999999).ToString();
            Settings.Set(email, code);
            SendPassResetEmail(email, code);
        }

        public bool CheckCode(string email, string code)
        {
            try
            {
                string savedCode = Settings.Get(email);
                return savedCode == code;
            } catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }
            
        }
        
        private void SendPassResetEmail(string emailTo, string code)
        {
            try
            {
                MailMessage mail = new MailMessage();
                SmtpClient SmtpServer = new SmtpClient("smtp.gmail.com");
                mail.From = new MailAddress("myc.passreset@gmail.com");
                mail.To.Add(emailTo);
                mail.Subject = "MyC password reset";
                mail.Body = "Per favore inserisci nella tua app il codice ricevuto in questa e-mail per confermare la riempostazione della password\n\n" + code;
                SmtpServer.Port = 587;
                SmtpServer.Credentials = new System.Net.NetworkCredential("myc.passreset@gmail.com", "tAntOnonM3laricordOoo");
                SmtpServer.EnableSsl = true;
                ServicePointManager.ServerCertificateValidationCallback = delegate (object sender, X509Certificate certificate, X509Chain chain, System.Net.Security.SslPolicyErrors sslPolicyErrors) {
                    return true;
                };
                SmtpServer.Send(mail);
            }

            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        private void SendRegistrationEmail(string emailTo, string code)
        {
            try
            {
                MailMessage mail = new MailMessage();
                SmtpClient SmtpServer = new SmtpClient("smtp.gmail.com");
                mail.From = new MailAddress("myc.emailconfirmation@gmail.com");
                mail.To.Add(emailTo);
                mail.Subject = "MyC account confirmation";
                mail.Body = "Per favore inserisci nella tua app il codice ricevuto in questa e-mail per confermare la tua registrazione\n\n" + code;
                SmtpServer.Port = 587;
                SmtpServer.Credentials = new System.Net.NetworkCredential("myc.emailconfirmation@gmail.com", "caccapupu");
                SmtpServer.EnableSsl = true;
                ServicePointManager.ServerCertificateValidationCallback = delegate (object sender, X509Certificate certificate, X509Chain chain, System.Net.Security.SslPolicyErrors sslPolicyErrors) {
                    return true;
                };
                SmtpServer.Send(mail);
            }

            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
    }
}
