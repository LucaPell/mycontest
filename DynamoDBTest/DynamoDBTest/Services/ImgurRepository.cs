﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Net;
using System.Threading.Tasks;
using System.IO;
using System.Collections.Specialized;
using System.Net.Http;
using System.Net.Http.Headers;

namespace DynamoDBTest.Services
{
    class ImgurRepository
    {
        const string ClientID = "90bc8cb3a0e5443";
        const string ClientSecret = "4e58e1e5c1d0c5fffabb80b96dcc355150456536";
        const string ClientID2 = "64b1aea44b3b432";
        const string ClientSecret2 = "e68107dcba645334b6c60612948d44c787b82428";
        const string UploadURL = "https://api.imgur.com/3/image";
        const string DeleteURL = "https://api.imgur.com/3/image/";
        WebClient client;

        //private struct ImgurResponseObj
        //{
        //    [Newtonsoft.Json.JsonExtensionDataAttribute]
        //    public IDictionary<string, Newtonsoft.Json.Linq.JToken> data;
        //    public ImgurImageInfo data;
        //    public string success;
        //    public string status;
        //}

        //private struct ImgurImageInfo
        //{
        //    public string deletehash;
        //    public string link;
        //}

        public ImgurRepository()
        {
            client = new WebClient();
        }

        public async Task<Dictionary<string, string>> UploadImage(byte[] stream)
        {
            var values = new NameValueCollection
            {
                { "key", ClientSecret },
                { "image", Convert.ToBase64String(stream) }
            };

            client.Headers.Add("Authorization", "Client-ID " + ClientID);
            try
            {
                var response = await client.UploadValuesTaskAsync(UploadURL, values);

                //ImgurResponseObj responseObj = Newtonsoft.Json.JsonConvert.DeserializeObject<ImgurResponseObj>(Encoding.UTF8.GetString(response));
                ImgurResponse responseObj = Newtonsoft.Json.JsonConvert.DeserializeObject<ImgurResponse>(Encoding.UTF8.GetString(response));
                Dictionary<string, string> imageUrls = new Dictionary<string, string>();

                if (responseObj.success)
                {
                    imageUrls.Add("link", responseObj.data.link);
                    imageUrls.Add("deletehash", responseObj.data.deletehash);
                }
                else
                {
                    return null;
                }

                return imageUrls;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return null;
            }
        }

        public async Task DeleteImageAsync(string deleteHash)
        {
            //client.Headers.Add("Authorization", "Client-ID " + ClientID);

            //var values = new NameValueCollection
            //{
            //    { "key", ClientSecret },
            //};
                try
                {
                    using (HttpClient client = new HttpClient())
                    {
                        client.DefaultRequestHeaders.Add("Authorization", "Client-ID " + ClientID);
                        var response = await client.DeleteAsync(new Uri(DeleteURL + deleteHash));
                        var c = response.Content.ReadAsStringAsync();
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }
        }

        public class ImgurRespData
        {
            public string id { get; set; }
            public object title { get; set; }
            public object description { get; set; }
            public int datetime { get; set; }
            public string type { get; set; }
            public bool animated { get; set; }
            public int width { get; set; }
            public int height { get; set; }
            public int size { get; set; }
            public int views { get; set; }
            public int bandwidth { get; set; }
            public object vote { get; set; }
            public bool favorite { get; set; }
            public object nsfw { get; set; }
            public object section { get; set; }
            public object account_url { get; set; }
            public int account_id { get; set; }
            public bool is_ad { get; set; }
            public bool in_most_viral { get; set; }
            public List<string> tags { get; set; }
            public int ad_type { get; set; }
            public string ad_url { get; set; }
            public bool in_gallery { get; set; }
            public string deletehash { get; set; }
            public string name { get; set; }
            public string link { get; set; }
        }

        public class ImgurResponse
        {
            public ImgurRespData data { get; set; }
            public bool success { get; set; }
            public int status { get; set; }
        }
    }
}
