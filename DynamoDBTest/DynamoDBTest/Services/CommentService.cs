﻿using System.Collections.Generic;
using DynamoDBTest.Models;
using System.Threading.Tasks;
using System.Threading;
using Amazon.DynamoDBv2.Model;

namespace DynamoDBTest.Services
{
    class CommentService
    {
        private CrudRepository<Comment> repo;

        public CommentService()
        {
            repo = new CrudRepository<Comment>();
        }

        public CommentService(CancellationToken token)
        {
            repo = new CrudRepository<Comment>(token);
        }

        public void UpdateToken(CancellationToken token)
        {
            repo.UpdateToken(token);
        }

        public async Task<Comment> Get(string id)
        {
            return await repo.GetAsync(id);
        }

        public async Task Add(Comment comment)
        {
            await repo.AddAsync(comment);

            var ExpressionAttributeValues = new Dictionary<string, AttributeValue>()
                     {
                     {":comment", new AttributeValue { SS = {comment.Id}}},
            };
            await repo.UpdateAttributesAsync(comment.ItemCommented, comment.ItemCommentedType, "ADD Comments :comment", "Id", ExpressionAttributeValues);
        }

        public async Task Update(Comment comment)
        {
            await repo.UpdateAsync(comment);
        }

        public async Task Delete(string id)
        {
            Comment comment = await repo.GetAsync(id);
            await this.Delete(comment);
        }

        public async Task Delete(Comment comment)
        {
            await repo.DeleteAsync(comment);
        }

        public async Task<bool> AddVote(StringWrapper idAndEmail)
        {
            //id                //tipo di tabella   //userid(email)
            return await repo.AddVoteAsync(idAndEmail.String1, "Comment", idAndEmail.String2);
        }

        public async Task<bool> RemoveVote(StringWrapper idAndEmail)
        {
            return await repo.RemoveVoteAsync(idAndEmail.String1, "Comment", idAndEmail.String2);
        }

        public async Task<IEnumerable<Comment>> GetAll()
        {
            return await repo.GetAllAsync();
        }

        public async Task<IEnumerable<Comment>> GetAllByItemCommented(string itemId)
        {
            return await repo.GetAllAsync(false, "ItemCommented", true, "ItemCommented", new string[] { itemId });
        }
    }
}
