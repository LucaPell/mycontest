﻿using System;
using System.Collections.Generic;
using DynamoDBTest.Models;
using System.Threading.Tasks;
using System.Threading;
using DynamoDBTest.Helpers;
using DynamoDBTest.Models;
using Amazon.DynamoDBv2.Model;

namespace DynamoDBTest.Services
{
    public class PartecipationService
    {
        private CrudRepository<Partecipation> repo;
        private Partecipation CurrentPartecipation;
        private string CurrentPartecipationId;
        public Command AddVoteCommand { get; set; }
        public Command RemoveVoteCommand { get; set; }
        CancellationToken Token;

        public PartecipationService()
        {
            repo = new CrudRepository<Partecipation>();
            CreateCommands();
        }

        public PartecipationService(CancellationToken token)
        {
            repo = new CrudRepository<Partecipation>(token);
            Token = token;
            CreateCommands();
        }

        private void CreateCommands()
        {
            AddVoteCommand = new Command<StringWrapper>(async (StringWrapper idAndEmail) => await AddVote(idAndEmail));
            RemoveVoteCommand = new Command<StringWrapper>(async (StringWrapper idAndEmail) => await RemoveVote(idAndEmail));

        }

        public void UpdateToken(CancellationToken token)
        {
            repo.UpdateToken(token);
            Token = token;
        }

        public void SetPartecipation(Partecipation partecipation)
        {
            CurrentPartecipation = partecipation;
        }

        public void SetPartecipation(string id)
        {
            CurrentPartecipationId = id;
        }

        public async Task<Partecipation> Get(string id)
        {
            return await repo.GetAsync(id);
        }

        public async Task Add(Partecipation partecipation)
        {
            await repo.AddAsync(partecipation);

            var ExpressionAttributeValues = new Dictionary<string, AttributeValue>()
                     {
                     {":partecipation", new AttributeValue { SS = {partecipation.Id}}},
            };
            await repo.UpdateAttributesAsync(partecipation.PartecipatingToContest, "Contest", "ADD HasPartecipations :partecipation", "Id", ExpressionAttributeValues);
            await repo.UpdateAttributesAsync(partecipation.CreatorUser, "User", "ADD Partecipations :partecipation", "Email", ExpressionAttributeValues);

            TimelineService Tservice = new TimelineService(Token);
            await Tservice.Add(partecipation.Id, partecipation.CreatorUser, partecipation.SortingDate, ItemType.Partecipation);
        }

        public async Task AddVictory(Partecipation partecipation)
        {
            await repo.AddAsync(partecipation);

            var ExpressionAttributeValues = new Dictionary<string, AttributeValue>()
                     {
                     {":partecipation", new AttributeValue { SS = {partecipation.Id}}},
            };
            await repo.UpdateAttributesAsync(partecipation.PartecipatingToContest, "Contest", "ADD HasPartecipations :partecipation", "Id", ExpressionAttributeValues);
            await repo.UpdateAttributesAsync(partecipation.CreatorUser, "User", "ADD Partecipations :partecipation", "Email", ExpressionAttributeValues);

            TimelineService Tservice = new TimelineService(Token);
            await Tservice.Add(partecipation.Id, partecipation.CreatorUser, partecipation.SortingDate, ItemType.Partecipation);
        }

        public async Task Update(Partecipation partecipation)
        {
            await repo.UpdateAsync(partecipation);
        }

        public async Task Delete(string id)
        {
            Partecipation partecipation = await repo.GetAsync(id);
            await this.Delete(partecipation);
        }

        public async Task Delete(Partecipation partecipation)
        {
            string deletehash = partecipation.DeleteHash;
            if (deletehash != null)
            {
                ImgurRepository imgurRepo = new ImgurRepository();
                await imgurRepo.DeleteImageAsync(deletehash);
            }
            await repo.DeleteAsync(partecipation);

            var ExpressionAttributeValues = new Dictionary<string, AttributeValue>()
                     {
                     {":partecipation", new AttributeValue { SS = {partecipation.Id}}},
            };
            await repo.UpdateAttributesAsync(partecipation.PartecipatingToContest, "Contest", "DELETE HasPartecipations :partecipation", "Id", ExpressionAttributeValues);
            await repo.UpdateAttributesAsync(partecipation.CreatorUser, "User", "DELETE Partecipations :partecipation", "Email", ExpressionAttributeValues);
            TimelineService Tservice = new TimelineService(Token);
            await Tservice.Delete(partecipation.Id);
            AuthenticationLoopCheck.GetInstance().DoOne();
        }

        public async Task DeleteVictory(Partecipation partecipation)
        {
            string deletehash = partecipation.DeleteHash;
            if (deletehash != null)
            {
                ImgurRepository imgurRepo = new ImgurRepository();
                await imgurRepo.DeleteImageAsync(deletehash);
            }
            await repo.DeleteAsync(partecipation);
            UserService Uservice = new UserService(Token);
            Uservice.SetUser(partecipation.CreatorUser);
            Uservice.RemovePartecipationCommand.Execute(partecipation.Id);

            TimelineService Tservice = new TimelineService(Token);
            await Tservice.Delete(partecipation.Id);
        }

        public async Task<IEnumerable<Partecipation>> GetAll()
        {
            return await repo.GetAllAsync(index: "Status", backward: true, key: "Status", keyValues: new string[] { "OK" });
        }

        public async Task<IEnumerable<Partecipation>> GetAllByContest(string contestId)
        {
            return await repo.GetAllAsync(false, "PartecipatingToContest", true, "PartecipatingToContest", new string[] { contestId });
        }

        public async Task<IEnumerable<Partecipation>> GetAllByUser(string userId)
        {
            return await repo.GetAllAsync(false, "CreatorUser", true, "CreatorUser", new string[] { userId });
        }

        private async Task<IEnumerable<Partecipation>> GetAllByUser(string[] userIds)
        {
            return await repo.GetAllAsync(false, "CreatorUser", true, "CreatorUser", userIds);
        }

        public async Task<bool> AddVote(StringWrapper idAndEmail)
        {
                                      //id                //tipo di tabella   //userid(email)
            return await repo.AddVoteAsync(idAndEmail.String1, "Partecipation", idAndEmail.String2);
        }

        public async Task<bool> RemoveVote(StringWrapper idAndEmail)
        {
            return await repo.RemoveVoteAsync(idAndEmail.String1, "Partecipation", idAndEmail.String2);
        }

        public async Task<Partecipation> GetVictoryPartecipation(string contestId)
        {
            var result = await repo.GetAllAsync(false, "VotesNumber", true, "PartecipatingToContest", new string[] { contestId }, 1);
            var enumerator = result.GetEnumerator();
            var dio = enumerator.Current;
            enumerator.MoveNext();
            return enumerator.Current;
        }

        private async Task CheckCurrentPartecipation()
        {
            if (CurrentPartecipation == null)
            {
                if (CurrentPartecipationId != null && CurrentPartecipationId.Length > 0)
                {
                    CurrentPartecipation = await this.Get(CurrentPartecipationId);
                }
                else
                {
                    throw new Exception("CurrentPartecipationId and CurrentPartecipation not set. At least one must be set before calling this method. Use SetPartecipation method.");
                }
            }
        }
    }
}
