﻿using System;
using System.Collections.Generic;
using DynamoDBTest.Models;
using System.Threading.Tasks;
using System.Threading;
using DynamoDBTest.Helpers;
using Amazon.DynamoDBv2.Model;
using System.IO;
using DynamoDBTest.Controllers;

namespace DynamoDBTest.Services
{
    public class UserService
    {
        private CrudRepository<User> repo;
        public User ManagedUser;
        public string ManagedUserId;
        public Command AddCreatedContestCommand { get; set; }
        public Command RemoveCreatedContestCommand { get; set; }
        public Command AddPartecipationCommand { get; set; }
        public Command RemovePartecipationCommand { get; set; }
        public Command AddShareCommand { get; set; }
        public Command RemoveShareCommand { get; set; }
        public Command AddFollowsUserCommand { get; set; }
        public Command RemoveFollowsUserCommand { get; set; }
        public Command AddFollowedByUserCommand { get; set; }
        public Command RemoveFollowedByUserCommand { get; set; }
        CancellationToken Token;

        public UserService()
        {
            repo = new CrudRepository<User>();
            CreateCommands();
        }

        public UserService(CancellationToken token) : this()
        {
            repo = new CrudRepository<User>(token);
            Token = token;
            CreateCommands();
        }

        private void CreateCommands()
        {
            AddCreatedContestCommand = new Command<string>(async (string ContestId) => await AddCreatedContest(ContestId));
            RemoveCreatedContestCommand = new Command<string>(async (string ContestId) => await RemoveCreatedContest(ContestId));
            AddPartecipationCommand = new Command<string>(async (string PartecipationId) => await AddPartecipation(PartecipationId));
            RemovePartecipationCommand = new Command<string>(async (string PartecipationId) => await RemovePartecipation(PartecipationId));
            AddShareCommand = new Command<string>(async (string ShareId) => await AddShare(ShareId));
            RemoveShareCommand = new Command<string>(async (string ShareId) => await RemoveShare(ShareId));


            AddFollowsUserCommand = new Command<User>(async (User user) => await AddFollowsUser(user));
            RemoveFollowsUserCommand = new Command<string>(async (string UserId) => await RemoveFollowsUser(UserId));
            AddFollowedByUserCommand = new Command<User>(async (User user) => await AddFollowedByUser(user));
            RemoveFollowedByUserCommand = new Command<string>(async (string UserId) => await RemoveFollowedByUser(UserId));
        }

        public void UpdateToken(CancellationToken token)
        {
            repo.UpdateToken(token);
            Token = token;
        }

        public void SetUser(User user)
        {
            ManagedUser = user;
        }

        public void SetUser(string id)
        {
            ManagedUserId = id;
        }

        public async Task ChangePassword(string password)
        {
            await CheckManagedUser();
            ManagedUser.changePassword(password);
            await repo.UpdateAsync(ManagedUser);
        }

        public async Task<User> Get(string id)
        {
            return await repo.GetAsync(id);
        }

        public async Task Add(User user)
        {
            await repo.AddAsync(user);
        }

        public async Task Update(User user)
        {
            await repo.UpdateAsync(user);
        }

        public async Task Delete(string id)
        {
            User user = await Get(id);
            await this.Delete(user);
        }

        public async Task Delete(User user)
        {
            await repo.DeleteAsync(user);
            UserService service = new UserService(Token);
            foreach(string email in user.FollowsUsers.Keys)
            {
                service.SetUser(email);
                service.RemoveFollowedByUserCommand.Execute(user.Email);
            }
            foreach(string email in user.FollowedByUsers.Keys)
            {
                service.SetUser(email);
                service.RemoveFollowsUserCommand.Execute(user.Email);
            }
        }

        public async Task<IEnumerable<User>> GetAll()
        {
            return await repo.GetAllAsync();
        }

        public async Task AddCreatedContest(string ContestId)
        {
            await CheckManagedUser();
            ManagedUser.CreatedContests.Add(ContestId);
            await repo.UpdateAsync(ManagedUser);
        }

        public async Task RemoveCreatedContest(string ContestId)
        {
            await CheckManagedUser();
            ManagedUser.CreatedContests.Remove(ContestId);
            await repo.UpdateAsync(ManagedUser);
        }

        public async Task AddPartecipation(string PartecipationId)
        {
            await CheckManagedUser();
            ManagedUser.Partecipations.Add(PartecipationId);
            await repo.UpdateAsync(ManagedUser);
        }

        public async Task RemovePartecipation(string PartecipationId)
        {
            await CheckManagedUser();
            
        }

        public async Task AddShare(string ShareId)
        {
            await CheckManagedUser();
            ManagedUser.Shares.Add(ShareId);
            await repo.UpdateAsync(ManagedUser);
        }

        public async Task RemoveShare(string ShareId)
        {
            await CheckManagedUser();
            ManagedUser.Shares.Remove(ShareId);
            await repo.UpdateAsync(ManagedUser);
        }

        public async Task AddFollowsUser(User user)
        {
            await CheckManagedUser();
            ManagedUser.FollowsUsers.Add(user.Email, user.Username);
            await repo.UpdateAsync(ManagedUser);
        }

        public async Task RemoveFollowsUser(string user)
        {
            await CheckManagedUser();
            ManagedUser.FollowsUsers.Remove(user);
            await repo.UpdateAsync(ManagedUser);
        }

        public async Task AddFollowedByUser(User user)
        {
            await CheckManagedUser();
            ManagedUser.FollowedByUsers.Add(user.Email, user.Username);
            await repo.UpdateAsync(ManagedUser);
        }

        public async Task RemoveFollowedByUser(string user)
        {
            await CheckManagedUser();
            ManagedUser.FollowedByUsers.Remove(user);
            await repo.UpdateAsync(ManagedUser);
        }

        public async Task AddFollowsContest(Contest contest)
        {
            await CheckManagedUser();
            ManagedUser.FollowsContests.Add(contest.Id, contest.Name);
            await repo.UpdateAsync(ManagedUser);
            ContestService Cservice = new ContestService(Token);
            Cservice.SetContest(contest);
            Cservice.AddFollowedByUsersCommand.Execute(ManagedUser);
        }

        public async Task RemoveFollowsContest(string contest)
        {
            await CheckManagedUser();
            ManagedUser.FollowsContests.Remove(contest);
            await repo.UpdateAsync(ManagedUser);
            ContestService Cservice = new ContestService(Token);
            Cservice.SetContest(contest);
            Cservice.RemoveFollowedByUsersCommand.Execute(ManagedUser.Email);
        }

        private async Task CheckManagedUser()
        {
            if (ManagedUser == null)
            {
                if (ManagedUserId != null && ManagedUserId.Length > 0)
                {
                    ManagedUser = await this.Get(ManagedUserId);
                }
                else
                {
                    throw new Exception("ManagedUser and ManagedUserId not set. At least one must be set before calling this method. Use SetUser method.");
                }
            }
        }

        public async Task<Dictionary<string, AttributeValue>> GetAvatars(HashSet<string> followsIds)
        {
            List<Dictionary<string, AttributeValue>> userResponse;
            var avatars = new Dictionary<string, AttributeValue>();
            AttributeValue id;
            AttributeValue avatar;
            var response = await repo.GetAvatars(followsIds);
            response.TryGetValue("User", out userResponse);

            //trasformo la risposta in una semplice mappa chiave attributo
            foreach(var a in userResponse)
            {
                if(a.TryGetValue("Email", out id))
                {
                    if(a.TryGetValue("Avatar", out avatar))
                    {
                        avatars.TryAdd(id.S, avatar);
                    }
                }
            }

            return avatars;
        }

        public async Task AddAvatar(MemoryStream avatar)
        {
            string newavatar;
            string newdeletehash;
            string olddeletehash = LoginController.GetInstance().CurrentUser.DeleteHashAvatar;
            ImgurRepository imgurRepo = new ImgurRepository();

            var urls = new Dictionary<string, string>();
            urls = await imgurRepo.UploadImage(avatar.ToArray());

            string avatarTemp;
            string deleteHTemp;
            if (urls.TryGetValue("link", out avatarTemp) && (urls.TryGetValue("deletehash", out deleteHTemp)))
            {
                newavatar = avatarTemp;
                newdeletehash = deleteHTemp;
            }
            else
            {
                return;
            }
            var updatedUrls = await repo.AddImageAsync(LoginController.GetInstance().CurrentUser.Email, newavatar, newdeletehash, "User", "Avatar", "DeleteHashAvatar", "Email");

            if (updatedUrls.TryGetValue("image", out avatarTemp) && (updatedUrls.TryGetValue("deleteHash", out deleteHTemp)))
            {
                if (newavatar.Equals(avatarTemp) && newdeletehash.Equals(deleteHTemp))
                {
                    LoginController.GetInstance().CurrentUser.Avatar = newavatar;
                    LoginController.GetInstance().CurrentUser.DeleteHashAvatar = newdeletehash;
                    if (olddeletehash != null)
                    {
                        await imgurRepo.DeleteImageAsync(olddeletehash);
                    }
                }
            }
        }

        public async Task AddBanner(MemoryStream banner)
        {
            string newbanner;
            string newdeletehash;
            string olddeletehash = LoginController.GetInstance().CurrentUser.DeleteHashBanner;
            ImgurRepository imgurRepo = new ImgurRepository();

            var urls = await imgurRepo.UploadImage(banner.ToArray());
            string bannerTemp;
            string deleteHTemp;
            if (urls.TryGetValue("link", out bannerTemp) && (urls.TryGetValue("deletehash", out deleteHTemp)))
            {
                newbanner = bannerTemp;
                newdeletehash = deleteHTemp;
            }
            else
            {
                return;
            }

            var updatedUrls = await repo.AddImageAsync(LoginController.GetInstance().CurrentUser.Email, newbanner, newdeletehash, "User", "Banner", "DeleteHashBanner", "Email");

            if (updatedUrls.TryGetValue("image", out bannerTemp) && (updatedUrls.TryGetValue("deleteHash", out deleteHTemp)))
            {
                LoginController.GetInstance().CurrentUser.Banner = newbanner;
                LoginController.GetInstance().CurrentUser.DeleteHashBanner = newdeletehash;
                if (olddeletehash != null)
                {
                    await imgurRepo.DeleteImageAsync(olddeletehash);
                }
            }
        }

        public async Task RemoveAvatar()
        {
            string deletehash = LoginController.GetInstance().CurrentUser.DeleteHashAvatar;
            ImgurRepository imgurRepo = new ImgurRepository();
            await imgurRepo.DeleteImageAsync(deletehash);

            var urls = new Dictionary<string, string>();
            await repo.DeleteAttributesAsync(LoginController.GetInstance().CurrentUser.Email, "User", "REMOVE Avatar, DeleteHashAvatar", "Email");

        }

        public async Task RemoveBanner()
        {
            string deletehash = LoginController.GetInstance().CurrentUser.DeleteHashBanner;
            ImgurRepository imgurRepo = new ImgurRepository();
            await imgurRepo.DeleteImageAsync(deletehash);

            var urls = new Dictionary<string, string>();
            await repo.DeleteAttributesAsync(LoginController.GetInstance().CurrentUser.Email, "User", "REMOVE Banner, DeleteHashBanner", "Email");
        }

        public async Task DeleteBio(string userid)
        {
            await repo.DeleteAttributesAsync(userid, "User", "REMOVE Bio", "Email");
        }

        public async Task UpdateBio(string userid, string bio)
        {
            var ExpressionAttributeValues = new Dictionary<string, AttributeValue>()
                     {
                     {":bio", new AttributeValue { S = bio}},
            };
            await repo.UpdateAttributesAsync(userid, "User", "SET Bio = :bio", "Email", ExpressionAttributeValues);
        }

         public async Task UpdateName(string userid, string name)
        {
            var ExpressionAttributeValues = new Dictionary<string, AttributeValue>()
                     {
                     {":name", new AttributeValue { S = name}},
            };
            await repo.UpdateAttributesAsync(userid, "User", "SET Name = :name", "Email", ExpressionAttributeValues);
        }

        public async Task<string> GetSingleAvatar(string id)
        {
            var result = await repo.GetAttributesAsync(id, "User", "Avatar");
            AttributeValue a;
            if(result.TryGetValue("Avatar", out a)){
                return a.S;
            }
            else
            {
                return null;
            }
        }

    }
}
