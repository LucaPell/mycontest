﻿using System;
using System.Collections.Generic;
using System.Text;
using DynamoDBTest.Models;
using DynamoDBTest.ViewModels;
using DynamoDBTest.Helpers;
using System.Threading;
using System.Threading.Tasks;
using System.Linq;

namespace DynamoDBTest.Services
{
    public class TimelineService
    {
        private CrudRepository<Timeline> repo;
        private ContestService contestService;
        private PartecipationService partecipationService;
        private ShareService shareService;
        private CancellationToken Token;

        public TimelineService()
        {
            repo = new CrudRepository<Timeline>();
        }

        public TimelineService(CancellationToken token)
        {
            Token = token;
            repo = new CrudRepository<Timeline>(token);
        }

        public void UpdateToken(CancellationToken token)
        {
            Token = token;
            repo.UpdateToken(token);
        }

        public async Task Add(Timeline post)
        {
            await repo.AddAsync(post);
        }

        public async Task Add(string id, string userId, DateTime date, ItemType type)
        {
            Timeline post = new Timeline(id, userId, date, type);
            await Add(post);
        }

        public async Task Delete(string id)
        {
            await repo.DeleteAsync(id);
        }

        public async Task Delete(Timeline post)
        {
            await repo.DeleteAsync(post);
        }

        public async Task<IEnumerable<Timeline>> GetAllByUser(string user)
        {
            return await repo.GetAllAsync(index: "Status", backward: true, key: "Status", keyValues: new string[] { "OK" }, attribute: "UserId", attributeValues: new string[] { user });
        }

        public async Task<IEnumerable<Timeline>> GetAllByUser(string[] users)
        {
            return await repo.GetAllAsync(index: "Status", backward: true, key: "Status", keyValues: new string[] { "OK" }, attribute: "UserId", attributeValues: users);
        }

        public async Task<IEnumerable<AbstractSortableByDateItem>> GetAllItemsFromTimeline(string[] users)
        {
            DynamoDBUtility converter = new DynamoDBUtility(Token);
            IEnumerable<Timeline> timelines = await GetAllByUser(users);

            while(timelines.Count() < 5)
            {
                timelines = timelines.Concat(await GetAllByUser(users));
            }

            Dictionary<string, IEnumerable<string>> ids = new Dictionary<string, IEnumerable<string>>();

            foreach (Timeline timeline in timelines)
            {
                List<string> items = (List<string>)(ids.GetValueOrDefault(timeline.Type.ToString()));
                if(items == null)
                {
                    items = new List<string>();
                    ids.Add(timeline.Type.ToString(), items);
                }
                items.Add(timeline.Id);
            }
            return await converter.ConvertBatchResponse(await repo.GetAllPostsById(ids));
        }
    }
}
