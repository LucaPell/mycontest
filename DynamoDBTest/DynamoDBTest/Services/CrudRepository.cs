﻿using System;
using System.Collections.Generic;
using Amazon.DynamoDBv2;
using Amazon.DynamoDBv2.DataModel;
using System.Threading.Tasks;
using System.Threading;
using Amazon.DynamoDBv2.Model;
using Amazon.DynamoDBv2.DocumentModel;
using System.IO;
using DynamoDBTest.Controllers;

namespace DynamoDBTest.Services
{
    public class CrudRepository<T>
    {
        private static AmazonDynamoDBClient client;
        private Amazon.DynamoDBv2.DataModel.DynamoDBContext context;
        private string accessKeyID = "AKIAJJY2EHKZHZINPJGQ";
        private string secretAccessKey = "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";
        private Amazon.RegionEndpoint region = Amazon.RegionEndpoint.USEast1;
        CancellationToken Token;
        AsyncSearch<T> searchItems;

        public CrudRepository()
        {
            client = new AmazonDynamoDBClient(accessKeyID, secretAccessKey, region);
            context = new Amazon.DynamoDBv2.DataModel.DynamoDBContext(client);
        }

        public CrudRepository(CancellationToken token) : this()
        {
            UpdateToken(token);
        }

        public void UpdateToken(CancellationToken token)
        {
            Token = token;
            if (Token != null)
            {
                Token.Register(OnTokenCancelled);
            }
        }

        private void OnTokenCancelled()
        {
            
        }

        public async Task AddAsync(T item)
        {
            await context.SaveAsync<T>(item, Token);
        }

        public async Task UpdateAsync(T item)
        {
            await context.SaveAsync<T>(item, Token);
        }

        public async Task DeleteAsync(string id)
        {
            await context.DeleteAsync<T>(id, Token);
        }

        public async Task DeleteAsync(T item)
        {
            await context.DeleteAsync<T>(item, Token);
        }

        public async Task<T> GetAsync(string id)
        {
            T item = default(T);
            item = await context.LoadAsync<T>(id, Token);
            return item;
        }

       

        public async Task<bool> AddVoteAsync(string id, string tableName, string email)
        {

            var request = new UpdateItemRequest
            {
                TableName = tableName,
                Key = new Dictionary<string, AttributeValue>() { { "Id", new AttributeValue { S = id } } },
                ExpressionAttributeNames = new Dictionary<string, string>()
                  {
                    { "#V", "Votes"},
                    { "#C", "VotesNumber"}
                  },
                ExpressionAttributeValues = new Dictionary<string, AttributeValue>()
                     {
                     {":vote",new AttributeValue { SS = {email}}},
                     {":plus",new AttributeValue { N = "1"}},
                     },
                UpdateExpression = "ADD #V :vote, #C :plus",
                ReturnValues = "UPDATED_NEW"
            };
            var response = await client.UpdateItemAsync(request);
            Dictionary<string, AttributeValue> result = response.Attributes;
            AttributeValue value;

            if (result.TryGetValue("Votes", out value))
            {
                return value.SS.Contains(email);
            }

            return false;
        }



        public async Task<bool> RemoveVoteAsync(string id, string tableName, String email)
        {
            var request = new UpdateItemRequest
            {
                TableName = tableName,
                Key = new Dictionary<string, AttributeValue>() { { "Id", new AttributeValue { S = id } } },
                ExpressionAttributeNames = new Dictionary<string, string>()
                  {
                    { "#V", "Votes"},
                    { "#C", "VotesNumber"}
                  },
                ExpressionAttributeValues = new Dictionary<string, AttributeValue>()
                     {
                     {":vote",new AttributeValue { SS = {email}}},
                     {":minus",new AttributeValue { N = "-1"}},
                     },
                UpdateExpression = "DELETE #V :vote ADD #C :minus",
                ReturnValues = "UPDATED_NEW"
            };
            var response = await client.UpdateItemAsync(request);
            Dictionary<string, AttributeValue> result = response.Attributes;
            AttributeValue value;

            if (result.TryGetValue("Votes", out value))
            {
                return !value.SS.Contains(email);
            }

            return true;
        }

        public async Task<IEnumerable<T>> GetAllAsync(bool forceRefresh = false, string index = null, bool backward = false, string key = null, string[] keyValues = null, int limit = 10, string attribute = null, string[] attributeValues = null)
        {
            List<T> items = new List<T>();
            if (searchItems == null) {

                //List<AttributeValue> valuesList = new List<AttributeValue>();
                //foreach(string value in values)
                //{
                //    valuesList.Add(new AttributeValue() { S = value });
                //}
                
                //QueryFilter filter = new QueryFilter();
                //filter.AddCondition(attribute, QueryOperator.Equal, valuesList);

                Expression filterExpression = new Expression();
                Expression keyExpression = new Expression();

                //QueryFilter filter = new QueryFilter();
                if(attribute != null && attributeValues != null)
                {
                    //foreach(string value in attributeValues)
                    //{
                    //    filter.AddCondition(attribute, QueryOperator.Equal, value);
                    //}

                    Dictionary<string, DynamoDBEntry> values = new Dictionary<string, DynamoDBEntry>();
                    char a = 'a';
                    foreach (string val in attributeValues)
                    {
                        values.Add(":" + a, val);
                        a++;
                    }

                    a = 'a';
                    string expression = "";
                    for (int i = 1; i <= attributeValues.Length; i++)
                    {
                        expression += attribute + " = :" + a;
                        a++;

                        if (i < attributeValues.Length)
                        {
                            expression += " or ";
                        }
                    }

                    filterExpression.ExpressionAttributeValues = values;
                    filterExpression.ExpressionStatement = expression;
                }

                Dictionary<string, DynamoDBEntry> keyVal = new Dictionary<string, DynamoDBEntry>();
                Dictionary<string, string> keyName = new Dictionary<string, string>();
                keyVal.Add(":v", keyValues[0]);
                keyName.Add("#A", key);
                keyExpression.ExpressionStatement = "#A = :v" ;
                keyExpression.ExpressionAttributeValues = keyVal;
                keyExpression.ExpressionAttributeNames = keyName;

                QueryOperationConfig config = new QueryOperationConfig()
                {
                    IndexName = index,
                    Limit = limit,
                    BackwardSearch = backward,
                    //Filter = filter,
                    FilterExpression = filterExpression,
                    KeyExpression = keyExpression
                };

                searchItems = context.FromQueryAsync<T>(config);
            }

            return await searchItems.GetNextSetAsync(Token).ContinueWith<IEnumerable<T>>((Task<List<T>> task) => {
                if(task.IsFaulted)
                {
                    Console.WriteLine("Operation failed");
                }
                if(task.IsCanceled)
                {
                    Console.WriteLine("Operation canceled");
                }
                
                return task.Result;
            });
        }

        public async Task<Dictionary<string, List<Dictionary<string, AttributeValue>>>> GetAllPostsById(Dictionary<string, IEnumerable<string>> postsIds)
        {
            Dictionary<string, KeysAndAttributes> requestItems = new Dictionary<string, KeysAndAttributes>();

            foreach (var tableName in postsIds.Keys) {
                List<Dictionary<string, AttributeValue>> tableKeys = new List<Dictionary<string, AttributeValue>>();
                foreach (string id in postsIds.GetValueOrDefault(tableName))
                {
                    tableKeys.Add(new Dictionary<string, AttributeValue> { { "Id", new AttributeValue { S = id } } });
                }

                //tutte le tabelle richiedono le stesse chiavi, quindi uso un costruttore generico
                KeysAndAttributes tableItems = new KeysAndAttributes
                {
                    Keys = tableKeys
                };

                //associo le tabelle 
                requestItems.Add(tableName, tableItems);
            }

            BatchGetItemRequest request = new BatchGetItemRequest
            {
                RequestItems = requestItems
            };

            var response = await client.BatchGetItemAsync(request).ContinueWith((Task<BatchGetItemResponse> task) => {
                if(task.IsFaulted)
                {
                    Console.WriteLine(task.Exception.Message);
                }
                return task.Result;
            });
            List<Dictionary<string, AttributeValue>> items = new List<Dictionary<string, AttributeValue>>();
            return response.Responses;
            //la risposta è un dictionary<tabella, List<Dictionary< colonna, attributoColonna>>>
        }

        //query per prendere gli attributi di specifiche colonne che corrispondono alla table di un certo id
        public async Task<Dictionary<string, AttributeValue>> GetAttributesAsync(string id, string tableName, string attributes)
        {
            string Idname = "Id";
            if(tableName == "User")
            {
                Idname = "Email";
            }
            var request = new GetItemRequest
            {
                TableName = tableName,
                Key = new Dictionary<string, AttributeValue>() { { Idname, new AttributeValue { S = id } } },
                // Optional parameters.
                ProjectionExpression = attributes,
                ConsistentRead = true
            };

            var response = await client.GetItemAsync(request);

            // Check the response.
            Dictionary<string, AttributeValue> result = response.Item;
            return result;
        }

        public async Task<Dictionary<string, List<Dictionary<string, AttributeValue>>>> CompleteShares(List<string> partecipationIds, List<string> contestIds)
        {

            List<Dictionary<string, AttributeValue>> partecipationTableKeys = new List<Dictionary<string, AttributeValue>>();
            foreach (string id in partecipationIds)
            {
                partecipationTableKeys.Add(new Dictionary<string, AttributeValue> { { "Id", new AttributeValue { S = id } } });
            }
            KeysAndAttributes partecipationTableItems = new KeysAndAttributes
            {
                Keys = partecipationTableKeys,
                ProjectionExpression = "Id, Media, Votes, Shares",
            };

            List<Dictionary<string, AttributeValue>> contestTableKeys = new List<Dictionary<string, AttributeValue>>();
            foreach (string id in contestIds)
            {
                contestTableKeys.Add(new Dictionary<string, AttributeValue> { { "Id", new AttributeValue { S = id } } });
            }
            KeysAndAttributes contestTableItems = new KeysAndAttributes
            {
                Keys = contestTableKeys,
                ProjectionExpression = "Id, Banner, Votes, Shares",
            };

            Dictionary<string, KeysAndAttributes> requestItems = new Dictionary<string, KeysAndAttributes>();

            if (partecipationIds.Count == 0)
            {
                requestItems.Add("Contest", contestTableItems);
            }
            if (contestIds.Count == 0)
            {
                requestItems.Add("Partecipation", partecipationTableItems);
            }

            BatchGetItemRequest request = new BatchGetItemRequest
            {
                RequestItems = requestItems
            };

            var response = await client.BatchGetItemAsync(request);
            return response.Responses;
            //la risposta è un dictionary<tabella, List<Dictionary<colonna, attributoColonna>>>

        }

        public async Task<Dictionary<string, List<Dictionary<string, AttributeValue>>>> GetAvatars(HashSet<string> userIds)
        {

            List<Dictionary<string, AttributeValue>> tableKeys = new List<Dictionary<string, AttributeValue>>();
            foreach (string id in userIds)
            {
                tableKeys.Add(new Dictionary<string, AttributeValue> { { "Email", new AttributeValue { S = id } } });
            }
            KeysAndAttributes tableItems = new KeysAndAttributes
            {
                Keys = tableKeys,
                ProjectionExpression = "Email, Avatar",
            };

            Dictionary<string, KeysAndAttributes> requestItems = new Dictionary<string, KeysAndAttributes>
            {
                { "User", tableItems }
            };
            BatchGetItemRequest request = new BatchGetItemRequest
            {
                RequestItems = requestItems
            };
            var response = await client.BatchGetItemAsync(request);
            return response.Responses;
            //la risposta è un dictionary<tabella, List<Dictionary<colonna, attributoColonna>>>

        }

        public async Task<Dictionary<string,string>> AddImageAsync(string id, string image, string deletehash, string table, string imageColumn, string deleteColumn, string idName)
        {
            var request = new UpdateItemRequest
            {
                TableName = table,
                Key = new Dictionary<string, AttributeValue>() { { idName, new AttributeValue { S = id } } },
                ExpressionAttributeNames = new Dictionary<string, string>()
                  {
                    { "#I", imageColumn},
                    { "#D", deleteColumn}
                               },
                ExpressionAttributeValues = new Dictionary<string, AttributeValue>()
                     {
                     {":image", new AttributeValue { S = image}},
                     {":deleteHash", new AttributeValue { S = deletehash}}
                     },
                UpdateExpression = "SET #I = :image, #D = :deleteHash",
                ReturnValues = "UPDATED_NEW"
            };
            var response = await client.UpdateItemAsync(request);
            Dictionary<string, AttributeValue> result = response.Attributes;
            AttributeValue value;
            Dictionary<string, string> urls = new Dictionary<string, string>();
            if (result.TryGetValue(imageColumn, out value))
            {
                urls.Add("image", value.S);
            }
            if (result.TryGetValue(deleteColumn, out value))
            {
                urls.Add("deleteHash", value.S);
            }
            return urls;
        }

        //Cancella attributi di una colonna in modo parametrico, esempio di implementazione su DeleteBanner di ContestService, notare in particolare il terzo parametro passato
        public async Task DeleteAttributesAsync(string id, string table, string uptExpression, string idName)
        {
            string updateExpression = uptExpression;

            var request = new UpdateItemRequest
            {
                TableName = table,
                Key = new Dictionary<string, AttributeValue>() { { idName, new AttributeValue { S = id } } },
                UpdateExpression = updateExpression,
            };
            var response = await client.UpdateItemAsync(request);
        }

        //come sopra ma per fare update, controllare UpdateText su ContestService per un esempio, mettere il puntatore del mouse su UpdateExpression per documentazione
        public async Task UpdateAttributesAsync(string id, string table, string uptExpression, string idName, Dictionary<string, AttributeValue> exprAttributeValues)
        {
            string updateExpression = uptExpression;

            var request = new UpdateItemRequest
            {
                TableName = table,
                Key = new Dictionary<string, AttributeValue>() { { idName, new AttributeValue { S = id } } },
                ExpressionAttributeValues = exprAttributeValues,
                UpdateExpression = updateExpression,
            };
            var response = await client.UpdateItemAsync(request).ContinueWith<UpdateItemResponse>((Task<UpdateItemResponse> task) => {
                if (task.IsFaulted)
                {
                    Console.WriteLine("Operation failed");
                }
                if (task.IsCanceled)
                {
                    Console.WriteLine("Operation canceled");
                }
                return task.Result;
            });
        }


        private List<T> catchException(Task<List<T>> task)
        {
            if (task.Exception != null)
            {
                Console.WriteLine(task.Exception.StackTrace);
            }
            return task.Result;
        }
    }
}
