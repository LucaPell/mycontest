﻿using System;
using System.Collections.Generic;
using DynamoDBTest.Models;
using System.Threading.Tasks;
using System.Threading;
using DynamoDBTest.Helpers;
using Amazon.DynamoDBv2.Model;
using System.Linq;

namespace DynamoDBTest.Services
{
    public class ShareService
    {
        private CrudRepository<Share> repo;
        private Share CurrentShare;
        private string CurrentShareId;
        public Command AddVoteCommand { get; set; }
        public Command RemoveVoteCommand { get; set; }
        CancellationToken Token;

        public ShareService()
        {
            repo = new CrudRepository<Share>();

        }

        public ShareService(CancellationToken token)
        {
            repo = new CrudRepository<Share>(token);
            Token = token;
        }

        private void CreateCommands()
        {
            AddVoteCommand = new Command<StringWrapper>(async (StringWrapper idAndEmail) => await AddVote(idAndEmail));
            RemoveVoteCommand = new Command<StringWrapper>(async (StringWrapper idAndEmail) => await RemoveVote(idAndEmail));

        }

        public void UpdateToken(CancellationToken token)
        {
            repo.UpdateToken(token);
            Token = token;
        }

        public void SetShare(Share share)
        {
            CurrentShare = share;
        }

        public void SetShare(string id)
        {
            CurrentShareId = id;
        }

        public async Task<Share> Get(string id)
        {
            return await repo.GetAsync(id);
        }

        public async Task Add(Share Share)
        {
            await repo.AddAsync(Share);
            if(Share.PartecipationId != null)
            {
                var ExpressionAttributeValues = new Dictionary<string, AttributeValue>()
                     {
                     {":plus", new AttributeValue { N = "1"}},
                };
                await repo.UpdateAttributesAsync(Share.PartecipationId, "Partecipation", "ADD Shares :plus", "Id", ExpressionAttributeValues);
            }
            else
            {
                var ExpressionAttributeValues = new Dictionary<string, AttributeValue>()
                     {
                     {":plus", new AttributeValue { N = "1"}},
                };
                await repo.UpdateAttributesAsync(Share.ContestId, "Contest", "ADD Shares :plus", "Id", ExpressionAttributeValues);
            }
            UserService Uservice = new UserService(Token);
            Uservice.SetUser(Share.CreatorUser);
            Uservice.AddShareCommand.Execute(Share.Id);

            TimelineService Tservice = new TimelineService(Token);
            await Tservice.Add(Share.Id, Share.CreatorUser, Share.SortingDate, ItemType.Share);
        }

        public async Task Update(Share Share)
        {
            await repo.UpdateAsync(Share);
        }

        public async Task Delete(string id)
        {
            Share Share = await repo.GetAsync(id);
            await this.Delete(Share);
        }

        public async Task Delete(Share Share)
        {
            await repo.DeleteAsync(Share);
            UserService Uservice = new UserService(Token);
            Uservice.SetUser(Share.CreatorUser);
            Uservice.RemoveShareCommand.Execute(Share.Id);

            TimelineService Tservice = new TimelineService(Token);
            await Tservice.Delete(Share.Id);
        }

        public async Task<IEnumerable<Share>> GetAll()
        {
            return await repo.GetAllAsync();
        }

        public async Task<IEnumerable<Share>> GetAllByUser(string userId)
        {
            return await repo.GetAllAsync(false, "CreatorUser", true, "CreatorUser", new string[] { userId });
        }

        private async Task<IEnumerable<Share>> GetAllByUser(string[] userIds)
        {
            return await repo.GetAllAsync(false, "CreatorUser", true, "CreatorUser", userIds);
        }

        private async Task CheckCurrentShare()
        {
            if (CurrentShare == null)
            {
                if (CurrentShareId != null && CurrentShareId.Length > 0)
                {
                    CurrentShare = await this.Get(CurrentShareId);
                }
                else
                {
                    throw new Exception("CurrentShareId and CurrentShare not set. At least one must be set before calling this method. Use SetShare method.");
                }
            }
        }


        public async Task<IEnumerable<Share>> CompleteShares(IEnumerable<Share> incompleteShares)
        {
            AttributeValue value = null;
            List<string> partecipationIds = new List<string>();
            List<string> contestIds = new List<string>();
            List<string> shareIds = new List<string>();

            foreach (var incompleteShare in incompleteShares)
            {
                if(incompleteShare.PartecipationId != null)
                {
                    partecipationIds.Add(incompleteShare.PartecipationId);
                }
                else if(incompleteShare.ContestId != null)
                {
                    contestIds.Add(incompleteShare.ContestId);
                }
                shareIds.Add(incompleteShare.Id);
            }
            var missingAttributes = await repo.CompleteShares(partecipationIds, contestIds);
            List<Dictionary<string, AttributeValue>> pAttributes; //completa share riferiti a partecipazioni
            List<Dictionary<string, AttributeValue>> cAttributes; //stessa cosa ma per contest
            missingAttributes.TryGetValue("Partecipation", out pAttributes);
            missingAttributes.TryGetValue("Contest", out cAttributes);


            foreach (var incompleteShare in incompleteShares)
            {
                if (incompleteShare.PartecipationId != null && pAttributes != null)
                {
                    foreach(Dictionary<string, AttributeValue> p in pAttributes)
                    {
                        if (p.TryGetValue("Id", out value))
                        {
                            if (incompleteShare.PartecipationId.Equals(value.S))
                            {
                                if (p.TryGetValue("Media", out value))
                                {
                                    incompleteShare.Media = value.S;
                                }

                                if (p.TryGetValue("Votes", out value))
                                {
                                    incompleteShare.Votes = value.SS;
                                }

                                if(p.TryGetValue("Shares", out value))
                                {
                                    incompleteShare.Shares = Convert.ToInt32(value.N);
                                }
                                pAttributes.Remove(p);
                                break;
                            }
                        }
                    }

                }
                else if (incompleteShare.ContestId != null && cAttributes != null)
                {

                    foreach (Dictionary<string, AttributeValue> c in cAttributes)
                    {
                        if (c.TryGetValue("Id", out value))
                        {
                            if (incompleteShare.ContestId.Equals(value.S))
                            {
                                if (c.TryGetValue("Banner", out value))
                                {
                                    incompleteShare.Media = value.S;
                                }

                                if (c.TryGetValue("Votes", out value))
                                {
                                    incompleteShare.Votes = value.SS;
                                }

                                if (c.TryGetValue("Shares", out value))
                                {
                                    incompleteShare.Shares = Convert.ToInt32(value.N);
                                }
                                cAttributes.Remove(c);
                                break;
                            }
                        }
                    }

                }

            }
            return incompleteShares;
        }

        public async Task<bool> AddVote(StringWrapper idTypeEmail)
        {

                          //id   //tipo di tabella   //userid(email)
            return await repo.AddVoteAsync(idTypeEmail.String1, idTypeEmail.String2, idTypeEmail.String3);
        }

        public async Task<bool> RemoveVote(StringWrapper idTypeEmail)
        {
            return await repo.RemoveVoteAsync(idTypeEmail.String1, idTypeEmail.String2, idTypeEmail.String3);
        }

    }
}
