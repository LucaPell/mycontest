﻿using System;
using System.Collections.Generic;
using DynamoDBTest.Models;
using System.Threading.Tasks;
using System.Threading;
using DynamoDBTest.Helpers;
using System.IO;
using DynamoDBTest.Controllers;
using Amazon.DynamoDBv2.Model;

namespace DynamoDBTest.Services
{
    public class ContestService
    {
        private CrudRepository<Contest> repo;
        private Contest CurrentContest;
        private string CurrentContestId;
        public Command AddHasPartecipationCommand { get; set; }
        public Command RemoveHasPartecipationCommand { get; set; }
        public Command AddFollowedByUsersCommand { get; set; }
        public Command RemoveFollowedByUsersCommand { get; set; }
        public Command AddVictoryPartecipationCommand { get; set; }
        private CancellationToken Token;

        public ContestService()
        {
            repo = new CrudRepository<Contest>();
            CreateCommands();
        }

        public ContestService(CancellationToken token)
        {
            repo = new CrudRepository<Contest>(token);
            Token = token;
            CreateCommands();
        }

        private void CreateCommands()
        {
            AddHasPartecipationCommand = new Command<string>(async (string PartecipationId) => await AddHasPartecipation(PartecipationId));
            RemoveHasPartecipationCommand = new Command<string>(async (string PartecipationId) => await RemoveHasPartecipation(PartecipationId));
            AddFollowedByUsersCommand = new Command<User>(async (User user) => await AddFollowedByUsers(user));
            RemoveFollowedByUsersCommand = new Command<string>(async (string user) => await RemoveFollowedByUsers(user));
            AddVictoryPartecipationCommand = new Command<string>(async (string PartecipationId) => await AddVictoryPartecipation(PartecipationId));
        }

         public void UpdateToken(CancellationToken token)
        {
            repo.UpdateToken(token);
            Token = token;
        }

        public void SetContest(Contest contest)
        {
            CurrentContest = contest;
        }

        public void SetContest(string id)
        {
            CurrentContestId = id;
        }

        public async Task<Contest> Get(string id)
        {
            return await repo.GetAsync(id);
        }

        public async Task Add(Contest contest)
        {
            await repo.AddAsync(contest);
            UserService Uservice = new UserService(Token);
            Uservice.SetUser(contest.CreatorUser);
            Uservice.AddCreatedContestCommand.Execute(contest.Id);

            TimelineService Tservice = new TimelineService(Token);
            await Tservice.Add(contest.Id, contest.CreatorUser, contest.SortingDate, ItemType.Contest);
        }

        public async Task AddVictoryPartecipation(string PartecipationId)
        {
            await CheckCurrentContest();
            var ExpressionAttributeValues = new Dictionary<string, AttributeValue>()
                     {
                     {":victory", new AttributeValue { S = PartecipationId}},
            };
            await repo.UpdateAttributesAsync(CurrentContestId, "Contest", "SET VictoryPartecipation = :victory", "Id", ExpressionAttributeValues);
        }

        public async Task Update(Contest contest)
        {
            await repo.UpdateAsync(contest);
        }

        public async Task Delete(string id)
        {
            Contest contest = await repo.GetAsync(id);
            await this.Delete(contest);
        }

        public async Task Delete(Contest contest)
        {
            await repo.DeleteAsync(contest);
            UserService service = new UserService(Token);
            service.SetUser(contest.CreatorUser);
            service.RemoveCreatedContestCommand.Execute(contest.Id);

            TimelineService Tservice = new TimelineService(Token);
            await Tservice.Delete(contest.Id);
        }

        public async Task<IEnumerable<Contest>> GetAll()
        {
            return await repo.GetAllAsync(false, "Status", true, "Status", new string[] { "OK" });
        }

        public async Task<IEnumerable<Contest>> GetAllByCreatorUser(string user)
        {
            return await repo.GetAllAsync(false, "CreatorUser", true, "CreatorUser", new string[] { user });
        }

        private async Task<IEnumerable<Contest>> GetAllByCreatorUser(string[] users)
        {
            return await repo.GetAllAsync(false, "CreatorUser", true, "CreatorUser", users);
        }

        public async Task AddHasPartecipation(string PartecipationId)
        {
            await CheckCurrentContest();
            CurrentContest.HasPartecipations.Add(PartecipationId);
            await repo.UpdateAsync(CurrentContest);
        }

        public async Task RemoveHasPartecipation(string PartecipationId)
        {
            await CheckCurrentContest();
            CurrentContest.HasPartecipations.Remove(PartecipationId);
            await repo.UpdateAsync(CurrentContest);
        }

        public async Task AddFollowedByUsers(User user)
        {
            await CheckCurrentContest();
            CurrentContest.FollowedByUsers.Add(user.Email, user.Username);
            await repo.UpdateAsync(CurrentContest);
        }

        public async Task RemoveFollowedByUsers(string email)
        {
            await CheckCurrentContest();
            CurrentContest.FollowedByUsers.Remove(email);
            await repo.UpdateAsync(CurrentContest);
        }

        private async Task CheckCurrentContest()
        {
            if (CurrentContest == null)
            {
                if (CurrentContestId != null && CurrentContestId.Length > 0)
                {
                    CurrentContest = await this.Get(CurrentContestId);
                }
                else
                {
                    throw new Exception("CurrentContestId and CurrentContest not set. At least one must be set before calling this method. Use SetContest method.");
                }
            }
        }

        public async Task AddBanner(Contest c)
        {
            string newbanner;
            string newdeletehash;
            string olddeletehash = c.DeleteHash;
            ImgurRepository imgurRepo = new ImgurRepository();

            var urls = await imgurRepo.UploadImage(c.TempBanner.ToArray());
            string bannerTemp;
            string deleteHTemp;
            if (urls.TryGetValue("link", out bannerTemp) && (urls.TryGetValue("deletehash", out deleteHTemp)))
            {
                newbanner = bannerTemp;
                newdeletehash = deleteHTemp;
            }
            else
            {
                return;
            }

            var updatedUrls = await repo.AddImageAsync(c.Id, newbanner, newdeletehash, "Contest", "Banner", "DeleteHash", "Id");

            if (updatedUrls.TryGetValue("image", out bannerTemp) && (updatedUrls.TryGetValue("deleteHash", out deleteHTemp)))
            {
                if (olddeletehash != null)
                {
                    await imgurRepo.DeleteImageAsync(olddeletehash);
                }
            }
        }

        public async Task DeleteBanner(Contest c)
        {
            string deletehash = c.DeleteHash;
            ImgurRepository imgurRepo = new ImgurRepository();
            await imgurRepo.DeleteImageAsync(deletehash);
            await repo.DeleteAttributesAsync(c.Id, "Contest", "REMOVE Banner, DeleteHash", "Id");
        }

        public async Task DeleteText(string contestid)
        {
            await repo.DeleteAttributesAsync(contestid, "Contest", "REMOVE Description", "Id");
        }

        public async Task UpdateText(string contestid, string description)
        {
            var ExpressionAttributeValues = new Dictionary<string, AttributeValue>()
                     {
                     {":desc", new AttributeValue { S = description}},
            };
            await repo.UpdateAttributesAsync(contestid, "Contest", "SET Description = :desc", "Id", ExpressionAttributeValues);
        }

        public async Task<bool> AddVote(StringWrapper idTypeEmail)
        {

            //id   //tipo di tabella   //userid(email)
            return await repo.AddVoteAsync(idTypeEmail.String1, idTypeEmail.String2, idTypeEmail.String3);
        }

        public async Task<bool> RemoveVote(StringWrapper idTypeEmail)
        {
            return await repo.RemoveVoteAsync(idTypeEmail.String1, idTypeEmail.String2, idTypeEmail.String3);
        }
    }
}