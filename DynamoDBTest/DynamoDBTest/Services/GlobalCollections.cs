﻿using System;
using System.Collections.Generic;
using DynamoDBTest.ViewModels;

using DynamoDBTest.Models;

namespace DynamoDBTest.Services
{
    public class GlobalCollections
    {
        private Dictionary<string, Partecipation> Partecipations;
        private Dictionary<string, Contest> Contests;
        private Dictionary<string, Share> Shares;

        private static GlobalCollections SingletonInstance;

        private GlobalCollections()
        {
            Shares = new Dictionary<string, Share>();
            Partecipations = new Dictionary<string, Partecipation>();
            Contests = new Dictionary<string, Contest>();
        }

        public static GlobalCollections GetInstance()
        {
            if (SingletonInstance == null)
            {
                SingletonInstance = new GlobalCollections();
            }
            return SingletonInstance;
        }

        public void Add(Partecipation item)
        {
            if (!Partecipations.ContainsKey(item.Id))
                Partecipations.Add(item.Id, item);
        }

        public void Add(Contest item)
        {
            if (!Contests.ContainsKey(item.Id))
                Contests.Add(item.Id, item);
        }

        public void Add(Share item)
        {
            if (!Shares.ContainsKey(item.Id))
                Shares.Add(item.Id, item);
        }

        public void Add(object item)
        {
            if(item.GetType().Equals(typeof(Partecipation)))
            {
                Add((Partecipation)item);
            }
            if (item.GetType().Equals(typeof(Contest)))
            {
                Add((Contest)item);
            }
            if (item.GetType().Equals(typeof(Share)))
            {
                Add((Share)item);
            }
        }

        public Partecipation GetPartecipation(string id)
        {
            return Partecipations.GetValueOrDefault(id, null);
        }

        public Contest GetContest(string id)
        {
            return Contests.GetValueOrDefault(id, null);
        }

        public Share GetShare(string id)
        {
            return Shares.GetValueOrDefault(id, null);
        }


        public void Remove(Partecipation item)
        {
            if (Partecipations.ContainsKey(item.Id))
                Partecipations.Remove(item.Id);
        }

        public void Remove(Contest item)
        {
            if (Contests.ContainsKey(item.Id))
                Contests.Remove(item.Id);
        }

        public void Remove(Share item)
        {
            if (Shares.ContainsKey(item.Id))
                Shares.Remove(item.Id);
        }

        public void Remove(object item)
        {
            if (item.GetType().Equals(typeof(Partecipation)))
            {
                Remove((Partecipation)item);
            }
            if (item.GetType().Equals(typeof(Contest)))
            {
                Remove((Contest)item);
            }
            if (item.GetType().Equals(typeof(Share)))
            {
                Remove((Share)item);
            }
        }
    }
}
