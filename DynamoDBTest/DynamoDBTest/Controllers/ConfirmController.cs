﻿using System.Threading.Tasks;
using System.Threading;

using DynamoDBTest.Services;
using DynamoDBTest.Models;
using DynamoDBTest.Helpers;

namespace DynamoDBTest.Controllers
{
    class ConfirmController
    {
        UserService service;
        public Command CheckCodeCommand { get; set; }
        public RetryCommand RetryCheckCodeCommand;
        public Command SendNewCodeCommand { get; set; }
        public Command SendNewCodeForResetCommand { get; set; }
        public RetryCommand RetrySendNewCodeCommand;
        public bool ConfirmationSuccessfull;
        public event ConfirmationTerminatedHandler ConfirmationTerminated;
        public delegate void ConfirmationTerminatedHandler(ConfirmController controller, bool confirmationSuccessfull, string message);

        public ConfirmController()
        {
            service = new UserService();
            CheckCodeCommand = new Command<string>(async (string code) => await CheckCode(code));
            RetryCheckCodeCommand = new RetryCommand<string>(CheckCode, 3, 100000, UpdateServiceWithNewToken);
            SendNewCodeCommand = new Command(async () => await SendNewCode());
            SendNewCodeForResetCommand = new Command<string>(async (string email) => await SendNewCodeForReset(email));
            RetrySendNewCodeCommand = new RetryCommand(SendNewCode, 3, 100000, UpdateServiceWithNewToken);
            ConfirmationSuccessfull = false;
        }

        private void UpdateServiceWithNewToken(CancellationToken token)
        {
            service = new UserService(token);
        }

        private async Task<bool> CheckCode(string code)
        {
            User user = LoginController.GetInstance().CurrentUser;
            CodeManager manager = new CodeManager();
            if(manager.CheckCode(user.Email, code))
            {
                user.EmailConfirmed = true;
                await service.Update(user);
                LoginController.GetInstance().CurrentUser = user;
                ConfirmationSuccessfull = true;
                ConfirmationTerminated(this, ConfirmationSuccessfull, "Email confermata correttamente");
            } else
            {
                ConfirmationTerminated(this, ConfirmationSuccessfull, "Codice errato");
            }
            return ConfirmationSuccessfull;
        }

        private async Task SendNewCode()
        {
            User user = LoginController.GetInstance().CurrentUser;
            CodeManager manager = new CodeManager();
            manager.EnterNewRegistrationEntry(user.Email);
        }

        private async Task SendNewCodeForReset(string email)
        {
            CodeManager manager = new CodeManager();
            manager.EnterNewResetPassEntry(email);
        }

    }
}
