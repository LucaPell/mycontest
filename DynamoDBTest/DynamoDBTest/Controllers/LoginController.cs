﻿using System;
using Amazon;
using Amazon.CognitoIdentity;
using Amazon.CognitoIdentityProvider;
using Amazon.Extensions.CognitoAuthentication;
using Amazon.Runtime;
using System.Threading.Tasks;
using System.Threading;
using DynamoDBTest.Models;
using DynamoDBTest.Services;
using DynamoDBTest.Helpers;

namespace DynamoDBTest.Controllers
{
    class LoginController
    {
        public const int LOGIN_SUCCESSFULL = 1;
        public const int EMAIL_NOT_CONFIRMED = 2;
        public const int BAD_CREDENTIALS = 3;
        public const int LOGIN_ERROR = 4;

        string ClientID = "7oapltiu7q7iktdhqbaoaoc4hb";
        string UserPoolID = "us-east-1_o9Dr0Oy18";
        string IdentityPoolID = "us-east-1:c3ebfbf1-a2aa-4d76-b130-8ab7a9707abd";
        RegionEndpoint endpoint = RegionEndpoint.USEast1;
        AWSCredentials clientCredentials;

        public Command LoginCommand { get; }
        public RetryCommand RetryLoginCommand { get; }
        UserService service;
        CancellationTokenSource tokenSource;
        public User CurrentUser { get; set; }
        public event LoginTerminatedHandler LoginTerminated;
        public delegate void LoginTerminatedHandler(LoginController controller, int loginResult, string message, string email);
        private Creds Credentials;
        private struct Creds
        {
            public string Email;
            public string Password;

            public Creds(string email, string password)
            {
                Email = email;
                Password = password;
            } 
        }

        private static LoginController SingletonLoginController;

        private LoginController()
        {
            clientCredentials = new CognitoAWSCredentials(IdentityPoolID, endpoint);

            service = new UserService();
            LoginCommand = new Command(async () => await Login());
            RetryLoginCommand = new RetryCommand(Login, 5, 12000, (CancellationToken token) => { service = new UserService(token); });
        }

        public static LoginController GetInstance()
        {
            if(SingletonLoginController == null)
            {
                SingletonLoginController = new LoginController();
            }
            return SingletonLoginController;
        }

        public void SetCredentials(string email, string password/*, bool saveCreds*/)
        {
            //if(saveCreds)
            //{
                Settings.Set("credsEmail", email);
                Settings.Set("credsPasswd", password);
            //}
            Credentials = new Creds(email, password);
        }

        public bool SetSavedCredentials()
        {
            string email = Settings.Get("credsEmail");
            string pass = Settings.Get("credsPasswd");
            if(email.Equals("") || pass.Equals(""))
            {
                return false;
            }
            Credentials = new Creds(email, pass);
            return true;
        }

        public async Task<bool> LoginCognito(string email, string password)
        {
            var provider = new AmazonCognitoIdentityProviderClient(new AnonymousAWSCredentials());
            CognitoUserPool userPool = new CognitoUserPool(UserPoolID, ClientID, provider);
            CognitoUser user = new CognitoUser(email, ClientID, userPool, provider);

            AuthFlowResponse context = await user.StartWithSrpAuthAsync(new InitiateSrpAuthRequest()
            {
                Password = password
            }).ConfigureAwait(false);

            CognitoAWSCredentials credentials =
                user.GetCognitoAWSCredentials(IdentityPoolID, endpoint);

            return false;
        }

        public async Task<int> Login()
        {
            try
            {
                User realUser = await service.Get(Credentials.Email);
                if (realUser != null && realUser.CheckPassword(Credentials.Password))
                {
                    //AuthenticationLoopCheck.GetInstance().Start();
                    CurrentUser = realUser;
                    if (CurrentUser.EmailConfirmed)
                    {
                        LoginTerminated(this, LOGIN_SUCCESSFULL, null, null);
                        return LOGIN_SUCCESSFULL;
                    }
                    else
                    {
                        LoginTerminated(this, EMAIL_NOT_CONFIRMED, "Devi prima confermare l'email", CurrentUser.Email);
                        return EMAIL_NOT_CONFIRMED;
                    }
                }
                else
                {
                    LoginTerminated(this, BAD_CREDENTIALS, "Combinazione email e password errata", null);
                    return BAD_CREDENTIALS;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                LoginTerminated(this, LOGIN_ERROR, ex.Message, null);
                return 0;
            }
            finally
            {
                foreach(Delegate d in LoginTerminated.GetInvocationList())
                {
                    LoginTerminated -= (LoginTerminatedHandler)d;
                }
            }
        }

        public void Logout()
        {
            AuthenticationLoopCheck.GetInstance().Stop();
            CurrentUser = null;
            SetCredentials("", ""/*, true*/);
            Settings.Set("automatic_login", "false");
        }
    }
}
