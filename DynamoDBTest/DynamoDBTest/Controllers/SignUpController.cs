﻿using System;
using Amazon;
using Amazon.CognitoIdentity;
using Amazon.CognitoIdentityProvider;
using Amazon.CognitoIdentityProvider.Model;
using Amazon.Runtime;
using System.Threading.Tasks;
using System.Threading;

using DynamoDBTest.Models;
using DynamoDBTest.Services;
using System.IO;
using DynamoDBTest.Helpers;

namespace DynamoDBTest.Controllers
{
    public class SignUpController
    {
        string ClientID = "7oapltiu7q7iktdhqbaoaoc4hb";
        string UserPoolID = "us-east-1_o9Dr0Oy18";
        string IdentityPoolID = "us-east-1:c3ebfbf1-a2aa-4d76-b130-8ab7a9707abd";
        RegionEndpoint endpoint = RegionEndpoint.USEast1;
        AWSCredentials clientCredentials;

        public Command RegisterCommand { get; }
        public RetryCommand RetryRegisterCommand { get; }
        UserService service;
        public User CurrentUser { get; set; }
        public bool RegistrationSuccessfull;
        public event RegistrationTerminatedHandler RegistrationTerminated;
        public delegate void RegistrationTerminatedHandler(SignUpController controller, bool registrationSuccessfull, string message, string email);

        public SignUpController()
        {
            clientCredentials = new CognitoAWSCredentials(IdentityPoolID, endpoint);

            service = new UserService();
            RegisterCommand = new Command<User>(async (User user) => await Register(user));
            RetryRegisterCommand = new RetryCommand<User>(Register, 5, 15000, (CancellationToken token) => { service = new UserService(token); });
            RegistrationSuccessfull = false;
        }

        public SignUpController(string email, string username, string password) : this()
        {
            CurrentUser = new User(email, username, password);
        }

        public async Task<bool> Register (string email, string username, string password)
        {
            return await Register(new User(email, username, password));
        }

        public async Task<bool> Register(User newUser)
        {
            try
            {
                User u = await service.Get(newUser.Email);
 
                if (u == null || !u.EmailConfirmed)
                {
                    if (u == null)
                    {
                        await service.Add(newUser);
                    }
                    CodeManager manager = new CodeManager();
                    manager.EnterNewRegistrationEntry(newUser.Email);
                    RegistrationSuccessfull = true;
                    LoginController.GetInstance().CurrentUser = newUser;
                    RegistrationTerminated(this, RegistrationSuccessfull, null, newUser.Email);
                    return true;
                }
                RegistrationTerminated(this, RegistrationSuccessfull, "L'email inserita è già utilizzata da un altro utente", null);
                return false;
            } catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
                RegistrationTerminated(this, RegistrationSuccessfull, "Qualcosa è andato storto", null);
                return false;
            }
        }

        private Stream GenerateStreamFromString(string s)
        {
            MemoryStream stream = new MemoryStream();
            StreamWriter writer = new StreamWriter(stream);
            writer.Write(s);
            writer.Flush();
            stream.Position = 0;
            return stream;
        }

        public async Task<bool> RegisterCognito(string email, string username, string password) //NON FUNZIONA
        {
            try
            {
                string AccessKey = "AKIAJJY2EHKZHZINPJGQ";
                string SecretKey = "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";
                string ClientID = "7oapltiu7q7iktdhqbaoaoc4hb";
                string UserPoolID = "us-east-1_o9Dr0Oy18";
                string IdentityPoolID = "us-east-1:c3ebfbf1-a2aa-4d76-b130-8ab7a9707abd";
                RegionEndpoint endpoint = RegionEndpoint.USEast1;
                AWSCredentials clientCredentials = new CognitoAWSCredentials(IdentityPoolID, endpoint);
                AmazonCognitoIdentityProviderClient provider = new AmazonCognitoIdentityProviderClient(clientCredentials, endpoint);

                ListUsersRequest req = new ListUsersRequest()
                {
                    UserPoolId = UserPoolID
                };
                ListUsersResponse res = await provider.ListUsersAsync(req);

                SignUpRequest request = new SignUpRequest()
                {
                    ClientId = ClientID,
                    Password = password,
                    Username = username
                };
                AttributeType emailAttribute = new AttributeType()
                {
                    Name = "email",
                    Value = email
                };
                request.UserAttributes.Add(emailAttribute);
                var response = await provider.SignUpAsync(request);
            }
            catch (Exception ex)
            {
                string message = ex.Message;
            }
            return false;
        }
    }
}
