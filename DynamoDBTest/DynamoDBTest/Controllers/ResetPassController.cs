﻿using System;
using Amazon;
using Amazon.CognitoIdentity;
using Amazon.CognitoIdentityProvider;
using Amazon.CognitoIdentityProvider.Model;
using Amazon.Runtime;
using System.Threading.Tasks;
using System.Threading;

using DynamoDBTest.Models;
using DynamoDBTest.Services;
using System.IO;
using DynamoDBTest.Helpers;

namespace DynamoDBTest.Controllers
{
    public class ResetPassController
    {
        string ClientID = "7oapltiu7q7iktdhqbaoaoc4hb";
        string UserPoolID = "us-east-1_o9Dr0Oy18";
        string IdentityPoolID = "us-east-1:c3ebfbf1-a2aa-4d76-b130-8ab7a9707abd";
        RegionEndpoint endpoint = RegionEndpoint.USEast1;
        AWSCredentials clientCredentials;

        public Command SendEmailCommand { get; }
        public Command ChangePasswordCommand { get; set; }
        UserService service;
        String givenEmail; 
        public bool CheckSuccessfull;
        public event CheckTerminatedHandler CheckTerminated;
        public delegate void CheckTerminatedHandler(ResetPassController controller, bool CheckSuccessfull, string message, string email);

        LoginController controller = LoginController.GetInstance();

        public ResetPassController()
        {
            clientCredentials = new CognitoAWSCredentials(IdentityPoolID, endpoint);
            service = new UserService();
            SendEmailCommand = new Command<String>(async (string email) => await SendRecoveryEmail(email));

            //porcata per dare piu paramentri al command dato che devo passare email e password
            ChangePasswordCommand = new Command<StringWrapper>(async (StringWrapper emailAndPassword) => await ChangePassword(emailAndPassword));

            CheckSuccessfull = false;
        }

        public async Task<bool> SendRecoveryEmail(String email)
        {
            try
            {
                User u = await service.Get(email);

                if (u != null)
                {
                    CodeManager manager = new CodeManager();
                    manager.EnterNewResetPassEntry(email);
                    CheckSuccessfull = true;

                    //serve a fare il login senza password, 
                    //dato che nella prossima activity dobbiamo cambiare password
                    LoginController.GetInstance().CurrentUser = u;

                    CheckTerminated(this, CheckSuccessfull, null, email);
                    return true;
                }
                CheckTerminated(this, CheckSuccessfull, "L'email inserita non è valida", null);
                return false;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                CheckTerminated(this, CheckSuccessfull, "Qualcosa è andato storto", null);
                return false;
            }
        }

        public async Task<bool> ChangePassword(StringWrapper passAndEmail)
        {
            try
            {
                service.SetUser(passAndEmail.String1);
                await service.ChangePassword(passAndEmail.String2);
                CheckSuccessfull = true;
                CheckTerminated(this, CheckSuccessfull, "Password cambiata con successo", null);
                return true;
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
                CheckTerminated(this, CheckSuccessfull, "Qualcosa è andato storto", null);
                return false;
            }
        }
    }
}
