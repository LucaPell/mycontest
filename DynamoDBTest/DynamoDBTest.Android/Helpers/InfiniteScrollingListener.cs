﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Support.V7.Widget;
using Android.Views;
using Android.Widget;

namespace DynamoDBTest.Droid.Helpers
{
    public class InfiniteScrollingListener : RecyclerView.OnScrollListener
    {
        private Activity activity;
        public event EventHandler EndScrollReached;

        public InfiniteScrollingListener(Activity a)
        {
            activity = a;
        }

        public override void OnScrolled(RecyclerView recyclerView, int dx, int dy)
        {
            var list = ((LinearLayoutManager)recyclerView.GetLayoutManager());
            int toolbarHeight = activity.FindViewById<Android.Support.V7.Widget.Toolbar>(Resource.Id.toolbar).Height;

            base.OnScrolled(recyclerView, dx, dy);

            if (dy > 0)
            {
                int itemCount = list.ItemCount;
                if ((list.FindLastVisibleItemPosition() == itemCount - 3 || itemCount < 3)&& itemCount > 0)
                {
                    EndScrollReached(this, new EventArgs());
                }
            }
        }
    }
}