﻿using System;
using Android.App;
using Android.Content;
using Android.Text;
using Android.Text.Style;
using Android.Views;


namespace DynamoDBTest.Droid
{
    public class MyClickableSpan : ClickableSpan
    {
        private string type;
        private string id;
        private string name;
        Activity activity;
        public MyClickableSpan(String type, String id, String name, Activity activity)
        {
            this.type = type; //per capire che cosa sto cliccando
            this.id = id;
            this.name = name;
            this.activity = activity;
        }

        public MyClickableSpan(String type, String id, Activity activity)
        {
            this.type = type; //per capire che cosa sto cliccando
            this.id = id;
            this.activity = activity;
        }

        public override void OnClick(View widget)
        {
            if (type.Equals("user"))
            {
                var intent = new Intent(activity, typeof(UserPageActivity));

                intent.PutExtra("userId", id);
                intent.PutExtra("username", name);
                activity.StartActivity(intent);
            }
            else if (type.Equals("contest"))
            {
                //TO DO
            }
            else if (type.Equals("partecipation"))
            {
                //TO DO
            }

        }

        public override void UpdateDrawState(TextPaint ds)
        {
            base.UpdateDrawState(ds);
            ds.UnderlineText = false;
            ds.Color = Android.Graphics.Color.ParseColor("#131917");
            ds.FakeBoldText = true;
           
        }

    }
}