﻿using System;
using System.Reflection;
using Android.App;
using Android.Support.Design.Widget;
using Android.Support.V7.Widget;
using Android.Widget;

namespace DynamoDBTest.Droid
{
    //serve a fixare un bug di google dove la collapsing toolbar non si apre quando si fa lo scroll in alto
    public class CollapsingPlusRecyclerListener : RecyclerView.OnScrollListener
    {
        private Android.Support.V7.Widget.Toolbar toolbar;
        private Activity activity;
        private int scrimHeight;

        public CollapsingPlusRecyclerListener(Activity a)
        {
            activity = a;
            toolbar = activity.FindViewById<Android.Support.V7.Widget.Toolbar>(Resource.Id.toolbar);
        }

        public override void OnScrolled(RecyclerView recyclerView, int dx, int dy)
        {
            int toolbarHeight = activity.FindViewById<Android.Support.V7.Widget.Toolbar>(Resource.Id.toolbar).Height; 

            base.OnScrolled(recyclerView, dx, dy);

            if (scrimHeight == 0)
            {
                scrimHeight = ((BaseActivity)activity).GetAppbar().Height - activity.FindViewById<ImageButton>(Resource.Id.banner).Height;
                activity.FindViewById<CollapsingToolbarLayout>(Resource.Id.collapsing_toolbar).ScrimVisibleHeightTrigger = scrimHeight;
            }

            if (dy < 0)
            {
                if (((LinearLayoutManager)recyclerView.GetLayoutManager()).FindFirstVisibleItemPosition() == 0)
                {
                    if (IsAppBarLayoutCollapsed(toolbarHeight * 2))
                    {
                        ((BaseActivity)activity).GetAppbar().SetExpanded(true, true);
                    }
                }
            }
        }

        private Boolean IsAppBarLayoutCollapsed(int comparator)
        {
            return (int)(((BaseActivity)activity).GetAppbar().GetY() + ((BaseActivity)activity).GetAppbar().Height) == comparator;
        }


    }



}