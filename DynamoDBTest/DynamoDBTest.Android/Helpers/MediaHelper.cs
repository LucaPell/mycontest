﻿using Android.App;
using Android.Content;
using Android.Graphics;
using Android.OS;
using Android.Runtime;
using Android.Views;
using DynamoDBTest.Controllers;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DynamoDBTest.Droid
{
    public class MediaHelper
    {
       private static Bitmap BitmapA { get; set; } //avatar 
       private static Bitmap BitmapB { get; set; } //banner
       private static Bitmap Result { get; set; }
        private static int Height { get; set; }
        private static int Width { get; set; }

     private static MediaHelper SingletonInstance;

        private MediaHelper()
        {
        }

        public static MediaHelper GetInstance()
        {
            if (SingletonInstance == null)
            {
                SingletonInstance = new MediaHelper();
            }
            return SingletonInstance;
        }

        //public void PassPersonalAvatar()
        //{
        //    if (LoginController.GetInstance().CurrentUser.Avatar != null)
        //    {
        //        BitmapA = BitmapFactory.DecodeStream(LoginController.GetInstance().CurrentUser.Avatar);
        //        LoginController.GetInstance().CurrentUser.Avatar.Position = 0;
        //    }
        //    else if (BitmapA != null) { 
        //            BitmapA = null;
        //    }
        //}

        //public void PassPersonalBanner()
        //{
        //    if (LoginController.GetInstance().CurrentUser.Banner != null)
        //    {
        //        BitmapB = BitmapFactory.DecodeStream(LoginController.GetInstance().CurrentUser.Banner);
        //        LoginController.GetInstance().CurrentUser.Banner.Position = 0;
        //    }
        //    else if (BitmapB != null){
        //            BitmapB = null;
        //    }

        //}

        public Bitmap GetAvatar()
        {
            return BitmapA;
        }

        public Bitmap GetBanner()
        {
            return BitmapB;
        }

        public Boolean HasAvatar()
        {
            return BitmapA != null;
        }

        public Boolean HasBanner()
        {
            return BitmapB != null;
        }

        public Boolean HasResult()
        {
            return Result != null;
        }

        public void SetAvatar(Bitmap bitmap)
        {
            BitmapA = bitmap;
        }

        public void SetBanner(Bitmap bitmap)
        {
            BitmapB = bitmap;
        }

        public void SetResult(Bitmap bitmap)  
        {
            Result = bitmap;
        }

        public void ClearBitmaps()
        {
            Result = null;
            BitmapA = null;
            BitmapB = null;
        }

        public Bitmap GetResult()
        {
            return Result;
        }

        public int ScreenHeight()
        {
            if(Height == 0)
            {
                ScreenSize();
            }
            return Height;
        }

        public int ScreenWidth()
        {
            if (Width == 0)
            {
                ScreenSize();
            }
            return Width;
        }

        public void ScreenSize()
        {
            IWindowManager windowManager = Application.Context.GetSystemService(Context.WindowService).JavaCast<IWindowManager>();
            Point outPoint = new Point();
            if (Build.VERSION.SdkInt >= BuildVersionCodes.JellyBeanMr1)
            {
                // include navigation bar
                windowManager.DefaultDisplay.GetRealSize(outPoint);
            }
            else
            {
                // exclude navigation bar
                windowManager.DefaultDisplay.GetSize(outPoint);
            }
            if (outPoint.Y > outPoint.X)
            {
                Height = outPoint.Y;
                Width = outPoint.X;
            }
            else
            {
                Height = outPoint.X;
                Width = outPoint.Y;
            }
        }
    }
}