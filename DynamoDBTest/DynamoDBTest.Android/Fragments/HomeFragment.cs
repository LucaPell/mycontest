﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Android.OS;
using Android.Support.V7.Widget;
using Android.Views;
using Android.App;
using Android.Content;
using Android.Graphics;
using Android.Widget;
using Android.Text;
using Android.Text.Style;
using Android.Text.Method;

using DynamoDBTest.ViewModels;
using DynamoDBTest.Models;
using DynamoDBTest.Controllers;
using DynamoDBTest.Helpers;
using DynamoDBTest.Services;

using Java.Lang;
using Com.Bumptech.Glide;
using Android.Support.V4.Content;
using Android.Runtime;
using Android.Animation;

namespace DynamoDBTest.Droid
{
    public class HomeFragment : AbstractRefresherFragment<AbstractSortableByDateItem>
    {
        public static HomeFragment NewInstance() =>
        new HomeFragment { Arguments = new Bundle() };

        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            HomeViewModel viewModel = new HomeViewModel();
            base.ViewModel = viewModel;
            base.LoadItemsCommand = viewModel.RetryLoadItemsCommand;
            base.adapter = new HomeContestAdapter(Activity, viewModel);
            base.adapter.fragment = this;
            View view = base.OnCreateView(inflater, container, savedInstanceState);
            return view;
        }

        public override void Adapter_ItemClick(object sender, RecyclerClickEventArgs e)
        {
            Intent intent;
            var item = ViewModel.Items[e.Position];
            if (item.GetType().Equals(typeof(Contest)))
            {
                intent = new Intent(Activity, typeof(ContestDetailActivity));
                MediaHelper.GetInstance().ClearBitmaps();
                intent.PutExtra("data", Newtonsoft.Json.JsonConvert.SerializeObject(item));

                Activity.StartActivity(intent);
            }
            else if(item.GetType().Equals(typeof(Partecipation)))
            {
                
            } else if (item.GetType().Equals(typeof(Share)))
            {

            }
        }

        public override void OnLoadItemsError(object sender, EventArgs a)
        {
            base.OnLoadItemsError(sender, a);

            CommandCompletedEventArgs args = a as CommandCompletedEventArgs;
            if (args.Status == CommandStates.MaxRetryReached)
            {
            }
        }

        public override void OnActivityResult(int requestCode, int resultCode, Intent data)
        {
            base.OnActivityResult(requestCode, resultCode, data);

            if (requestCode == HomeContestAdapter.PARTECIPATE_REQUEST_CODE)
            {
                if (resultCode == 0)
                {
                    Partecipation newPartecipation = GlobalCollections.GetInstance().GetPartecipation(data.GetStringExtra("newPartecipationId"));
                    ((HomeViewModel)base.ViewModel).PutItem(newPartecipation);
                    AuthenticationLoopCheck.GetInstance().DoOne();
                }
            }
            if(requestCode == HomeContestAdapter.CONTEST_DETAIL_REQUEST_CODE)
            {
                if (resultCode == 0)
                {
                    Contest contestUpdated = GlobalCollections.GetInstance().GetContest(data.GetStringExtra("contestUpdatedId"));
                    ViewModel.Items.RemoveAt(((HomeContestAdapter)adapter).requestFromIndex);
                    ViewModel.Items.Insert(((HomeContestAdapter)adapter).requestFromIndex, contestUpdated);
                }
            }

            ((MainActivity)Activity).ProgressB.Animation = ((MainActivity)Activity).outAnimation;
            ((MainActivity)Activity).ProgressB.Visibility = ViewStates.Gone;
        }
    }

    public class HomeContestAdapter : ItemsAdapter<AbstractSortableByDateItem>
    {
        public static int VIEW_TYPE_CONTEST = 0;
        public static int VIEW_TYPE_PARTECIPATION = 1;
        public static int VIEW_TYPE_SHARE = 2;
        public static ushort PARTECIPATE_REQUEST_CODE = 10;
        public static ushort CONTEST_DETAIL_REQUEST_CODE = 20;

        public int requestFromIndex;
        PartecipationDetailViewModel victoryPartecipationViewModel = new PartecipationDetailViewModel();

        public HomeContestAdapter(Activity activity, ICollectioViewModel<AbstractSortableByDateItem> viewModel) : base(activity, viewModel)
        {

        }

        public override RecyclerView.ViewHolder OnCreateViewHolder(ViewGroup parent, int viewType)
        {
            View itemView = null;
            if (viewType == VIEW_TYPE_CONTEST)
            {
                var id = Resource.Layout.contest_browse;
                itemView = LayoutInflater.From(parent.Context).Inflate(id, parent, false);
                
                var vh = new ContestViewHolder(itemView, OnClick, OnLongClick);
                return vh;
            }
            else if (viewType == VIEW_TYPE_PARTECIPATION)
            {
                var id = Resource.Layout.partecipation_browse;
                itemView = LayoutInflater.From(parent.Context).Inflate(id, parent, false);

                var vh = new PartecipationViewHolder(itemView, OnClick, OnLongClick);
                return vh;
            }
            else
            {
                var id = Resource.Layout.share_browse2;
                itemView = LayoutInflater.From(parent.Context).Inflate(id, parent, false);

                var vh = new ShareViewHolder(itemView, OnClick, OnLongClick);
                return vh;
            }
        }

        public override int GetItemViewType(int position)
        {
            if (base.viewModel.Items[position].GetType().Equals(typeof(Contest)))
            {
                return VIEW_TYPE_CONTEST;
            }
            else if (base.viewModel.Items[position].GetType().Equals(typeof(Partecipation)))
            {
                return VIEW_TYPE_PARTECIPATION;
            }
            else
            {
                return VIEW_TYPE_SHARE;
            }
        }

        // Replace the contents of a view (invoked by the layout manager)
        public override void OnBindViewHolder(RecyclerView.ViewHolder holder, int position)
        {
            var item = base.viewModel.Items[position];
            var user = LoginController.GetInstance().CurrentUser;
            if (holder.GetType().Equals(typeof(ContestViewHolder)))
            {
                var myHolder = holder as ContestViewHolder;
                var contest = item as Contest;
                myHolder.Username.Text = contest.CreatorUserName;
                myHolder.Date.Text = PrettyDate.GetPrettyDate(contest.SortingDate);
                myHolder.Contest.Text = contest.Name;
                if (contest.Description != null)
                {
                    myHolder.Description.Visibility = ViewStates.Visible;
                    myHolder.Description.Text = contest.Description;
                }
                else
                {
                    myHolder.Description.Visibility = ViewStates.Gone;
                }

                if (DateTime.Compare(contest.EndDate, DateTime.Now) <= 0)
                {
                    myHolder.Partecipate.Visibility = ViewStates.Gone;
                    myHolder.Partecipate.Clickable = false;

                    if (contest.VictoryPartecipation == null)
                    {
                        victoryPartecipationViewModel.RetryLoadVictoryPartecipationNoSaveCommand.Execute(contest.Id);
                    }
                }
                else
                {
                    myHolder.Partecipate.Visibility = ViewStates.Visible;
                    myHolder.Partecipate.Clickable = true;
                }

                if (contest.Banner != null)
                {
                    Glide.With(activity).Load(contest.Banner).Into(myHolder.Media);
                    myHolder.Media.Clickable = true;
                }
                else
                {
                    myHolder.Media.SetImageResource(Resource.Color.imageBlank);
                    myHolder.Media.Clickable = false;
                }

                if (contest.Avatar != null)
                {
                    Glide.With(activity).Load(contest.Avatar).Into(myHolder.Avatar);
                }
                else
                {
                    myHolder.Avatar.SetImageResource(Resource.Color.imageBlank);
                }
                myHolder.Votes.Text = contest.Votes.Count.ToString();
                myHolder.Shares.Text = contest.Shares.ToString();
                myHolder.Comments.Text = contest.Comments.Count.ToString();


                if (contest.Votes.Contains(user.Email))
                {
                    myHolder.AddVote.Activated = true;
                }

                myHolder.OnAddLikeClick = (object sender, EventArgs e) =>
                {
                    myHolder.AddVote.Enabled = false;
                    if (!myHolder.AddVote.Activated)
                    {
                        ((HomeViewModel)viewModel).RetryAddVoteCommand.Execute(position);

                        if (contest.Votes.Contains(user.Email))
                        {
                            int newcount = contest.Votes.Count;
                            Int32.TryParse(myHolder.Votes.Text, out newcount);
                            myHolder.Votes.Text = (newcount + 1).ToString();
                            myHolder.AddVote.Activated = true;
                        }

                    }
                    else
                    {
                        ((HomeViewModel)viewModel).RetryRemoveVoteCommand.Execute(position);

                        if (!contest.Votes.Contains(user.Email))
                        {
                            int newcount = contest.Votes.Count;
                            Int32.TryParse(myHolder.Votes.Text, out newcount);
                            myHolder.Votes.Text = (newcount - 1).ToString();
                            myHolder.AddVote.Activated = false;
                        }
                    }
                    myHolder.AddVote.Enabled = true;
                };
                myHolder.AddVote.Click += myHolder.OnAddLikeClick;

                myHolder.OnCommentClick = (object sender, EventArgs e) =>
                {
                    var intent = new Intent(activity, typeof(ContestDetailActivity));

                    intent.PutExtra("data", Newtonsoft.Json.JsonConvert.SerializeObject(contest));
                    intent.PutExtra("tab", 1);
                    activity.StartActivity(intent);
                };
                myHolder.Comment.Click += myHolder.OnCommentClick;

                myHolder.OnMediaClick = (object sender, EventArgs e) =>
                {
                    fragment.ImageZoom(myHolder.Media, contest.Banner);
                };
                myHolder.Media.Click += myHolder.OnMediaClick;

                myHolder.OnShareClick = (object sender, EventArgs e) =>
                {
                    var intent = new Intent(activity, typeof(AddShareActivity));
                    intent.PutExtra("contest", contest.Id);
                    activity.StartActivity(intent);
                };
                myHolder.Share.Click += myHolder.OnShareClick;

                if (user.Partecipations.Contains(user.Email + contest.Id))
                {
                    myHolder.Partecipate.Activated = true;
                    myHolder.Partecipate.Text = "Partecipando";
                    myHolder.Partecipate.SetTextColor(Color.White);
                }
                else
                {
                    myHolder.Partecipate.Activated = false;
                    myHolder.Partecipate.Text = "Partecipa";
                    myHolder.Partecipate.SetTextColor(ContextCompat.GetColorStateList(activity, Resource.Color.mediumFont));
                }

                myHolder.OnPartecipateClick = (object sender, EventArgs e) =>
                {
                    myHolder.Partecipate.Clickable = false;
                    if (!myHolder.Partecipate.Activated)
                    {
                        var newIntent = new Intent(activity, typeof(AddPartecipationActivity));
                        newIntent.PutExtra("data", Newtonsoft.Json.JsonConvert.SerializeObject(contest));
                        newIntent.PutExtra("fragment", "OK");
                        requestFromIndex = position;
                        MediaHelper.GetInstance().SetBanner(null);
                        MediaHelper.GetInstance().SetResult(null);
                        activity.StartActivityForResult(newIntent, PARTECIPATE_REQUEST_CODE);
                    }
                    else
                    {
                        AlertDialog.Builder builder = new AlertDialog.Builder(activity, Resource.Style.WarningAlertDialogStyle);
                        builder.SetTitle("Vuoi annullare la tua partecipazione?");
                        builder.SetMessage("Una volta confermato la partecipazione sparirà per sempre e perderai tutti i voti legati ad essa.");
                        builder.SetPositiveButton("SI", (senderAlert, args) => {
                            PartecipationViewModel partecipationViewModel = new PartecipationViewModel();
                            partecipationViewModel.RetryDeletePartecipationCommand.Execute(user.Email + contest.Id);
                        });
                        builder.SetNegativeButton("NO", (senderAlert, args) => {
                        });
                        builder.Create().Show();
                    }
                    myHolder.Partecipate.Clickable = true;
                };
                myHolder.Partecipate.Click += myHolder.OnPartecipateClick;

                myHolder.OnUserClick = (object sender, EventArgs e) =>
                {
                    if (user.Email == contest.CreatorUser)
                    {
                        var intent = new Intent(activity, typeof(PersonalUserPageActivity));
                        MediaHelper.GetInstance().ClearBitmaps();
                        activity.StartActivity(intent);
                    }
                    else{
                        var intent = new Intent(activity, typeof(UserPageActivity));
                        intent.PutExtra("userId", contest.CreatorUser);
                        intent.PutExtra("username", contest.CreatorUserName);
                        intent.PutExtra("avatar", contest.Avatar);
                        activity.StartActivity(intent);
                    }
                };
                myHolder.Username.Click += myHolder.OnUserClick;
                myHolder.Avatar.Click += myHolder.OnUserClick;

                myHolder.OnContestClick = (object sender, EventArgs e) =>
                {
                    var intent = new Intent(activity, typeof(ContestDetailActivity));
                    MediaHelper.GetInstance().ClearBitmaps();
                    intent.PutExtra("data", Newtonsoft.Json.JsonConvert.SerializeObject(item));
                    requestFromIndex = position;
                    activity.StartActivityForResult(intent, CONTEST_DETAIL_REQUEST_CODE);
                };
                myHolder.Contest.Click += myHolder.OnContestClick;
            }
            else if (holder.GetType().Equals(typeof(PartecipationViewHolder)))
            {
                var myHolder = holder as PartecipationViewHolder;
                var partecipation = item as Partecipation;
                myHolder.Username.Text = partecipation.CreatorUserName;
                myHolder.Date.Text = PrettyDate.GetPrettyDate(partecipation.SortingDate);
                myHolder.Contest.Text = partecipation.PartecipatingToContestName;
                if (partecipation.Content != null)
                {
                    myHolder.Description.Visibility = ViewStates.Visible;
                    myHolder.Description.Text = partecipation.Content;
                }
                else
                {
                    myHolder.Description.Visibility = ViewStates.Gone;
                }

                if (DateTime.Compare(partecipation.ContestEndDate, DateTime.Now) <= 0)
                {
                    myHolder.Partecipate.Visibility = ViewStates.Gone;
                    myHolder.Partecipate.Clickable = false;
                }
                else
                {
                    myHolder.Partecipate.Visibility = ViewStates.Visible;
                    myHolder.Partecipate.Clickable = true;
                }

                if (partecipation.Media != null)
                {
                    Glide.With(activity).Load(partecipation.Media).Into(myHolder.Media);
                    myHolder.Media.Clickable = true;
                }
                else
                {
                    myHolder.Media.SetImageResource(Resource.Color.imageBlank);
                    myHolder.Media.Clickable = false;
                }

                if (partecipation.Avatar != null)
                {
                    Glide.With(activity).Load(partecipation.Avatar).Into(myHolder.Avatar);
                }
                else
                {
                    myHolder.Avatar.SetImageResource(Resource.Color.imageBlank);
                }

                if(partecipation.Winner == 1)
                {
                    myHolder.Crown.Visibility = ViewStates.Visible;
                    myHolder.Verb.Text = "Ha vinto il Contest:";
                }
                else
                {
                    myHolder.Crown.Visibility = ViewStates.Gone;
                    myHolder.Verb.Text = "Partecipa al Contest:";
                }

                myHolder.Votes.Text = partecipation.Votes.Count.ToString();
                myHolder.Shares.Text = partecipation.Shares.ToString();
                myHolder.Comments.Text = partecipation.Comments.Count.ToString();

                if (partecipation.Votes.Contains(user.Email))
                {
                    myHolder.AddVote.Activated = true;
                }

                myHolder.OnAddVoteClick = (object sender, EventArgs e) =>
                {
                    if (DateTime.Compare(partecipation.ContestEndDate, DateTime.Now) <= 0)
                    {
                        Toast.MakeText(activity, "Il contest è finito!", ToastLength.Short).Show();
                    }
                    else
                    {
                        myHolder.AddVote.Enabled = false;
                        if (!myHolder.AddVote.Activated)
                        {
                            ((HomeViewModel)viewModel).RetryAddVoteCommand.Execute(position);

                            if (partecipation.Votes.Contains(user.Email))
                            {
                                int newcount = partecipation.Votes.Count;
                                Int32.TryParse(myHolder.Votes.Text, out newcount);
                                myHolder.Votes.Text = (newcount + 1).ToString();
                                myHolder.AddVote.Activated = true;
                            }

                        }
                        else
                        {
                            ((HomeViewModel)viewModel).RetryRemoveVoteCommand.Execute(position);

                            if (!partecipation.Votes.Contains(user.Email))
                            {
                                int newcount = partecipation.Votes.Count;
                                Int32.TryParse(myHolder.Votes.Text, out newcount);
                                myHolder.Votes.Text = (newcount - 1).ToString();
                                myHolder.AddVote.Activated = false;
                            }
                        }
                        myHolder.AddVote.Enabled = true;
                    }
                };
                myHolder.AddVote.Click += myHolder.OnAddVoteClick;

                myHolder.OnCommentClick = (object sender, EventArgs e) =>
                {
                    myHolder.Comment.Clickable = false;
                    var intent = new Intent(activity, typeof(PartecipationCommentActivity));
                    intent.PutExtra("partecipationId", partecipation.Id);
                    activity.StartActivity(intent);
                    activity.OverridePendingTransition(Resource.Animator.EnterBottomToTop, Resource.Animator.Still);
                    myHolder.Comment.Clickable = true;
                };
                myHolder.Comment.Click += myHolder.OnCommentClick;

                myHolder.OnMediaClick = (object sender, EventArgs e) =>
                {
                    fragment.ImageZoom(myHolder.Media, partecipation.Media);
                };
                myHolder.Media.Click += myHolder.OnMediaClick;

                myHolder.OnShareClick = (object sender, EventArgs e) =>
                {
                    var intent = new Intent(activity, typeof(AddShareActivity));
                    intent.PutExtra("partecipation", partecipation.Id);
                    activity.StartActivity(intent);
                };
                myHolder.Share.Click += myHolder.OnShareClick;

                if (user.Partecipations.Contains(user.Email + partecipation.PartecipatingToContest))
                {
                    myHolder.Partecipate.Activated = true;
                    myHolder.Partecipate.Text = "Partecipando";
                    myHolder.Partecipate.SetTextColor(Color.White);
                }
                else
                {
                    myHolder.Partecipate.Activated = false;
                    myHolder.Partecipate.Text = "Partecipa";
                    myHolder.Partecipate.SetTextColor(ContextCompat.GetColorStateList(activity, Resource.Color.mediumFont));
                }

                myHolder.OnPartecipateClick = (object sender, EventArgs e) =>
                {
                    myHolder.Partecipate.Clickable = false;
                    if (!myHolder.Partecipate.Activated)
                    {
                        var newIntent = new Intent(activity, typeof(AddPartecipationActivity));
                        newIntent.PutExtra("partecipation", Newtonsoft.Json.JsonConvert.SerializeObject(partecipation));
                        requestFromIndex = position;
                        MediaHelper.GetInstance().SetBanner(null);
                        MediaHelper.GetInstance().SetResult(null);
                        activity.StartActivityForResult(newIntent, PARTECIPATE_REQUEST_CODE);
                    }
                    else
                    {
                        AlertDialog.Builder builder = new AlertDialog.Builder(activity, Resource.Style.WarningAlertDialogStyle);
                        builder.SetTitle("Vuoi annullare la tua partecipazione?");
                        builder.SetMessage("Una volta confermato la partecipazione sparirà per sempre e perderai tutti i voti legati ad essa.");
                        builder.SetPositiveButton("SI", (senderAlert, args) => {
                            ((MainActivity)activity).ProgressB.Animation = ((MainActivity)activity).inAnimation;
                            ((MainActivity)activity).ProgressB.Visibility = ViewStates.Visible;
                            PartecipationViewModel partecipationViewModel = new PartecipationViewModel();
                            partecipationViewModel.RetryDeletePartecipationCommand.Execute(user.Email + partecipation.PartecipatingToContest);
                        });
                        builder.SetNegativeButton("NO", (senderAlert, args) => {
                        });
                        builder.Create().Show();
                    }
                    myHolder.Partecipate.Clickable = true;
                };
                myHolder.Partecipate.Click += myHolder.OnPartecipateClick;

                myHolder.OnUserClick = (object sender, EventArgs e) =>
                {
                    if (user.Email == partecipation.CreatorUser)
                    {
                        var intent = new Intent(activity, typeof(PersonalUserPageActivity));
                        MediaHelper.GetInstance().ClearBitmaps();
                        activity.StartActivity(intent);
                    }
                    else
                    {
                        var intent = new Intent(activity, typeof(UserPageActivity));
                        intent.PutExtra("userId", partecipation.CreatorUser);
                        intent.PutExtra("username", partecipation.CreatorUserName);
                        intent.PutExtra("avatar", partecipation.Avatar);
                        activity.StartActivity(intent);
                    }
                };
                myHolder.Username.Click += myHolder.OnUserClick;
                myHolder.Avatar.Click += myHolder.OnUserClick;

                myHolder.OnContestClick = (object sender, EventArgs e) =>
                {
                    var intent = new Intent(activity, typeof(ContestDetailActivity));
                    MediaHelper.GetInstance().ClearBitmaps();
                    intent.PutExtra("contestId", partecipation.PartecipatingToContest);
                    activity.StartActivity(intent);
                };
                myHolder.Contest.Click += myHolder.OnContestClick;

            }
            else if (holder.GetType().Equals(typeof(ShareViewHolder)))
            {
                var myHolder = holder as ShareViewHolder;
                var share = item as Share;

                myHolder.Username.Text = share.CreatorUserName;
                myHolder.OriginalUsername.Text = share.OriginalUserName;
                myHolder.Date.Text = PrettyDate.GetPrettyDate(share.SortingDate);
                myHolder.OriginalDate.Text = PrettyDate.GetPrettyDate(share.OriginalDate);
                myHolder.OriginalDescription.Text = share.OriginalContent;
                myHolder.OriginalContest.Text = share.ContestName;
                if(share.PartecipationId != null)
                {
                    myHolder.Verb.Text = "Partecipa a:";
                }
                else if (share.ContestId != null)
                {
                    myHolder.Verb.Text = "Ha creato il Contest:";
                }

                if (share.Description != null)
                {
                    myHolder.Description.Visibility = ViewStates.Visible;
                    myHolder.Description.Text = share.Description;
                }
                else
                {
                    myHolder.Description.Visibility = ViewStates.Gone;
                }

                if (share.OriginalContent != null)
                {
                    myHolder.OriginalDescription.Visibility = ViewStates.Visible;
                    myHolder.OriginalDescription.Text = share.OriginalContent;
                }
                if (share.ContestEndDate != null)
                {
                    if (DateTime.Compare(share.ContestEndDate, DateTime.Now) <= 0)
                    {
                        myHolder.Partecipate.Visibility = ViewStates.Gone;
                        myHolder.Partecipate.Clickable = false;
                    }
                    else
                    {
                        myHolder.Partecipate.Visibility = ViewStates.Visible;
                        myHolder.Partecipate.Clickable = true;
                    }
                }
                else
                {
                    myHolder.Partecipate.Visibility = ViewStates.Visible;
                    myHolder.Partecipate.Clickable = true;
                }

                if (share.Media != null)
                {
                    Glide.With(activity).Load(share.Media).Into(myHolder.Media);
                    myHolder.Media.Clickable = true;
                }
                else
                {
                    myHolder.Media.SetImageResource(Resource.Color.imageBlank);
                    myHolder.Media.Clickable = false;
                }

                if (share.Avatar != null)
                {
                    Glide.With(activity).Load(share.Avatar).Into(myHolder.Avatar);
                }
                else
                {
                    myHolder.Avatar.SetImageResource(Resource.Color.imageBlank);
                }
                if (share.OriginalAvatar != null)
                {
                    Glide.With(activity).Load(share.OriginalAvatar).Into(myHolder.OriginalAvatar);
                }
                else
                {
                    myHolder.Avatar.SetImageResource(Resource.Color.imageBlank);
                }


                myHolder.Votes.Text = share.Votes.Count.ToString();
                myHolder.Shares.Text = share.Shares.ToString();
                myHolder.Comments.Text = share.Comments.Count.ToString();


                if (share.Votes.Contains(user.Email))
                {
                    myHolder.AddVote.Activated = true;
                }

                myHolder.OnAddVoteClick = (object sender, EventArgs e) =>
                {
                    if (DateTime.Compare(share.ContestEndDate, DateTime.Now) <= 0)
                    {
                        Toast.MakeText(activity, "Il contest è finito!", ToastLength.Short).Show();
                    }
                    else
                    {
                        myHolder.AddVote.Enabled = false;
                        if (!myHolder.AddVote.Activated)
                        {
                            ((HomeViewModel)viewModel).RetryAddVoteCommand.Execute(position);

                            if (share.Votes.Contains(user.Email))
                            {
                                int newcount = share.Votes.Count;
                                Int32.TryParse(myHolder.Votes.Text, out newcount);
                                myHolder.Votes.Text = (newcount + 1).ToString();
                                myHolder.AddVote.Activated = true;
                            }

                        }
                        else
                        {
                            ((HomeViewModel)viewModel).RetryRemoveVoteCommand.Execute(position);

                            if (!share.Votes.Contains(user.Email))
                            {
                                int newcount = share.Votes.Count;
                                Int32.TryParse(myHolder.Votes.Text, out newcount);
                                myHolder.Votes.Text = (newcount - 1).ToString();
                                myHolder.AddVote.Activated = false;
                            }
                        }
                        myHolder.AddVote.Enabled = true;
                    }
                };
                myHolder.AddVote.Click += myHolder.OnAddVoteClick;

                myHolder.OnCommentClick = (object sender, EventArgs e) =>
                {
                    //TODO
                    //SE share.partecipationID != null -> ho una partecipazione -> faccio lo stesso metodo che apre i commenti della partecipazione (vedere la parte fragment della partecipazione)
                    //Altrimenti fare la cosa del dirigere nel contest

                    myHolder.Comment.Clickable = false;
                    var intent = new Intent(activity, typeof(ShareCommentActivity));
                    intent.PutExtra("shareId", share.Id);
                    activity.StartActivity(intent);
                    activity.OverridePendingTransition(Resource.Animator.EnterBottomToTop, Resource.Animator.Still);
                    myHolder.Comment.Clickable = true;
                };
                myHolder.Comment.Click += myHolder.OnCommentClick;

                if (share.Media != null)
                {
                    myHolder.Media.Clickable = true;
                }
                else
                {
                    myHolder.Media.Clickable = false;
                }

                myHolder.OnMediaClick = (object sender, EventArgs e) =>
                {
                    fragment.ImageZoom(myHolder.Media, share.Media);
                };
                myHolder.Media.Click += myHolder.OnMediaClick;

                myHolder.OnShareClick = (object sender, EventArgs e) =>
                {
                    var intent = new Intent(activity, typeof(AddShareActivity));
                    intent.PutExtra("share", share.Id);
                    activity.StartActivity(intent);
                };
                myHolder.Share.Click += myHolder.OnShareClick;

                if (user.Partecipations.Contains(user.Email + share.ContestId))
                {
                    myHolder.Partecipate.Activated = true;
                    myHolder.Partecipate.Text = "Partecipando";
                    myHolder.Partecipate.SetTextColor(Color.White);
                }
                else
                {
                    myHolder.Partecipate.Activated = false;
                    myHolder.Partecipate.Text = "Partecipa";
                    myHolder.Partecipate.SetTextColor(ContextCompat.GetColorStateList(activity, Resource.Color.mediumFont));
                }

                myHolder.OnPartecipateClick = (object sender, EventArgs e) =>
                {
                    myHolder.Partecipate.Enabled = false;
                    if (!myHolder.Partecipate.Activated)
                    {
                        var newIntent = new Intent(activity, typeof(AddPartecipationActivity));
                        newIntent.PutExtra("share", Newtonsoft.Json.JsonConvert.SerializeObject(share));
                        requestFromIndex = position;
                        MediaHelper.GetInstance().SetBanner(null);
                        MediaHelper.GetInstance().SetResult(null);
                        activity.StartActivityForResult(newIntent, PARTECIPATE_REQUEST_CODE);
                    }
                    else
                    {
                        AlertDialog.Builder builder = new AlertDialog.Builder(activity, Resource.Style.WarningAlertDialogStyle);
                        builder.SetTitle("Vuoi annullare la tua partecipazione?");
                        builder.SetMessage("Una volta confermato la partecipazione sparirà per sempre e perderai tutti i voti legati ad essa.");
                        builder.SetPositiveButton("SI", (senderAlert, args) => {
                            ((MainActivity)activity).ProgressB.Animation = ((MainActivity)activity).inAnimation;
                            ((MainActivity)activity).ProgressB.Visibility = ViewStates.Visible;
                            PartecipationViewModel partecipationViewModel = new PartecipationViewModel();
                            partecipationViewModel.RetryDeletePartecipationCommand.Execute(user.Email + share.ContestId);
                        });
                        builder.SetNegativeButton("NO", (senderAlert, args) => {
                        });
                        builder.Create().Show();
                    }
                    myHolder.Partecipate.Enabled = true;
                };
                myHolder.Partecipate.Click += myHolder.OnPartecipateClick;

                myHolder.OnOriginalUserClick = (object sender, EventArgs e) =>
                {
                    if (user.Email == share.OriginalUser)
                    {
                        var intent = new Intent(activity, typeof(PersonalUserPageActivity));
                        MediaHelper.GetInstance().ClearBitmaps();
                        activity.StartActivity(intent);
                    }
                    else
                    {
                        var intent = new Intent(activity, typeof(UserPageActivity));
                        intent.PutExtra("userId", share.OriginalUser);
                        intent.PutExtra("username", share.OriginalUserName);
                        intent.PutExtra("avatar", share.OriginalAvatar);
                        activity.StartActivity(intent);
                    }
                };

                myHolder.OnSharingUserClick = (object sender, EventArgs e) =>
                {
                    if (user.Email == share.CreatorUser)
                    {
                        var intent = new Intent(activity, typeof(PersonalUserPageActivity));
                        MediaHelper.GetInstance().ClearBitmaps();
                        activity.StartActivity(intent);
                    }
                    else
                    {
                        var intent = new Intent(activity, typeof(UserPageActivity));
                        intent.PutExtra("userId", share.CreatorUser);
                        intent.PutExtra("username", share.CreatorUserName);
                        intent.PutExtra("avatar", share.Avatar);
                        activity.StartActivity(intent);
                    }
                };

                myHolder.Username.Click += myHolder.OnSharingUserClick;
                myHolder.Avatar.Click += myHolder.OnSharingUserClick;
                myHolder.OriginalUsername.Click += myHolder.OnOriginalUserClick;
                myHolder.OriginalAvatar.Click += myHolder.OnOriginalUserClick;

                myHolder.OnOriginalContenstClick = (object sender, EventArgs e) =>
                {
                    var intent = new Intent(activity, typeof(ContestDetailActivity));
                    intent.PutExtra("contestId", share.ContestId);
                    MediaHelper.GetInstance().ClearBitmaps();
                    activity.StartActivity(intent);
                };
                myHolder.OriginalContest.Click += myHolder.OnOriginalContenstClick;
            }
        }



        public override void OnViewRecycled(Java.Lang.Object holder)
        {
            base.OnViewRecycled(holder);

            if (holder.GetType().Equals(typeof(ShareViewHolder)))
            {
                ShareViewHolder viewHolder = holder as ShareViewHolder;
                viewHolder.AddVote.Activated = false;
                viewHolder.Username.Click -= viewHolder.OnSharingUserClick;
                viewHolder.Avatar.Click -= viewHolder.OnSharingUserClick;
                viewHolder.OriginalUsername.Click -= viewHolder.OnOriginalUserClick;
                viewHolder.OriginalAvatar.Click -= viewHolder.OnOriginalUserClick;
                viewHolder.OriginalContest.Click -= viewHolder.OnOriginalContenstClick;
                viewHolder.Media.Click -= viewHolder.OnMediaClick;
                viewHolder.AddVote.Click -= viewHolder.OnAddVoteClick;
                viewHolder.Comment.Click -= viewHolder.OnCommentClick;
                viewHolder.Share.Click -= viewHolder.OnShareClick;
                viewHolder.Partecipate.Click -= viewHolder.OnPartecipateClick;
                viewHolder.Description.Text = null;
            }
            else if  (holder.GetType().Equals(typeof(PartecipationViewHolder)))

            {
                PartecipationViewHolder viewHolder = holder as PartecipationViewHolder;
                viewHolder.AddVote.Activated = false;
                viewHolder.Username.Click -= viewHolder.OnUserClick;
                viewHolder.Avatar.Click -= viewHolder.OnUserClick;
                viewHolder.Contest.Click -= viewHolder.OnContestClick;
                viewHolder.Media.Click -= viewHolder.OnMediaClick;
                viewHolder.AddVote.Click -= viewHolder.OnAddVoteClick;
                viewHolder.Comment.Click -= viewHolder.OnCommentClick;
                viewHolder.Share.Click -= viewHolder.OnShareClick;
                viewHolder.Partecipate.Click -= viewHolder.OnPartecipateClick;
                viewHolder.Description.Text = null;
            }
            else if (holder.GetType().Equals(typeof(ContestViewHolder)))
            {
                ContestViewHolder viewHolder = holder as ContestViewHolder;
                viewHolder.AddVote.Activated = false;
                viewHolder.Username.Click -= viewHolder.OnUserClick;
                viewHolder.Avatar.Click -= viewHolder.OnUserClick;
                viewHolder.Contest.Click -= viewHolder.OnContestClick;
                viewHolder.Media.Click -= viewHolder.OnMediaClick;
                viewHolder.AddVote.Click -= viewHolder.OnAddLikeClick;
                viewHolder.Comment.Click -= viewHolder.OnCommentClick;
                viewHolder.Share.Click -= viewHolder.OnShareClick;
                viewHolder.Partecipate.Click -= viewHolder.OnPartecipateClick;
                viewHolder.Description.Text = null;
            }

        }

    }

   
}
