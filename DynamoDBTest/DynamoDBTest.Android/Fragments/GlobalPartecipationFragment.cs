﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;

using DynamoDBTest.ViewModels;
using DynamoDBTest.Models;
using DynamoDBTest.Helpers;
using DynamoDBTest.Services;

namespace DynamoDBTest.Droid
{
    public class GlobalPartecipationFragment : AbstractRefresherFragment<Partecipation>
    {
        public static GlobalPartecipationFragment NewInstance() =>
            new GlobalPartecipationFragment { Arguments = new Bundle() };

        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            PartecipationViewModel viewModel = new PartecipationViewModel();
            base.ViewModel = viewModel;
            base.LoadItemsCommand = viewModel.RetryLoadItemsCommand;
            base.adapter = new PartecipationsAdapter(Activity, viewModel);
            base.adapter.fragment = this;
            return base.OnCreateView(inflater, container, savedInstanceState);
        }

        public override void Adapter_ItemClick(object sender, RecyclerClickEventArgs e)
        {
            
        }

        public override void OnLoadItemsError(object sender, EventArgs a)
        {
            base.OnLoadItemsError(sender, a);

            CommandCompletedEventArgs args = a as CommandCompletedEventArgs;
            if (args.Status == CommandStates.MaxRetryReached)
            {
            }
        }

        public override void OnActivityResult(int requestCode, int resultCode, Intent data)
        {
            base.OnActivityResult(requestCode, resultCode, data);

            if (requestCode == PartecipationsAdapter.PARTECIPATE_REQUEST_CODE)
            {
                if (resultCode == 0)
                {
                    Partecipation newPartecipation = GlobalCollections.GetInstance().GetPartecipation(data.GetStringExtra("newPartecipationId"));
                    ((PartecipationViewModel)base.ViewModel).PutItem(newPartecipation);
                    AuthenticationLoopCheck.GetInstance().DoOne();
                }
            }

            ((MainActivity)Activity).ProgressB.Animation = ((MainActivity)Activity).outAnimation;
            ((MainActivity)Activity).ProgressB.Visibility = ViewStates.Gone;
        }
    }
}