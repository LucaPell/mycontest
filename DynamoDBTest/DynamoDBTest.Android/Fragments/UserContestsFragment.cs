﻿using System;

using Android.App;
using Android.OS;
using Android.Views;
using Android.Widget;
using Android.Support.V7.Widget;

using DynamoDBTest.ViewModels;
using DynamoDBTest.Models;
using DynamoDBTest.Helpers;
using DynamoDBTest.Services;
using Com.Bumptech.Glide;
using DynamoDBTest.Controllers;
using Android.Content;
using Android.Graphics;
using Android.Support.V4.Content;

namespace DynamoDBTest.Droid
{
    public class UserContestsFragment : AbstractRefresherFragment<Contest>
    {
        public static UserContestsFragment NewInstance() =>
            new UserContestsFragment { Arguments = new Bundle() };


        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            ContestViewModel viewModel = new ContestViewModel();
            base.ViewModel = viewModel;
            base.param = Activity.Intent.GetStringExtra("userId");
            base.adapter = new ContestsAdapter(Activity, viewModel);
            base.adapter.fragment = this;
            var view = base.OnCreateView(inflater, container, savedInstanceState);
            var listener = new CollapsingPlusRecyclerListener(Activity);
            recyclerView.AddOnScrollListener(listener);
            base.LoadItemsCommand = viewModel.RetryLoadAllByCreatorUserCommand;
            return view;
        }

        public override void Adapter_ItemClick(object sender, RecyclerClickEventArgs e)
        {
            var item = ViewModel.Items[e.Position];
            Intent intent = new Intent(Activity, typeof(ContestDetailActivity));
            MediaHelper.GetInstance().ClearBitmaps();
            intent.PutExtra("data", Newtonsoft.Json.JsonConvert.SerializeObject(item));

            Activity.StartActivity(intent);
        }

        public override void OnLoadItemsError(object sender, EventArgs a)
        {
            base.OnLoadItemsError(sender, a);

            CommandCompletedEventArgs args = a as CommandCompletedEventArgs;
            if (args.Status == CommandStates.MaxRetryReached)
            {
            }
        }

        public override void OnActivityResult(int requestCode, int resultCode, Intent data)
        {
            base.OnActivityResult(requestCode, resultCode, data);

            if (requestCode == ContestsAdapter.CONTEST_DETAIL_REQUEST_CODE)
            {
                if (resultCode == 0)
                {
                    Contest contestUpdated = GlobalCollections.GetInstance().GetContest(data.GetStringExtra("contestUpdatedId"));
                    ViewModel.Items.RemoveAt(((ContestsAdapter)adapter).requestFromIndex);
                    ViewModel.Items.Insert(((ContestsAdapter)adapter).requestFromIndex, contestUpdated);
                }
            }

            if (Activity.GetType().Equals(typeof(UserPageActivity)))
            {
                ((UserPageActivity)Activity).ProgressB.Animation = ((UserPageActivity)Activity).outAnimation;
                ((UserPageActivity)Activity).ProgressB.Visibility = ViewStates.Gone;
            } else
            {
                ((PersonalUserPageActivity)Activity).ProgressB.Animation = ((PersonalUserPageActivity)Activity).outAnimation;
                ((PersonalUserPageActivity)Activity).ProgressB.Visibility = ViewStates.Gone;
            }
        }
    }

    public class ContestsAdapter : ItemsAdapter<Contest>
    {
        public static ushort PARTECIPATE_REQUEST_CODE = 10;
        public static ushort CONTEST_DETAIL_REQUEST_CODE = 20;

        public int requestFromIndex;

        PartecipationDetailViewModel victoryPartecipationViewModel = new PartecipationDetailViewModel();

        public ContestsAdapter(Activity activity, ICollectioViewModel<Contest> viewModel) : base(activity, viewModel)
        {
            
        }

        public override RecyclerView.ViewHolder OnCreateViewHolder(ViewGroup parent, int viewType)
        {
            View itemView = null;
            var id = Resource.Layout.contest_browse;
            itemView = LayoutInflater.From(parent.Context).Inflate(id, parent, false);

            var vh = new ContestViewHolder(itemView, OnClick, OnLongClick);
            return vh;
        }

        // Replace the contents of a view (invoked by the layout manager)
        public override void OnBindViewHolder(RecyclerView.ViewHolder holder, int position)
        {
            var contest = base.viewModel.Items[position];
            var user = LoginController.GetInstance().CurrentUser;

            var myHolder = holder as ContestViewHolder;
            myHolder.Username.Text = contest.CreatorUserName;
            myHolder.Date.Text = PrettyDate.GetPrettyDate(contest.SortingDate);
            myHolder.Contest.Text = contest.Name;
            if (contest.Description != null)
            {
                myHolder.Description.Visibility = ViewStates.Visible;
                myHolder.Description.Text = contest.Description;
            }
            else
            {
                myHolder.Description.Visibility = ViewStates.Gone;
            }

            if (DateTime.Compare(contest.EndDate, DateTime.Now) <= 0)
            {
                myHolder.Partecipate.Visibility = ViewStates.Gone;
                myHolder.Partecipate.Clickable = false;

                if (contest.VictoryPartecipation == null)
                {
                    victoryPartecipationViewModel.RetryLoadVictoryPartecipationNoSaveCommand.Execute(contest.Id);
                }
            }
            else
            {
                myHolder.Partecipate.Visibility = ViewStates.Visible;
                myHolder.Partecipate.Clickable = true;
            }

            if (contest.Banner != null)
            {
                Glide.With(activity).Load(contest.Banner).Into(myHolder.Media);
                myHolder.Media.Clickable = true;
            }
            else
            {
                myHolder.Media.SetImageResource(Resource.Color.imageBlank);
                myHolder.Media.Clickable = false;
            }

            if (contest.Avatar != null)
            {
                Glide.With(activity).Load(contest.Avatar).Into(myHolder.Avatar);
            }
            else
            {
                myHolder.Avatar.SetImageResource(Resource.Color.imageBlank);
            }
            myHolder.Votes.Text = contest.Votes.Count.ToString();
            myHolder.Shares.Text = contest.Shares.ToString();
            myHolder.Comments.Text = contest.Comments.Count.ToString();


            if (contest.Votes.Contains(user.Email))
            {
                myHolder.AddVote.Activated = true;
            }

            myHolder.OnAddLikeClick = (object sender, EventArgs e) =>
            {
                myHolder.AddVote.Enabled = false;
                if (!myHolder.AddVote.Activated)
                {
                    ((ContestViewModel)viewModel).RetryAddVoteCommand.Execute(position);

                    if (contest.Votes.Contains(user.Email))
                    {
                        int newcount = contest.Votes.Count;
                        Int32.TryParse(myHolder.Votes.Text, out newcount);
                        myHolder.Votes.Text = (newcount + 1).ToString();
                        myHolder.AddVote.Activated = true;
                    }

                }
                else
                {
                    ((ContestViewModel)viewModel).RetryRemoveVoteCommand.Execute(position);

                    if (!contest.Votes.Contains(user.Email))
                    {
                        int newcount = contest.Votes.Count;
                        Int32.TryParse(myHolder.Votes.Text, out newcount);
                        myHolder.Votes.Text = (newcount - 1).ToString();
                        myHolder.AddVote.Activated = false;
                    }
                }
                myHolder.AddVote.Enabled = true;
            };
            myHolder.AddVote.Click += myHolder.OnAddLikeClick;

            myHolder.OnCommentClick = (object sender, EventArgs e) =>
            {
                var intent = new Intent(activity, typeof(ContestDetailActivity));

                intent.PutExtra("data", Newtonsoft.Json.JsonConvert.SerializeObject(contest));
                intent.PutExtra("tab", 1);
                activity.StartActivity(intent);
            };
            myHolder.Comment.Click += myHolder.OnCommentClick;

            myHolder.OnMediaClick = (object sender, EventArgs e) =>
            {
                fragment.ImageZoom(myHolder.Media, contest.Banner);
            };
            myHolder.Media.Click += myHolder.OnMediaClick;

            myHolder.OnShareClick = (object sender, EventArgs e) =>
            {
                var intent = new Intent(activity, typeof(AddShareActivity));
                intent.PutExtra("contest", contest.Id);
                activity.StartActivity(intent);
            };
            myHolder.Share.Click += myHolder.OnShareClick;

            if (user.Partecipations.Contains(user.Email + contest.Id))
            {
                myHolder.Partecipate.Activated = true;
                myHolder.Partecipate.Text = "Partecipando";
                myHolder.Partecipate.SetTextColor(Color.White);
            }
            else
            {
                myHolder.Partecipate.Activated = false;
                myHolder.Partecipate.Text = "Partecipa";
                myHolder.Partecipate.SetTextColor(ContextCompat.GetColorStateList(activity, Resource.Color.mediumFont));
            }

            myHolder.OnPartecipateClick = (object sender, EventArgs e) =>
            {
                myHolder.Partecipate.Clickable = false;
                if (!myHolder.Partecipate.Activated)
                {
                    var newIntent = new Intent(activity, typeof(AddPartecipationActivity));
                    newIntent.PutExtra("data", Newtonsoft.Json.JsonConvert.SerializeObject(contest));
                    newIntent.PutExtra("fragment", "OK");
                    requestFromIndex = position;
                    MediaHelper.GetInstance().SetBanner(null);
                    MediaHelper.GetInstance().SetResult(null);
                    activity.StartActivityForResult(newIntent, PARTECIPATE_REQUEST_CODE);
                }
                else
                {
                    AlertDialog.Builder builder = new AlertDialog.Builder(activity, Resource.Style.WarningAlertDialogStyle);
                    builder.SetTitle("Vuoi annullare la tua partecipazione?");
                    builder.SetMessage("Una volta confermato la partecipazione sparirà per sempre e perderai tutti i voti legati ad essa.");
                    builder.SetPositiveButton("SI", (senderAlert, args) => {
                        if (activity.GetType().Equals(typeof(UserPageActivity)))
                        {
                            ((UserPageActivity)activity).ProgressB.Animation = ((UserPageActivity)activity).outAnimation;
                            ((UserPageActivity)activity).ProgressB.Visibility = ViewStates.Gone;
                        }
                        else if(activity.GetType().Equals(typeof(PersonalUserPageActivity)))
                        {
                            ((PersonalUserPageActivity)activity).ProgressB.Animation = ((PersonalUserPageActivity)activity).outAnimation;
                            ((PersonalUserPageActivity)activity).ProgressB.Visibility = ViewStates.Gone;
                        }
                        else if (activity.GetType().Equals(typeof(MainActivity)))
                        {
                            ((MainActivity)activity).ProgressB.Animation = ((MainActivity)activity).inAnimation;
                            ((MainActivity)activity).ProgressB.Visibility = ViewStates.Visible;
                        }
                        PartecipationViewModel partecipationViewModel = new PartecipationViewModel();
                        partecipationViewModel.RetryDeletePartecipationCommand.Execute(user.Email + contest.Id);
                    });
                    builder.SetNegativeButton("NO", (senderAlert, args) => {
                    });
                    builder.Create().Show();
                }
                myHolder.Partecipate.Clickable = true;
            };
            myHolder.Partecipate.Click += myHolder.OnPartecipateClick;

            myHolder.OnUserClick = (object sender, EventArgs e) =>
            {
                if (user.Email == contest.CreatorUser)
                {
                    var intent = new Intent(activity, typeof(PersonalUserPageActivity));
                    MediaHelper.GetInstance().ClearBitmaps();
                    activity.StartActivity(intent);
                }
                else
                {
                    var intent = new Intent(activity, typeof(UserPageActivity));
                    intent.PutExtra("userId", contest.CreatorUser);
                    intent.PutExtra("username", contest.CreatorUserName);
                    intent.PutExtra("avatar", contest.Avatar);
                    activity.StartActivity(intent);
                }
            };
            myHolder.Username.Click += myHolder.OnUserClick;
            myHolder.Avatar.Click += myHolder.OnUserClick;

            myHolder.OnContestClick = (object sender, EventArgs e) =>
            {
                var intent = new Intent(activity, typeof(ContestDetailActivity));
                MediaHelper.GetInstance().ClearBitmaps();
                intent.PutExtra("data", Newtonsoft.Json.JsonConvert.SerializeObject(contest));
                requestFromIndex = position;
                activity.StartActivityForResult(intent, CONTEST_DETAIL_REQUEST_CODE);
            };
            myHolder.Contest.Click += myHolder.OnContestClick;

        }

        public override void OnViewRecycled(Java.Lang.Object holder)
        {
            base.OnViewRecycled(holder);

            ContestViewHolder viewHolder = holder as ContestViewHolder;
            viewHolder.AddVote.Activated = false;
            viewHolder.Username.Click -= viewHolder.OnUserClick;
            viewHolder.Avatar.Click -= viewHolder.OnUserClick;
            viewHolder.Contest.Click -= viewHolder.OnContestClick;
            viewHolder.Media.Click -= viewHolder.OnMediaClick;
            viewHolder.AddVote.Click -= viewHolder.OnAddLikeClick;
            viewHolder.Comment.Click -= viewHolder.OnCommentClick;
            viewHolder.Share.Click -= viewHolder.OnShareClick;
            viewHolder.Partecipate.Click -= viewHolder.OnPartecipateClick;
            viewHolder.Description.Text = null;
        }
    }
}