﻿using Android.OS;
using Android.Views;
using Android.Content;

using DynamoDBTest.ViewModels;
using DynamoDBTest.Models;
using DynamoDBTest.Helpers;

using System;
using System.Collections.Generic;
using DynamoDBTest.Services;

namespace DynamoDBTest.Droid
{
    public class UserPartecipationsFragment : AbstractRefresherFragment<Partecipation>
    {
        public static UserPartecipationsFragment NewInstance() =>
            new UserPartecipationsFragment { Arguments = new Bundle() };

        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            PartecipationViewModel viewModel = new PartecipationViewModel();
            base.ViewModel = viewModel;
            base.param = Activity.Intent.GetStringExtra("userId");
            base.adapter = new PartecipationsAdapter(Activity, viewModel);
            base.adapter.fragment = this;
            var view = base.OnCreateView(inflater, container, savedInstanceState);
            var listener = new CollapsingPlusRecyclerListener(Activity);
            recyclerView.AddOnScrollListener(listener);
            base.LoadItemsCommand = viewModel.RetryLoadItemsByUserCommand;

            return view;
        }

        public override void Adapter_ItemClick(object sender, RecyclerClickEventArgs e)
        {
            
        }

        public override void OnLoadItemsError(object sender, EventArgs a)
        {
            base.OnLoadItemsError(sender, a);

            CommandCompletedEventArgs args = a as CommandCompletedEventArgs;
            if (args.Status == CommandStates.MaxRetryReached)
            {
            }
        }

        public override void OnActivityResult(int requestCode, int resultCode, Intent data)
        {
            base.OnActivityResult(requestCode, resultCode, data);

            if (requestCode == PartecipationsAdapter.PARTECIPATE_REQUEST_CODE)
            {
                if (resultCode == 0)
                {
                    Partecipation newPartecipation = GlobalCollections.GetInstance().GetPartecipation(data.GetStringExtra("newPartecipationId"));
                    ((PartecipationViewModel)base.ViewModel).PutItem(newPartecipation);
                    AuthenticationLoopCheck.GetInstance().DoOne();
                }
            }

            if (Activity.GetType().Equals(typeof(UserPageActivity)))
            {
                ((UserPageActivity)Activity).ProgressB.Animation = ((UserPageActivity)Activity).outAnimation;
                ((UserPageActivity)Activity).ProgressB.Visibility = ViewStates.Gone;
            }
            else
            {
                ((PersonalUserPageActivity)Activity).ProgressB.Animation = ((PersonalUserPageActivity)Activity).outAnimation;
                ((PersonalUserPageActivity)Activity).ProgressB.Visibility = ViewStates.Gone;
            }
        }
    }
}