﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android.Animation;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Support.V7.Widget;
using Android.Views;
using Android.Widget;
using Refractored.Controls;

namespace DynamoDBTest.Droid
{
    public class ShareViewHolder : RecyclerView.ViewHolder
    {
        //chi ha fatto lo share
        public TextView Username { get; set; }
        public TextView Date { get; set; }
        public TextView Description { get; set; }
        public CircleImageView Avatar { get; set; }
        public TextView Verb { get; set; }

        //a chi si riferisce
        public TextView OriginalUsername { get; set; }
        public TextView OriginalContest { get; set; }
        public TextView OriginalDescription { get; set; }
        public TextView OriginalDate { get; set; }
        public CircleImageView OriginalAvatar { get; set; }
        public TextView OriginalVerb { get; set; }

        public ImageButton Media { get; set; }
        public ImageButton AddVote { get; set; }
        public ImageButton Share { get; set; }
        public ImageButton Comment { get; set; }
        public TextView Votes { get; set; }
        public TextView Shares { get; set; }
        public TextView Comments { get; set; }
        public Button Partecipate { get; set; }

        public EventHandler OnSharingUserClick;
        public EventHandler OnOriginalUserClick;
        public EventHandler OnOriginalContenstClick;
        public EventHandler OnMediaClick;
        public EventHandler OnAddVoteClick;
        public EventHandler OnCommentClick;
        public EventHandler OnShareClick;
        public EventHandler OnPartecipateClick;

        public ShareViewHolder(View itemView, Action<RecyclerClickEventArgs> clickListener,
                            Action<RecyclerClickEventArgs> longClickListener) : base(itemView)
        {
            Username = itemView.FindViewById<TextView>(Resource.Id.User);
            Date = itemView.FindViewById<TextView>(Resource.Id.date);
            Description = itemView.FindViewById<TextView>(Resource.Id.desc);
            Avatar = itemView.FindViewById<CircleImageView>(Resource.Id.avatar);
            Verb = itemView.FindViewById<TextView>(Resource.Id.verb2);

            OriginalUsername = itemView.FindViewById<TextView>(Resource.Id.origUser);
            OriginalDate = itemView.FindViewById<TextView>(Resource.Id.origDate);
            OriginalContest = itemView.FindViewById<TextView>(Resource.Id.origContent);
            OriginalDescription = itemView.FindViewById<TextView>(Resource.Id.origDesc);
            OriginalAvatar = itemView.FindViewById<CircleImageView>(Resource.Id.origAvatar);
            OriginalVerb = itemView.FindViewById<TextView>(Resource.Id.verb2);

            Media = itemView.FindViewById<ImageButton>(Resource.Id.media);
            AddVote = itemView.FindViewById<ImageButton>(Resource.Id.addVote);
            Share = itemView.FindViewById<ImageButton>(Resource.Id.share);
            Comment = itemView.FindViewById<ImageButton>(Resource.Id.comment);
            Votes = itemView.FindViewById<TextView>(Resource.Id.votes);
            Shares = itemView.FindViewById<TextView>(Resource.Id.shares);
            Comments = itemView.FindViewById<TextView>(Resource.Id.comments);
            Partecipate = itemView.FindViewById<Button>(Resource.Id.partecipate);

            itemView.Click += (sender, e) => clickListener(new RecyclerClickEventArgs { View = itemView, Position = AdapterPosition });
            itemView.LongClick += (sender, e) => longClickListener(new RecyclerClickEventArgs { View = itemView, Position = AdapterPosition });
        }
    }

    public class ContestViewHolder : RecyclerView.ViewHolder
    {
        public TextView Username { get; set; }
        public TextView Date { get; set; }
        public TextView Description { get; set; }
        public CircleImageView Avatar { get; set; }
        public TextView Contest { get; set; }

        public ImageButton Media { get; set; }
        public ImageButton AddVote { get; set; }
        public ImageButton Share { get; set; }
        public ImageButton Comment { get; set; }
        public TextView Votes { get; set; }
        public TextView Shares { get; set; }
        public TextView Comments { get; set; }
        public Button Partecipate { get; set; }

        public EventHandler OnUserClick;
        public EventHandler OnContestClick;
        public EventHandler OnMediaClick;
        public EventHandler OnAddLikeClick;
        public EventHandler OnCommentClick;
        public EventHandler OnShareClick;
        public EventHandler OnPartecipateClick;

        public ContestViewHolder(View itemView, Action<RecyclerClickEventArgs> clickListener,
                            Action<RecyclerClickEventArgs> longClickListener) : base(itemView)
        {
            Avatar = itemView.FindViewById<CircleImageView>(Resource.Id.avatar);
            Username = itemView.FindViewById<TextView>(Resource.Id.User);
            Date = itemView.FindViewById<TextView>(Resource.Id.date);
            Contest = itemView.FindViewById<TextView>(Resource.Id.contest);
            Description = itemView.FindViewById<TextView>(Resource.Id.desc);

            Media = itemView.FindViewById<ImageButton>(Resource.Id.media);
            AddVote = itemView.FindViewById<ImageButton>(Resource.Id.addLike);
            Share = itemView.FindViewById<ImageButton>(Resource.Id.share);
            Comment = itemView.FindViewById<ImageButton>(Resource.Id.comment);
            Votes = itemView.FindViewById<TextView>(Resource.Id.likes);
            Shares = itemView.FindViewById<TextView>(Resource.Id.shares);
            Comments = itemView.FindViewById<TextView>(Resource.Id.comments);
            Partecipate = itemView.FindViewById<Button>(Resource.Id.partecipate);

            itemView.Click += (sender, e) => clickListener(new RecyclerClickEventArgs { View = itemView, Position = AdapterPosition });
            itemView.LongClick += (sender, e) => longClickListener(new RecyclerClickEventArgs { View = itemView, Position = AdapterPosition });
        }
    }

    public class PartecipationViewHolder : RecyclerView.ViewHolder
    {
        public TextView Username { get; set; }
        public TextView Date { get; set; }
        public TextView Description { get; set; }
        public CircleImageView Avatar { get; set; }
        public TextView Contest { get; set; }
        public TextView Verb { get; set; }

        public ImageButton Media { get; set; }
        public ImageButton AddVote { get; set; }
        public ImageButton Share { get; set; }
        public ImageButton Comment { get; set; }
        public TextView Votes { get; set; }
        public TextView Shares { get; set; }
        public TextView Comments { get; set; }
        public Button Partecipate { get; set; }
        public ImageView Crown { get; set; }

        public EventHandler OnUserClick;
        public EventHandler OnContestClick;
        public EventHandler OnMediaClick;
        public EventHandler OnAddVoteClick;
        public EventHandler OnCommentClick;
        public EventHandler OnShareClick;
        public EventHandler OnPartecipateClick;

        public Animator animator;
        public int mShortAnimationDuration;

        public PartecipationViewHolder(View itemView, Action<RecyclerClickEventArgs> clickListener,
                            Action<RecyclerClickEventArgs> longClickListener) : base(itemView)
        {
            Avatar = itemView.FindViewById<CircleImageView>(Resource.Id.avatar);
            Username = itemView.FindViewById<TextView>(Resource.Id.User);
            Date = itemView.FindViewById<TextView>(Resource.Id.date);
            Contest = itemView.FindViewById<TextView>(Resource.Id.contest);
            Description = itemView.FindViewById<TextView>(Resource.Id.partecipationContent);
            Verb = itemView.FindViewById<TextView>(Resource.Id.verb);
            Crown = itemView.FindViewById<ImageView>(Resource.Id.crown);

            Media = itemView.FindViewById<ImageButton>(Resource.Id.partecipationMedia);
            AddVote = itemView.FindViewById<ImageButton>(Resource.Id.addVote);
            Share = itemView.FindViewById<ImageButton>(Resource.Id.share);
            Comment = itemView.FindViewById<ImageButton>(Resource.Id.comment);
            Votes = itemView.FindViewById<TextView>(Resource.Id.votes);
            Shares = itemView.FindViewById<TextView>(Resource.Id.shares);
            Comments = itemView.FindViewById<TextView>(Resource.Id.comments);
            Partecipate = itemView.FindViewById<Button>(Resource.Id.partecipate);




            itemView.Click += (sender, e) => clickListener(new RecyclerClickEventArgs { View = itemView, Position = AdapterPosition });
            itemView.LongClick += (sender, e) => longClickListener(new RecyclerClickEventArgs { View = itemView, Position = AdapterPosition });
        }
    }

    public class CommentViewHolder : RecyclerView.ViewHolder
    {
        public TextView Username { get; set; }
        public TextView Content { get; set; }
        public TextView Date { get; set; }
        public ImageButton AddVote { get; set; }
        public TextView Votes { get; set; }
        public CircleImageView Avatar { get; set; }
        public EventHandler OnUserClick;
        public EventHandler OnAddLikeClick;
        
        public CommentViewHolder(View itemView, Action<RecyclerClickEventArgs> clickListener,
                            Action<RecyclerClickEventArgs> longClickListener) : base(itemView)
        {
            Username = itemView.FindViewById<TextView>(Resource.Id.User);
            Content = itemView.FindViewById<TextView>(Resource.Id.comment);
            Date = itemView.FindViewById<TextView>(Resource.Id.date);
            AddVote = itemView.FindViewById<ImageButton>(Resource.Id.addLike);
            Votes = itemView.FindViewById<TextView>(Resource.Id.likes);
            Avatar = itemView.FindViewById<CircleImageView>(Resource.Id.avatar);

            itemView.Click += (sender, e) => clickListener(new RecyclerClickEventArgs { View = itemView, Position = AdapterPosition });
            itemView.LongClick += (sender, e) => longClickListener(new RecyclerClickEventArgs { View = itemView, Position = AdapterPosition });
        }
    }
}