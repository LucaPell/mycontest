﻿using Android.Content;
using Android.OS;
using Android.Views;

using DynamoDBTest.ViewModels;
using DynamoDBTest.Models;
using DynamoDBTest.Helpers;
using DynamoDBTest.Services;

using System;

namespace DynamoDBTest.Droid
{
    public class GlobalContestFragment : AbstractRefresherFragment<Contest>
    {
        public static GlobalContestFragment NewInstance() =>
            new GlobalContestFragment { Arguments = new Bundle() };

        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            ContestViewModel viewModel = new ContestViewModel();
            base.ViewModel = viewModel;
            base.LoadItemsCommand = viewModel.RetryLoadItemsCommand;
            base.adapter = new ContestsAdapter(Activity, viewModel);
            base.adapter.fragment = this;
            return base.OnCreateView(inflater, container, savedInstanceState);
        }

        public override void Adapter_ItemClick(object sender, RecyclerClickEventArgs e)
        {
            var item = ViewModel.Items[e.Position];
            Intent intent = new Intent(Activity, typeof(ContestDetailActivity));
            MediaHelper.GetInstance().ClearBitmaps();
            intent.PutExtra("data", Newtonsoft.Json.JsonConvert.SerializeObject(item));

            Activity.StartActivity(intent);
        }

        public override void OnLoadItemsError(object sender, EventArgs a)
        {
            base.OnLoadItemsError(sender, a);

            CommandCompletedEventArgs args = a as CommandCompletedEventArgs;
            if (args.Status == CommandStates.MaxRetryReached)
            {
            }
        }

        public override void OnActivityResult(int requestCode, int resultCode, Intent data)
        {
            base.OnActivityResult(requestCode, resultCode, data);

            if (requestCode == ContestsAdapter.CONTEST_DETAIL_REQUEST_CODE)
            {
                if (resultCode == 0)
                {
                    Contest contestUpdated = GlobalCollections.GetInstance().GetContest(data.GetStringExtra("contestUpdatedId"));
                    ViewModel.Items.RemoveAt(((ContestsAdapter)adapter).requestFromIndex);
                    ViewModel.Items.Insert(((ContestsAdapter)adapter).requestFromIndex, contestUpdated);
                }
            }

            ((MainActivity)Activity).ProgressB.Animation = ((MainActivity)Activity).outAnimation;
            ((MainActivity)Activity).ProgressB.Visibility = ViewStates.Gone;
        }
    }
}