﻿using System;
using System.Windows.Input;

using Android.App;
using Android.OS;
using Android.Views;
using Android.Widget;
using Android.Support.V7.Widget;
using Android.Support.V4.Widget;

using DynamoDBTest.ViewModels;
using DynamoDBTest.Helpers;
using Android.Animation;
using Android.Content;
using Android.Graphics;
using Android.Views.Animations;
using DynamoDBTest.Droid.Helpers;

namespace DynamoDBTest.Droid
{
    public abstract class AbstractRefresherFragment<T> : Android.Support.V4.App.Fragment, IFragmentVisible
    {
        public ICollectioViewModel<T> ViewModel;
        public RetryCommand LoadItemsCommand;
        public object param; //parametro per il LoadItemsCommand
        public ItemsAdapter<T> adapter;
        public SwipeRefreshLayout refresher;
        public RecyclerView recyclerView;
        LinearLayout progress;
        AlphaAnimation inAnimation;
        AlphaAnimation outAnimation;
        //Animator animator;
        //public ImageView ZoomContainer;
        //LruCache cache = new LruCache((int)(Runtime.GetRuntime().MaxMemory() / 4));

        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            View view = inflater.Inflate(Resource.Layout.fragment_generic_recyclerview, container, false);
            recyclerView = view.FindViewById<RecyclerView>(Resource.Id.recyclerView);
            //ZoomContainer = view.FindViewById<ImageView>(Resource.Id.zoomcontainer);
            var EndScrollListener = new InfiniteScrollingListener(Activity);
            EndScrollListener.EndScrollReached += OnEndScroll;
            recyclerView.AddOnScrollListener(EndScrollListener);

            //adapter.cache = cache;

            recyclerView.HasFixedSize = true;
            recyclerView.SetAdapter(adapter);
            recyclerView.SetItemViewCacheSize(4);
            //recyclerView.ChildViewAttachedToWindow += new EventHandler<RecyclerView.ChildViewAttachedToWindowEventArgs>(OnChildViewAttached);
            //recyclerView.ChildViewDetachedFromWindow += new EventHandler<RecyclerView.ChildViewDetachedFromWindowEventArgs>(OnChildViewDetached);

            refresher = view.FindViewById<SwipeRefreshLayout>(Resource.Id.refresher);
            refresher.SetColorSchemeColors(Resource.Color.accent);
            progress = view.FindViewById<LinearLayout>(Resource.Id.progress);

            inAnimation = new AlphaAnimation(0f, 1f);
            inAnimation.Duration = 200;
            outAnimation = new AlphaAnimation(1f, 0f);
            outAnimation.Duration = 200;

            return view;
        }
        /*
        public void OnChildViewAttached(object sender, RecyclerView.ChildViewAttachedToWindowEventArgs eventArgs)
        {
            ImageView media = eventArgs.View.FindViewById<ImageView>(Resource.Id.partecipationMedia);
            if (media != null)
            {
                media.Visibility = ViewStates.Visible;
                Bitmap bmp = (Bitmap)cache.Get(eventArgs.View.Tag);
                if (bmp != null)
                {
                    media.SetImageBitmap(bmp);
                }
            }
        }

        public void OnChildViewDetached(object sender, RecyclerView.ChildViewDetachedFromWindowEventArgs eventArgs)
        {
            ImageView media = eventArgs.View.FindViewById<ImageView>(Resource.Id.partecipationMedia);
            if (media != null)
            {
                cache.Put(eventArgs.View.Tag, ((BitmapDrawable)(media.Drawable)).Bitmap);
                LruCache c = cache;
            }
        }*/

        public override void OnStart()
        {
            base.OnStart();

            refresher.Refresh += Refresher_Refresh;
            adapter.ItemClick += Adapter_ItemClick;

            Refresher_Refresh(this, new EventArgs());
        }

        public override void OnStop()
        {
            base.OnStop();
            refresher.Refresh -= Refresher_Refresh;
            adapter.ItemClick -= Adapter_ItemClick;
        }

        public override void OnDestroy()
        {
            base.OnDestroy();

            ViewModel.Dispose();
        }

        public abstract void Adapter_ItemClick(object sender, RecyclerClickEventArgs e);

        public virtual void OnLoadItemsError(object sender, EventArgs a)
        {
            progress.Animation = outAnimation;
            progress.Visibility = ViewStates.Gone;
        }

        void Refresher_Refresh(object sender, EventArgs e)
        {
            ViewModel.RefreshOnNextCommand = true;
            LoadItemsCommand.OperationCompleted += OnLoadItemsError;
            LoadItemsCommand.Execute(param);
            refresher.Refreshing = false;
        }

        void OnEndScroll(object sender, EventArgs e)
        {
            LoadItemsCommand.OperationCompleted += OnLoadItemsError;
            LoadItemsCommand.Execute(param);
            progress.Animation = inAnimation;
            progress.Visibility = ViewStates.Visible;
        }

        public void BecameVisible()
        {

        }


        public void ImageZoom(View imageToZoom, string url)
        {
            //ZoomContainer.Visibility = ViewStates.Visible;
            //// If there's an animation in progress, cancel it
            //// immediately and proceed with this one.
            //if (animator != null)
            //{
            //    animator.Cancel();
            //}

            //// Load the high-resolution "zoomed-in" image.

            var intent = new Intent(Activity, typeof(ZoomActivity));
            if (url != null)
            {
                intent.PutExtra("Image", url);
            }

            // animation time.
            //int mShortAnimationDuration = 200;

            //// Calculate the starting and ending bounds for the zoomed-in image.
            //Rect startBounds = new Rect();
            //Rect finalBounds = new Rect();
            //Point globalOffset = new Point();

            //imageToZoom.GetGlobalVisibleRect(startBounds);
            //ZoomContainer.GetGlobalVisibleRect(finalBounds, globalOffset);
            //startBounds.Offset(-globalOffset.X, -globalOffset.Y);
            //finalBounds.Offset(-globalOffset.X, -globalOffset.Y);

            //// Adjust the start bounds to be the same aspect ratio as the final
            //// bounds using the "center crop" technique. This prevents undesirable
            //// stretching during the animation. Also calculate the start scaling
            //// factor (the end scaling factor is always 1.0).
            //float startScale;
            //if ((float)finalBounds.Width() / finalBounds.Height()
            //        > (float)startBounds.Width() / startBounds.Height())
            //{
            //    // Extend start bounds horizontally
            //    startScale = (float)startBounds.Height() / finalBounds.Height();
            //    float startWidth = startScale * finalBounds.Width();
            //    float deltaWidth = (startWidth - startBounds.Width()) / 2;
            //    startBounds.Left -= (int)deltaWidth;
            //    startBounds.Right += (int)deltaWidth;
            //}
            //else
            //{
            //    // Extend start bounds vertically
            //    startScale = (float)startBounds.Width() / finalBounds.Width();
            //    float startHeight = startScale * finalBounds.Height();
            //    float deltaHeight = (startHeight - startBounds.Height()) / 2;
            //    startBounds.Top -= (int)deltaHeight;
            //    startBounds.Bottom += (int)deltaHeight;
            //}

            //// Hide the thumbnail and show the zoomed-in view. When the animation
            //// begins, it will position the zoomed-in view in the place of the
            //// thumbnail.
            ////view.Alpha = 0f;
            Activity.StartActivity(intent);
            Activity.OverridePendingTransition(Resource.Animation.abc_fade_in, Resource.Animator.Still);

            //    // Set the pivot point for SCALE_X and SCALE_Y transformations
            //    // to the top-left corner of the zoomed-in view (the default
            //    // is the center of the view).
            //    ZoomContainer.PivotX = 0f;
            //    ZoomContainer.PivotY = 0f;

            //    // Construct and run the parallel animation of the four translation and
            //    // scale properties (X, Y, SCALE_X, and SCALE_Y).
            //    AnimatorSet set = new AnimatorSet();

            //    set
            //   .Play(ObjectAnimator.OfFloat(ZoomContainer, View.X,
            //           startBounds.Left, finalBounds.Left))
            //   .With(ObjectAnimator.OfFloat(ZoomContainer, View.Y,
            //           startBounds.Top, finalBounds.Top))
            //   .With(ObjectAnimator.OfFloat(ZoomContainer, View.ScaleXs,
            //           startScale, 1f))
            //   .With(ObjectAnimator.OfFloat(ZoomContainer,
            //           View.ScaleYs, startScale, 1f));

            //    set.SetDuration(mShortAnimationDuration);
            //    set.SetInterpolator(new DecelerateInterpolator());

            //    set.AnimationEnd += (object sender, EventArgs e) =>
            //    {
            //        animator = null;
            //    };
            //    set.AnimationCancel += (object sender, EventArgs e) =>
            //    {
            //        animator = null;
            //    };
            //    set.Start();
            //    animator = set;

            //    // Upon clicking the zoomed-in image, it should zoom back down
            //    // to the original bounds and show the thumbnail instead of
            //    // the expanded image. 

            //    float startScaleFinal = startScale;
            //    //ZoomedImage.Click += (object sender, EventArgs e) =>
            //    //{
            //    //    if (animator != null)
            //    //    {
            //    //        animator.Cancel();
            //    //    }


            //    //    // Animate the four positioning/sizing properties in parallel,
            //    //    // back to their original values.

            //    //    set = new AnimatorSet();

            //    //    set.Play(ObjectAnimator
            //    //            .OfFloat(ZoomedImage, View.X, startBounds.Left))
            //    //            .With(ObjectAnimator
            //    //                    .OfFloat(ZoomedImage,
            //    //                            View.Y, startBounds.Top))
            //    //            .With(ObjectAnimator
            //    //                    .OfFloat(ZoomedImage,
            //    //                            View.ScaleXs, startScaleFinal))
            //    //            .With(ObjectAnimator
            //    //                    .OfFloat(ZoomedImage,
            //    //                            View.ScaleYs, startScaleFinal));
            //    //    set.SetDuration(mShortAnimationDuration);
            //    //    set.SetInterpolator(new DecelerateInterpolator());

            //    //    set.AnimationEnd += (object sender2, EventArgs e2) =>
            //    //    {
            //    //        //view.Alpha = 1f;
            //    //        ZoomContainer.Visibility = ViewStates.Gone;
            //    //        //attacher.Update();
            //    //        animator = null;
            //    //    };
            //    //    set.AnimationCancel += (object sender3, EventArgs e3) =>
            //    //    {
            //    //        //view.Alpha = 1f;
            //    //        ZoomContainer.Visibility = ViewStates.Gone;
            //    //        //attacher.Update();
            //    //        animator = null;
            //    //    };
            //    //    set.Start();
            //    //    animator = set;
            //    //};
            
        }

    }

    public abstract class ItemsAdapter<T> : BaseRecycleViewAdapter
    {
        public ICollectioViewModel<T> viewModel;
        public Activity activity;
        public AbstractRefresherFragment<T> fragment;
        //public LruCache cache;

        public ItemsAdapter(Activity activity, ICollectioViewModel<T> viewModel)
        {
            this.viewModel = viewModel;
            this.activity = activity;

            this.viewModel.Items.CollectionChanged += (sender, args) =>
            {
                this.activity.RunOnUiThread(NotifyDataSetChanged);
            };
        }

        public abstract override RecyclerView.ViewHolder OnCreateViewHolder(ViewGroup parent, int viewType);

        public abstract override void OnBindViewHolder(RecyclerView.ViewHolder holder, int position);

        public override int ItemCount => viewModel.Items.Count;
    }
}