﻿using Android.OS;
using Android.Views;
using Android.Content;

using DynamoDBTest.ViewModels;
using DynamoDBTest.Models;
using DynamoDBTest.Helpers;
using DynamoDBTest.Services;

using System;
using System.Collections.Generic;

namespace DynamoDBTest.Droid
{
    public class ContestPartecipationsFragment : AbstractRefresherFragment<Partecipation>
    {
        public static ContestPartecipationsFragment NewInstance() =>
            new ContestPartecipationsFragment { Arguments = new Bundle() };

        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            PartecipationViewModel viewModel = new PartecipationViewModel();
            base.ViewModel = viewModel;
            base.LoadItemsCommand = viewModel.RetryLoadItemsByContestCommand;
            base.param = Activity.Intent.GetStringExtra("itemId");
            base.adapter = new PartecipationsAdapter(Activity, viewModel);

            if(Activity.GetType().Equals(typeof(ContestDetailActivity)))
            {
                var activity = Activity as ContestDetailActivity;
                activity.fragmentPartecipationViewModel = viewModel;

            }

            base.adapter.fragment = this;
            var View = base.OnCreateView(inflater, container, savedInstanceState);
            var listener = new CollapsingPlusRecyclerListener(Activity);
            recyclerView.AddOnScrollListener(listener);
            return View;
        }

        public override void Adapter_ItemClick(object sender, RecyclerClickEventArgs e)
        {
            
        }

        public override void OnLoadItemsError(object sender, EventArgs a)
        {
            base.OnLoadItemsError(sender, a);

            CommandCompletedEventArgs args = a as CommandCompletedEventArgs;
            if (args.Status == CommandStates.MaxRetryReached)
            {
            }
        }

        public override void OnActivityResult(int requestCode, int resultCode, Intent data)
        {
            base.OnActivityResult(requestCode, resultCode, data);

            if (requestCode == PartecipationsAdapter.PARTECIPATE_REQUEST_CODE)
            {
                if (resultCode == 0)
                {
                    Partecipation newPartecipation = GlobalCollections.GetInstance().GetPartecipation(data.GetStringExtra("newPartecipationId"));
                    ((PartecipationViewModel)base.ViewModel).PutItem(newPartecipation);
                    AuthenticationLoopCheck.GetInstance().DoOne();
                }
            }

            ((ContestDetailActivity)Activity).ProgressB.Animation = ((ContestDetailActivity)Activity).outAnimation;
            ((ContestDetailActivity)Activity).ProgressB.Visibility = ViewStates.Gone;
        }
    }
}