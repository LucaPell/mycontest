﻿using System;
using System.Collections.Generic;

using Android.App;
using Android.OS;
using Android.Views;
using Android.Widget;
using Android.Content;
using Android.Support.V7.Widget;

using DynamoDBTest.ViewModels;
using DynamoDBTest.Models;
using DynamoDBTest.Controllers;
using DynamoDBTest.Helpers;
using Com.Bumptech.Glide;
using Android.Util;

namespace DynamoDBTest.Droid
{
    class CommentsFragment : AbstractRefresherFragment<Comment>
    {
        public static CommentsFragment NewInstance() =>
            new CommentsFragment { Arguments = new Bundle() };

        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            CommentViewModel viewModel = new CommentViewModel();
            base.ViewModel = viewModel;
            base.LoadItemsCommand = viewModel.RetryLoadItemsCommand;
            string itemId = Activity.Intent.GetStringExtra("itemId");
            string itemType = Activity.Intent.GetStringExtra("type");
            base.param = itemId;
            base.adapter = new CommentAdapter(Activity, viewModel);
            View view;
            View recyclerFragment = base.OnCreateView(inflater, container, savedInstanceState);
         

            if (!Activity.GetType().Equals(typeof(ContestDetailActivity)))
            {
                view = inflater.Inflate(Resource.Layout.fragment_comments, container, false);
                ((RelativeLayout)view).AddView(recyclerFragment, 0);
            }
            else
            {
                view = inflater.Inflate(Resource.Layout.fragment_contest_comments, container, false);
                ((LinearLayout)view).AddView(recyclerFragment, 1);
                var listener = new CollapsingPlusRecyclerListener(Activity);
                recyclerView.AddOnScrollListener(listener);
            }


            AppCompatImageView AddComment = view.FindViewById<AppCompatImageView>(Resource.Id.addComment);
            EditText Content = view.FindViewById<EditText>(Resource.Id.commentForm);
            AddComment.Visibility = ViewStates.Visible;
            Content.Visibility = ViewStates.Visible;
            AddComment.Click += (object sender, EventArgs e) =>
            {
                AddComment.Enabled = false;
                if (!String.IsNullOrWhiteSpace(Content.Text))
                {
                    Comment newComment = new Comment(Content.Text, LoginController.GetInstance().CurrentUser, itemId, itemType);
                    viewModel.RetryAddCommentCommand.OperationCompleted += OnLoadItemsError;
                    viewModel.RetryAddCommentCommand.Execute(newComment);
                    Content.Text = null;
                }
                AddComment.Enabled = true;
            };

            return view;
        }

        public override void Adapter_ItemClick(object sender, RecyclerClickEventArgs e)
        {

        }

        public override void OnLoadItemsError(object sender, EventArgs a)
        {
            base.OnLoadItemsError(sender, a);

            CommandCompletedEventArgs args = a as CommandCompletedEventArgs;
            if (args.Status == CommandStates.MaxRetryReached)
            {
            }
        }
    }

    class CommentAdapter : ItemsAdapter<Comment>
    {
        public CommentAdapter(Activity activity, ICollectioViewModel<Comment> viewModel) : base(activity, viewModel)
        {

        }

        public override RecyclerView.ViewHolder OnCreateViewHolder(ViewGroup parent, int viewType)
        {
            View itemView = null;
            var id = Resource.Layout.comment_browse2;
            itemView = LayoutInflater.From(parent.Context).Inflate(id, parent, false);

            var vh = new CommentViewHolder(itemView, OnClick, OnLongClick);
            return vh;
        }

        public override void OnBindViewHolder(RecyclerView.ViewHolder holder, int position)
        {
            var item = base.viewModel.Items[position];
            var myHolder = holder as CommentViewHolder;
            User user = LoginController.GetInstance().CurrentUser;
            myHolder.Username.Text = item.CreatorUserName;
            myHolder.Content.Text = item.Content;
            myHolder.Votes.Text = item.Votes.Count.ToString();
            myHolder.Date.Text = PrettyDate.GetPrettyDate(item.SortingDate);
            myHolder.OnUserClick = (object sender, EventArgs e) =>
            {
                    if (item.CreatorUser == user.Email)
                    {
                        var intent = new Intent(activity, typeof(PersonalUserPageActivity));
                        MediaHelper.GetInstance().ClearBitmaps();
                        activity.StartActivity(intent);
                    }
                    else{
                        var intent = new Intent(activity, typeof(UserPageActivity));
                        intent.PutExtra("userId", item.CreatorUser);
                        intent.PutExtra("username", item.CreatorUserName);
                        intent.PutExtra("avatar", item.Avatar);
                        activity.StartActivity(intent);
                    }
            };
                myHolder.Username.Click += myHolder.OnUserClick;
                myHolder.Avatar.Click += myHolder.OnUserClick;

            if (item.Votes.Contains(LoginController.GetInstance().CurrentUser.Email))
            {
                myHolder.AddVote.Activated = true;
            }

            if(item.Avatar != null)
            {
                Glide.With(activity).Load(item.Avatar).Into(myHolder.Avatar);
            }
            else
            {
                myHolder.Avatar.SetImageResource(Resource.Color.imageBlank);
            }

            myHolder.OnAddLikeClick = (object sender, EventArgs e) =>
            {
                myHolder.AddVote.Enabled = false;
                if (!myHolder.AddVote.Activated)
                {
                    ((CommentViewModel)viewModel).RetryAddLikeCommand.Execute(position);

                    if (item.Votes.Contains(user.Email))
                    {
                        int newcount = item.Votes.Count;
                        Int32.TryParse(myHolder.Votes.Text, out newcount);
                        myHolder.Votes.Text = (newcount + 1).ToString();
                        myHolder.AddVote.Activated = true;
                    }

                }
                else
                {
                    ((CommentViewModel)viewModel).RetryRemoveLikeCommand.Execute(position);

                    if (!item.Votes.Contains(user.Email))
                    {
                        int newcount = item.Votes.Count;
                        Int32.TryParse(myHolder.Votes.Text, out newcount);
                        myHolder.Votes.Text = (newcount - 1).ToString();
                        myHolder.AddVote.Activated = false;
                    }
                }
                myHolder.AddVote.Enabled = true;
            };
            myHolder.AddVote.Click += myHolder.OnAddLikeClick;
        }

        public override void OnViewRecycled(Java.Lang.Object holder)
        {
            base.OnViewRecycled(holder);
            CommentViewHolder viewHolder = holder as CommentViewHolder;
            viewHolder.Username.Click -= viewHolder.OnUserClick;
            viewHolder.Avatar.Click -= viewHolder.OnUserClick;
            viewHolder.AddVote.Click -= viewHolder.OnAddLikeClick;
            viewHolder.AddVote.Activated = false;

        }
    }

}