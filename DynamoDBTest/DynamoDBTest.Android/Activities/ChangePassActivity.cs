﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Views.Animations;
using Android.Views.InputMethods;
using Android.Widget;
using DynamoDBTest.Controllers;
using DynamoDBTest.Helpers;
using DynamoDBTest.Models;
using DynamoDBTest.Services;

namespace DynamoDBTest.Droid
{
    [Activity(Label = "POIApplication")]
    public class ChangePassActivity : BaseActivity
    {
        TextView confirm;
        protected override int LayoutResource => Resource.Layout.activity_change_pass;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            confirm = FindViewById<TextView>(Resource.Id.confirm);
            EditText pass = FindViewById<EditText>(Resource.Id.changePassword);
            EditText confirmPass = FindViewById<EditText>(Resource.Id.changePasswordConfirm);

            confirm.Click += (object sender, EventArgs e) =>
            {
                User u = LoginController.GetInstance().CurrentUser;
                String email = u.Email;
                confirm.Enabled = false;
                Validator validator = new Validator(email, pass.Text, confirmPass.Text);
                if (validator.ValidatePassReset())
                {
                    StringWrapper passAndEmail = new StringWrapper(email, pass.Text);
                    ResetPassController controller = new ResetPassController();
                    controller.CheckTerminated += new ResetPassController.CheckTerminatedHandler(OnCheckTerminated);
                    controller.ChangePasswordCommand.Execute(passAndEmail);
                }
                else
                {
                    foreach (string message in validator.ErrorMessages)
                    {
                        Android.Widget.Toast.MakeText(this, message, Android.Widget.ToastLength.Short).Show();
                    }
                    confirm.Enabled = true;
                }
            };
        }

        private void OnCheckTerminated(ResetPassController controller, bool checkSuccessful, string message, string email)
        {
            Android.Widget.Toast.MakeText(this, message, Android.Widget.ToastLength.Short).Show();
            if (checkSuccessful)
            {
                var newIntent = new Intent(this, typeof(MainActivity));
                confirm.Enabled = true;
                StartActivity(newIntent);
                this.Finish();
            }
            else
            {
                confirm.Enabled = true;
            }
        }



        public override bool OnKeyDown(Keycode keycode, KeyEvent e)
        {
            if (keycode == Keycode.Back)
            {
                Finish();
                OverridePendingTransition(Resource.Animator.Still, Resource.Animator.ExitTopToBottom);
                return true;
            }
            return base.OnKeyDown(keycode, e);
        }

        public override void OnBackPressed()
        {
            Finish();
            OverridePendingTransition(Resource.Animator.Still, Resource.Animator.ExitTopToBottom);
        }

        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            switch (item.ItemId)
            {
                case Android.Resource.Id.Home:
                    View view = CurrentFocus;
                    if (view != null)
                    {
                        InputMethodManager imm = (InputMethodManager)GetSystemService(InputMethodService);
                        imm.HideSoftInputFromWindow(view.WindowToken, 0);
                    }
                    Finish();
                    OverridePendingTransition(Resource.Animator.Still, Resource.Animator.ExitTopToBottom);
                    return true;

                default:
                    return base.OnOptionsItemSelected(item);
            }
        }
    }
}