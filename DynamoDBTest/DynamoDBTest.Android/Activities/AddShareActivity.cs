﻿using System;
using System.Threading.Tasks;
using Android.App;
using Android.Content;
using Android.Graphics;
using Android.OS;
using Android.Views;
using Android.Views.Animations;
using Android.Widget;
using DynamoDBTest.Controllers;
using DynamoDBTest.Helpers;
using DynamoDBTest.Models;
using DynamoDBTest.Services;
using DynamoDBTest.ViewModels;

namespace DynamoDBTest.Droid
{
    [Activity(Label = "AddShareActivity")]
    public class AddShareActivity : BaseActivity
    {
        EditText comment;
        String pdata;
        String cdata;
        String sdata;
        Partecipation partecipation;
        Share oldshare;
        Contest contest;
        TextView confirm;
        FrameLayout ProgressB;
        AlphaAnimation inAnimation;
        AlphaAnimation outAnimation;
        Share newShare;

        PartecipationDetailViewModel pViewModel;
        ContestDetailViewModel cViewModel;
        ShareDetailViewModel sViewModel;

        protected override int LayoutResource => Resource.Layout.new_share_activity;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            comment = FindViewById<EditText>(Resource.Id.shareText);
            SupportActionBar.SetDisplayHomeAsUpEnabled(true);
            SupportActionBar.SetHomeButtonEnabled(true);
            pdata = Intent.GetStringExtra("partecipation");
            cdata = Intent.GetStringExtra("contest");
            sdata = Intent.GetStringExtra("share");

            ProgressB = FindViewById<FrameLayout>(Resource.Id.progress);
            ProgressB.Visibility = ViewStates.Gone;
            inAnimation = new AlphaAnimation(0f, 1f);
            inAnimation.Duration = 200;
            outAnimation = new AlphaAnimation(1f, 0f);
            outAnimation.Duration = 200;


            //carico post da condividere
            if (cdata != null) //ho un contest
            {
                contest = GlobalCollections.GetInstance().GetContest(cdata);

                if (contest == null)
                {
                    cViewModel = new ContestDetailViewModel();
                    cViewModel.RetryLoadItemCommand.Execute(cdata);
                    contest = cViewModel.contest;
                }
            }
            else if (pdata != null) //ho una partecipazione
            {
                partecipation = GlobalCollections.GetInstance().GetPartecipation(pdata);
          
                if (partecipation == null)
                {
                    pViewModel = new PartecipationDetailViewModel();
                    pViewModel.RetryLoadItemCommand.Execute(pdata);
                    partecipation = pViewModel.partecipation;
                }

            }
            else if (sdata != null) //sto condividendo una condivisione
            {
                oldshare = GlobalCollections.GetInstance().GetShare(sdata);
                if (oldshare == null)
                {
                    sViewModel = new ShareDetailViewModel();
                    sViewModel.RetryLoadItemCommand.Execute(sdata);
                    oldshare = sViewModel.share;
                }
            }
            else //ho un post normale
            {
                //TO DO
            }



            confirm = FindViewById<TextView>(Resource.Id.confirm);
            confirm.Click += (object sender, EventArgs e) =>
            {
                confirm.Enabled = false;
                //costruisco e salvo lo share
                if (cdata != null) 
                {
                    newShare = new Share(comment.Text, LoginController.GetInstance().CurrentUser, contest);
                }
                else if (pdata != null)
                {
                    newShare = new Share(comment.Text, LoginController.GetInstance().CurrentUser, partecipation);
                }
                else if (sdata != null)
                {
                    newShare = new Share(comment.Text, LoginController.GetInstance().CurrentUser, oldshare);
                }
                else 
                {
                    Toast.MakeText(this, "Qualcosa è andato storto", ToastLength.Short).Show();
                    this.Finish();
                }

                try
                {
                    ProgressB.Animation = inAnimation;
                    ProgressB.Visibility = ViewStates.Visible;
                    ShareViewModel viewModel = new ShareViewModel();
                    viewModel.RetryAddShareCommand.OperationCompleted += OnAddShareCompleted;
                    viewModel.RetryAddShareCommand.Execute(newShare);
                }
                catch (Exception ex)
                {
                    Toast.MakeText(this, ex.Message, ToastLength.Long).Show();

                }

            };
        }

        private void OnAddShareCompleted(object sender, EventArgs args)
        {
            RunOnUiThread(() => {
                ProgressB.Animation = outAnimation;
                ProgressB.Visibility = ViewStates.Gone;

                CommandCompletedEventArgs commandArgs = args as CommandCompletedEventArgs;
                if (commandArgs.Status == CommandStates.Completed)
                {
                    //aggiorno numero di shares
                    if (cdata != null) //ho un contest
                    {
                        contest = GlobalCollections.GetInstance().GetContest(cdata);
                        contest.Shares++;
                    }
                    else if (pdata != null) //ho una partecipazione
                    {
                        partecipation = GlobalCollections.GetInstance().GetPartecipation(pdata);
                        partecipation.Shares++;
                    }
                    else //ho un post normale
                    {
                        //TO DO
                    }
                    Finish();
                }
                else
                {
                    Toast.MakeText(this, "Non c'è connessione, riprova", Android.Widget.ToastLength.Short).Show();
                }
            });
        }

        public override bool OnKeyDown(Keycode keycode, KeyEvent e)
        {
            if (keycode == Keycode.Back)
            {
                this.Finish();
                return true;
            }
            return base.OnKeyDown(keycode, e);
        }
    }
}