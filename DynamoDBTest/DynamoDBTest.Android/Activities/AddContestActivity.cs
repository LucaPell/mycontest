﻿using System;
using System.IO;
using System.Threading.Tasks;
using System.Collections.Generic;

using Android;
using Android.Runtime;
using Android.Content;
using Android.Content.PM;
using Android.App;
using Android.OS;
using Android.Views;
using Android.Widget;
using Android.Views.Animations;
using Android.Graphics;

using DynamoDBTest.ViewModels;
using DynamoDBTest.Models;
using DynamoDBTest.Helpers;
using DynamoDBTest.Controllers;

using Refractored.Controls;
using Com.Theartofdev.Edmodo.Cropper;
using Uri = Android.Net.Uri;

namespace DynamoDBTest.Droid
{
    [Activity(Label = "AddContestActivity")]
    public class AddContestActivity : BaseActivity
    {
        public ContestViewModel ViewModel = new ContestViewModel();

        private const int IMAGE_RESULT_CODE = 5514;
        private Bitmap image;
        private MemoryStream memoryStream;
        protected override int LayoutResource => Resource.Layout.activity_new_contest;
        int daysPosition;
        int hoursPosition;
        Spinner Days;
        Spinner Hours;
        EditText title;
        EditText Description;
        TextView Confirm;
        CircleImageView AddImage;
        private Uri _cropImageUri;
        ImageView preview;
        FrameLayout ProgressB;
        CircleImageView Delete;
        AlphaAnimation inAnimation;
        AlphaAnimation outAnimation;
        TextView Giorni;
        TextView Ore;
        View Separator;
        private int previewHeight;
        private int screenHeight;
        private int screenWidth;
        private int newPreviewHeight;
        int[] days;
        int[] hours;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            preview = FindViewById<ImageView>(Resource.Id.preview);
            Days = FindViewById<Spinner>(Resource.Id.days);
            Hours = FindViewById<Spinner>(Resource.Id.hours);
            title = FindViewById<EditText>(Resource.Id.titleText);
            Description = FindViewById<EditText>(Resource.Id.descText);
            Confirm = FindViewById<TextView>(Resource.Id.confirm);
            AddImage = FindViewById<CircleImageView>(Resource.Id.add);
            Delete = FindViewById<CircleImageView>(Resource.Id.delete);
            ProgressB = FindViewById<FrameLayout>(Resource.Id.progress);
            Separator = FindViewById<View>(Resource.Id.imgsep);
            Giorni = FindViewById<TextView>(Resource.Id.giorni);
            Ore = FindViewById<TextView>(Resource.Id.ore);

            ProgressB.Visibility = ViewStates.Gone;
            inAnimation = new AlphaAnimation(0f, 1f);
            inAnimation.Duration = 200;
            outAnimation = new AlphaAnimation(1f, 0f);
            outAnimation.Duration = 200;

            days = new int[]{ 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30 };
            ArrayAdapter adapterDays = new ArrayAdapter<int>(this, Android.Resource.Layout.SimpleSpinnerItem, days);
            hours = new int[]{ 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23 };
            ArrayAdapter adapterHours = new ArrayAdapter<int>(this, Android.Resource.Layout.SimpleSpinnerItem, hours);

            Days.ItemSelected += new EventHandler<AdapterView.ItemSelectedEventArgs>(SpinnerDaysItemSelected);
            Hours.ItemSelected += new EventHandler<AdapterView.ItemSelectedEventArgs>(SpinnerHoursItemSelected);
            Days.Adapter = adapterDays;
            Hours.Adapter = adapterHours;

            Confirm.Click += (object sender, EventArgs e) => {
                Confirm.Enabled = false;
                    if (!(daysPosition == 0 && hoursPosition == 0) && title.Text != null && !String.IsNullOrWhiteSpace(title.Text) && memoryStream!=null)
                    {
                        ProgressB.Animation = inAnimation;
                        ProgressB.Visibility = ViewStates.Visible;
                        DateTime endDate = DateTime.Now;
                        endDate = endDate.AddDays(days[daysPosition]);
                        endDate = endDate.AddHours(hours[hoursPosition]);
                        if (String.IsNullOrWhiteSpace(Description.Text))
                        {
                            Description.Text = null;
                        }
                        Contest newContest = new Contest(title.Text, endDate, Description.Text, LoginController.GetInstance().CurrentUser);
                        newContest.TempBanner = memoryStream;
                        ViewModel.RetryAddContestCommand.OperationCompleted += OnAddContestCompleted;
                        ViewModel.RetryAddContestCommand.Execute(newContest);
                        AuthenticationLoopCheck.GetInstance().DoOne();
                    }
                    else
                    {
                        if ((daysPosition == 0 && hoursPosition == 0))
                        {
                            Toast.MakeText(this, "Il contest deve durare almeno un'ora", Android.Widget.ToastLength.Short).Show();
                        }
                        else if(memoryStream == null)
                        {
                        Toast.MakeText(this, "L'immagine è obbligatoria", Android.Widget.ToastLength.Short).Show();
                        }
                        else
                        {
                            Toast.MakeText(this, "Il Contest deve avere un titolo", Android.Widget.ToastLength.Short).Show();
                        }
                    }
                Confirm.Enabled = true;
            };

            AddImage.Click += (object sender, EventArgs e) => {
                if (CropImage.IsExplicitCameraPermissionRequired(this))
                {
                    RequestPermissions(new[] { Manifest.Permission.Camera }, CropImage.CameraCapturePermissionsRequestCode);
                }
                else
                {
                    CropImage.StartPickImageActivity(this);
                }

            };

            preview.Click += (object sender, EventArgs e) => {
                if (CropImage.IsExplicitCameraPermissionRequired(this))
                {
                    RequestPermissions(new[] { Manifest.Permission.Camera }, CropImage.CameraCapturePermissionsRequestCode);
                }
                else
                {
                    CropImage.StartPickImageActivity(this);
                }

            };

            Delete.Click += (object sender, EventArgs e) =>
            {
                preview.SetScaleType(ImageView.ScaleType.CenterInside);
                preview.SetImageResource(Resource.Drawable.addpic);
                Delete.Visibility = ViewStates.Gone;
                AddImage.Visibility = ViewStates.Gone;
                Separator.Visibility = ViewStates.Visible;
                MediaHelper.GetInstance().SetBanner(null);

            };

            if (Resources.Configuration.Orientation == Android.Content.Res.Orientation.Landscape)
            {
                preview.Post(() =>
                {
                    previewHeight = preview.Height;
                    screenHeight = MediaHelper.GetInstance().ScreenHeight();
                    screenWidth = MediaHelper.GetInstance().ScreenWidth();
                    newPreviewHeight = screenHeight * previewHeight / screenWidth;
                    preview.LayoutParameters.Height = newPreviewHeight;
                });
            }
        }

        private void OnAddContestCompleted(object sender, EventArgs args)
        {
            RunOnUiThread(() => {
                ProgressB.Animation = outAnimation;
                ProgressB.Visibility = ViewStates.Gone;

                CommandCompletedEventArgs commandArgs = args as CommandCompletedEventArgs;
                if (commandArgs.Status == CommandStates.Completed)
                {
                    Finish();
                }
                else
                {
                    Toast.MakeText(this, "Non c'è connessione, riprova", Android.Widget.ToastLength.Short).Show();
                }
            });
        }

        private void SpinnerDaysItemSelected(object sender, AdapterView.ItemSelectedEventArgs e)
        {
            Spinner spinner = (Spinner)sender;
            daysPosition = (int)spinner.GetItemAtPosition(e.Position);
            Giorni.Text = "Giorni: " + days[daysPosition].ToString();
        }

        private void SpinnerHoursItemSelected(object sender, AdapterView.ItemSelectedEventArgs e)
        {
            Spinner spinner = (Spinner)sender;
            hoursPosition = (int)spinner.GetItemAtPosition(e.Position);
            Ore.Text = "Ore: " + hours[hoursPosition].ToString();
        }

        public override bool OnKeyDown(Keycode keycode, KeyEvent e)
        {
            if (keycode == Keycode.Back)
            {
                this.Finish();
                return true;
            }
            return base.OnKeyDown(keycode, e);
        }

        protected override void OnActivityResult(int requestCode, [GeneratedEnum] Result resultCode, Intent data)
        {
            base.OnActivityResult(requestCode, resultCode, data);

            if (requestCode == CropImage.PickImageChooserRequestCode && resultCode == Result.Ok)
            {
                var imageUri = CropImage.GetPickImageResultUri(this, data);

                var requirePermissions = false;
                if (CropImage.IsReadExternalStoragePermissionsRequired(this, imageUri))
                {
                    requirePermissions = true;
                    _cropImageUri = imageUri;
                    RequestPermissions(new[] { Manifest.Permission.ReadExternalStorage }, CropImage.PickImagePermissionsRequestCode);
                }
                else
                {
                    Intent intent = new Intent(this, typeof(CropActivity));
                    intent.PutExtra("URI", imageUri.ToString());
                    intent.PutExtra("SHAPE", "CONTEST");
                    intent.PutExtra("WIDTH", preview.Width);
                    intent.PutExtra("HEIGHT", preview.Height);
                    StartActivityForResult(intent, IMAGE_RESULT_CODE);
                }
            }else if(requestCode == IMAGE_RESULT_CODE && resultCode == Result.Ok) {
                if (image == null)
                {
                    if (MediaHelper.GetInstance().HasBanner())
                    {
                        image = MediaHelper.GetInstance().GetBanner();
                    }
                }
                memoryStream = new MemoryStream(); 
                image.Compress(Bitmap.CompressFormat.Jpeg, 90, memoryStream);
            }
        }

        public override void OnRequestPermissionsResult(int requestCode, string[] permissions, [GeneratedEnum] Permission[] grantResults)
        {
            if (requestCode == CropImage.CameraCapturePermissionsRequestCode)
            {
                if (grantResults.Length > 0 && grantResults[0] == Permission.Granted)
                {
                    CropImage.StartPickImageActivity(this);
                }
                else
                {
                    Toast.MakeText(this, "Cancelling, required permissions are not granted", ToastLength.Long).Show();
                }
            }
            else
            if (requestCode == CropImage.PickImagePermissionsRequestCode)
            {
                if (_cropImageUri != null && grantResults.Length > 0 && grantResults[0] == Permission.Granted)
                {
                    Intent intent = new Intent(this, typeof(CropActivity));
                    intent.PutExtra("URI", _cropImageUri.ToString());
                    intent.PutExtra("SHAPE", "CONTEST");
                    intent.PutExtra("WIDTH", preview.Width);
                    intent.PutExtra("HEIGHT", preview.Height);
                    StartActivityForResult(intent, IMAGE_RESULT_CODE);
                }
                else
                {
                    Toast.MakeText(this, "Cancelling, required permissions are not granted", ToastLength.Long).Show();
                }
            }
        }

        protected override void OnResume()
        {
            if (MediaHelper.GetInstance().HasBanner())
            {
                image = MediaHelper.GetInstance().GetBanner();
            }
            if (image != null){
                preview.SetScaleType(ImageView.ScaleType.CenterCrop);
                preview.SetImageBitmap(image);
                AddImage.Visibility = ViewStates.Visible;
                Delete.Visibility = ViewStates.Visible;
                Separator.Visibility = ViewStates.Gone;
            }
            base.OnResume();
        }
    }
}