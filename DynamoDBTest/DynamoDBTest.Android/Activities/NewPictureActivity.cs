﻿using System;
using System.IO;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Provider;
using Android.Runtime;
using Android.Widget;
using Com.Theartofdev.Edmodo.Cropper;
using DynamoDBTest.Services;
using Refractored.Controls;
using Android.Graphics;
using Android.Content.PM;
using Android;
using Uri = Android.Net.Uri;
using Android.Views;
using Android.Support.Design.Widget;
using Android.Views.Animations;
using DynamoDBTest.ViewModels;
using DynamoDBTest.Helpers;

namespace DynamoDBTest.Droid
{
    [Activity(Label = "NewAvatarActivity")]
    public class NewPictureActivity : BaseActivity
    {
        const int AvatarHeight = 256;
        const int AvatarWidth = 256;
        const int BannerMaxHeight = 528;
        int BannerMaxWidth;
        private Uri _cropImageUri;
        Button confirm;
        Button modify;
        CircleImageView another;
        CircleImageView another2;
        CircleImageView another3;
        CircleImageView circle;
        ImageView banner;
        ImageView other;
        CoordinatorLayout av;
        CoordinatorLayout ban;
        CoordinatorLayout oth;
        private int bannerHeight;
        private int screenHeight;
        private int screenWidth;
        private int newBannerHeight;
        AlphaAnimation inAnimation;
        AlphaAnimation outAnimation;
        FrameLayout ProgressB;
        private Bitmap bmp;

        protected override int LayoutResource => Resource.Layout.activity_new_picture;

        protected override void OnCreate(Bundle savedInstanceState)
        {

            base.OnCreate(savedInstanceState);
           
            circle = FindViewById<CircleImageView>(Resource.Id.circle);
            banner = FindViewById<ImageView>(Resource.Id.banner);
            other = FindViewById<ImageView>(Resource.Id.other);
            another = FindViewById<CircleImageView>(Resource.Id.another);
            another2 = FindViewById<CircleImageView>(Resource.Id.another2);
            another3 = FindViewById<CircleImageView>(Resource.Id.another3);
            av = FindViewById<CoordinatorLayout>(Resource.Id.av);
            ban = FindViewById<CoordinatorLayout>(Resource.Id.ban);
            oth = FindViewById<CoordinatorLayout>(Resource.Id.oth);
            confirm = FindViewById<Button>(Resource.Id.confirm);
            modify = FindViewById<Button>(Resource.Id.modify);

            inAnimation = new AlphaAnimation(0f, 1f);
            inAnimation.Duration = 200;
            outAnimation = new AlphaAnimation(1f, 0f);
            outAnimation.Duration = 200;
            ProgressB = FindViewById<FrameLayout>(Resource.Id.progress);

            var imageUri = Intent.GetParcelableExtra("RESULTURI").JavaCast<Android.Net.Uri>();
            if (Intent.GetStringExtra("SHAPE").Equals("CIRCLE"))
            {
                av.Visibility = ViewStates.Visible;
                circle.Visibility = ViewStates.Visible;
                another.Visibility = ViewStates.Visible;
                if (imageUri != null)
                {
                    circle.SetImageURI(imageUri);
                }
                else
                {
                    circle.SetImageBitmap(MediaHelper.GetInstance().GetResult());
                }
            }
            else if ((Intent.GetStringExtra("SHAPE").Equals("BANNER")) || Intent.GetStringExtra("SHAPE").Equals("CONTEST"))
            {
                ban.Visibility = ViewStates.Visible;
                banner.Visibility = ViewStates.Visible;
                another2.Visibility = ViewStates.Visible;
                if (imageUri != null)
                {
                    banner.SetImageURI(imageUri);
                }
                else
                {
                    banner.SetImageBitmap(MediaHelper.GetInstance().GetResult());
                
                }

                if (Resources.Configuration.Orientation == Android.Content.Res.Orientation.Landscape)
                {
                    banner.Post(() =>
                    {
                        bannerHeight = banner.Height;
                        screenHeight = MediaHelper.GetInstance().ScreenHeight();
                        screenWidth = MediaHelper.GetInstance().ScreenWidth();
                        newBannerHeight = screenHeight * bannerHeight / screenWidth;
                        banner.LayoutParameters.Height = newBannerHeight;
                    });
                }
            }
            else
            {
                oth.Visibility = ViewStates.Visible;
                other.Visibility = ViewStates.Visible;
                another3.Visibility = ViewStates.Visible;
                if (imageUri != null)
                {
                    other.SetImageURI(imageUri);
                }
                else
                {
                    other.SetImageBitmap(MediaHelper.GetInstance().GetResult());
                }
            }





          
          

            confirm.Click += (object sender, EventArgs e) =>
            {
                Bitmap bitmap;
                confirm.Enabled = false;
                if (imageUri != null)
                {
                    bitmap = MediaStore.Images.Media.GetBitmap(ContentResolver, imageUri);
                }
                else
                {
                    bitmap = MediaHelper.GetInstance().GetResult();
                }

                if (bitmap != null)
                {
                    MemoryStream memoryStream = new MemoryStream();
                    bmp = null;
                    if(Intent.GetStringExtra("SHAPE").Equals("CIRCLE"))
                    {
                        if(bitmap.Width > AvatarWidth) {
                            bmp = Bitmap.CreateScaledBitmap(bitmap, AvatarWidth, AvatarHeight, true);                            
                        }
                        else
                        {
                            bmp = bitmap;
                        }
                    }
                    else if(Intent.GetStringExtra("SHAPE").Equals("BANNER")|| Intent.GetStringExtra("SHAPE").Equals("CONTEST"))
                    {
                        if(bitmap.Height > BannerMaxHeight)
                        {
                            BannerMaxWidth = BannerMaxHeight * bitmap.Width / bitmap.Height;
                            bmp = Bitmap.CreateScaledBitmap(bitmap, BannerMaxWidth, BannerMaxHeight, true);
                        }
                        else
                        {
                            bmp = bitmap;
                        }
                    } else
                    {
                        if(bitmap.Width > 1280 || bitmap.Height > 1280)
                        {
                            if (bitmap.Width > bitmap.Height)
                            {
                                int newHeight = 1280 * bitmap.Height / bitmap.Width;
                                bmp = Bitmap.CreateScaledBitmap(bitmap, 1280, newHeight, true);
                            }
                            else
                            {
                                int newWidth = 1280 * bitmap.Width / bitmap.Height;
                                bmp = Bitmap.CreateScaledBitmap(bitmap, 1280, newWidth, true);
                            }
                        }
                        else
                        {
                            bmp = bitmap;
                        }
                        
                    }


                    if(bmp != null)
                    {
                        Intent intent = new Intent();
                        if (Intent.GetStringExtra("SHAPE").Equals("CIRCLE"))
                        {
                            bmp.Compress(Bitmap.CompressFormat.Jpeg, 90, memoryStream);                        
      
                                UserViewModel viewmodel = new UserViewModel();
                                ProgressB.Animation = inAnimation;
                                ProgressB.Visibility = ViewStates.Visible;
                                viewmodel.RetryUpdateAvatarCommand.OperationCompleted += OnAvatarUploadCompleted;
                                viewmodel.RetryUpdateAvatarCommand.Execute(memoryStream);
       
                        }
                        else if (Intent.GetStringExtra("SHAPE").Equals("BANNER")){
                            bmp.Compress(Bitmap.CompressFormat.Jpeg, 90, memoryStream);

                                UserViewModel viewmodel = new UserViewModel();
                                ProgressB.Animation = inAnimation;
                                ProgressB.Visibility = ViewStates.Visible;
                                viewmodel.RetryUpdateBannerCommand.OperationCompleted += OnBannerUploadCompleted;
                                viewmodel.RetryUpdateBannerCommand.Execute(memoryStream);
   
                        }
                        else if (Intent.GetStringExtra("SHAPE").Equals("CONTEST"))
                        {
                            MediaHelper.GetInstance().SetBanner(bmp);
                            SetResult(Result.Ok);
                            Finish();
                        }
                        else if (Intent.GetStringExtra("SHAPE").Equals("OTHER"))
                        {
                            MediaHelper.GetInstance().SetResult(bmp);
                            SetResult(Result.Ok);
                            Finish();
                        }
                    }
                }
                confirm.Enabled = true;
            };

            modify.Click += (object sender, EventArgs e) =>
            {
                Intent intent = new Intent(this, typeof(CropActivity));
                intent.PutExtra("URI", Intent.GetStringExtra("ORIGINALURI"));
                intent.PutExtra("SHAPE", Intent.GetStringExtra("SHAPE"));
                intent.PutExtra("WIDTH", Intent.GetIntExtra("WIDTH", 2000));
                intent.PutExtra("HEIGHT", Intent.GetIntExtra("HEIGHT", 348));
                StartActivity(intent);
                Finish();
            };

            another.Click += (object sender, EventArgs e) =>
            {
                if (CropImage.IsExplicitCameraPermissionRequired(this))
                {
                    RequestPermissions(new[] { Manifest.Permission.Camera }, CropImage.CameraCapturePermissionsRequestCode);
                }
                else
                {
                    CropImage.StartPickImageActivity(this);
                }
            };

            another2.Click += (object sender, EventArgs e) =>
            {
                if (CropImage.IsExplicitCameraPermissionRequired(this))
                {
                    RequestPermissions(new[] { Manifest.Permission.Camera }, CropImage.CameraCapturePermissionsRequestCode);
                }
                else
                {
                    CropImage.StartPickImageActivity(this);
                }
            };

            another3.Click += (object sender, EventArgs e) =>
            {
                if (CropImage.IsExplicitCameraPermissionRequired(this))
                {
                    RequestPermissions(new[] { Manifest.Permission.Camera }, CropImage.CameraCapturePermissionsRequestCode);
                }
                else
                {
                    CropImage.StartPickImageActivity(this);
                }
            };

        }

        private void OnAvatarUploadCompleted(object sender, EventArgs args)
        {
            RunOnUiThread(() => {
                ProgressB.Animation = outAnimation;
                ProgressB.Visibility = ViewStates.Gone;

                CommandCompletedEventArgs commandArgs = args as CommandCompletedEventArgs;
                if (commandArgs.Status == CommandStates.Completed)
                {
                    MediaHelper.GetInstance().SetAvatar(bmp);
                    Finish();
                }
                else
                {
                    Toast.MakeText(this, "Non c'è connessione, riprova", Android.Widget.ToastLength.Short).Show();
                }
            });
        }

        private void OnBannerUploadCompleted(object sender, EventArgs args)
        {
            RunOnUiThread(() => {
                ProgressB.Animation = outAnimation;
                ProgressB.Visibility = ViewStates.Gone;

                CommandCompletedEventArgs commandArgs = args as CommandCompletedEventArgs;
                if (commandArgs.Status == CommandStates.Completed)
                {
                    MediaHelper.GetInstance().SetBanner(bmp);
                    Finish();
                }
                else
                {
                    Toast.MakeText(this, "Non c'è connessione, riprova", Android.Widget.ToastLength.Short).Show();
                }
            });
        }


        protected override void OnActivityResult(int requestCode, [GeneratedEnum] Result resultCode, Intent data)
        {
            base.OnActivityResult(requestCode, resultCode, data);

            if (requestCode == CropImage.PickImageChooserRequestCode && resultCode == Result.Ok)
            {
                var imageUri = CropImage.GetPickImageResultUri(this, data);

                // For API >= 23 we need to check specifically that we have permissions to read external storage,
                // but we don't know if we need to for the URI so the simplest is to try open the stream and see if we get error.
                var requirePermissions = false;
                if (CropImage.IsReadExternalStoragePermissionsRequired(this, imageUri))
                {

                    // request permissions and handle the result in onRequestPermissionsResult()
                    requirePermissions = true;
                    _cropImageUri = imageUri;
                    RequestPermissions(new[] { Manifest.Permission.ReadExternalStorage }, CropImage.PickImagePermissionsRequestCode);
                }
                else
                {
                    Intent intent = new Intent(this, typeof(CropActivity));
                    intent.PutExtra("URI", imageUri.ToString());
                    intent.PutExtra("SHAPE", Intent.GetStringExtra("SHAPE"));
                    StartActivity(intent);
                    Finish();
                }
            }
        }

        public override void OnRequestPermissionsResult(int requestCode, string[] permissions, [GeneratedEnum] Permission[] grantResults)
        {
            if (requestCode == CropImage.CameraCapturePermissionsRequestCode)
            {
                if (grantResults.Length > 0 && grantResults[0] == Permission.Granted)
                {
                    CropImage.StartPickImageActivity(this);
                }
                else
                {
                    Toast.MakeText(this, "Cancelling, required permissions are not granted", ToastLength.Long).Show();
                }
            }
            if (requestCode == CropImage.PickImagePermissionsRequestCode)
            {
                if (_cropImageUri != null && grantResults.Length > 0 && grantResults[0] == Permission.Granted)
                {
                    Intent intent = new Intent(this, typeof(CropActivity));
                    intent.PutExtra("URI", _cropImageUri.ToString());
                    intent.PutExtra("SHAPE", Intent.GetStringExtra("SHAPE"));
                    StartActivity(intent);
                    Finish();
                }
                else
                {
                    Toast.MakeText(this, "Cancelling, required permissions are not granted", ToastLength.Long).Show();
                }
            }
        }



        //private int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight)
        //{
        //    // Raw height and width of image
        //    int height = options.OutHeight;
        //    int width = options.OutWidth;
        //    int inSampleSize = 1;

        //    if (height > reqHeight || width > reqWidth)
        //    {

        //        int halfHeight = height / 2;
        //        int halfWidth = width / 2;

        //        // Calculate the largest inSampleSize value that is a power of 2 and keeps both
        //        // height and width larger than the requested height and width.
        //        while ((halfHeight / inSampleSize) > reqHeight
        //                && (halfWidth / inSampleSize) > reqWidth)
        //        {
        //            inSampleSize *= 2;
        //        }
        //    }

        //    return inSampleSize;
        //}

    }
}