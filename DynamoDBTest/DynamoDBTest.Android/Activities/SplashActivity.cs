﻿using Android.App;
using Android.Content;
using Android.OS;
using Android.Support.V7.App;

using System;
using System.Collections.Generic;

using DynamoDBTest.Controllers;
using DynamoDBTest.Helpers;
using static Android.App.ActivityManager;

namespace DynamoDBTest.Droid
{
	[Activity(Label = "@string/app_name", Theme = "@style/SplashTheme", MainLauncher = true)]
	public class SplashActivity : AppCompatActivity
	{
		protected override void OnCreate(Bundle savedInstanceState)
		{
			base.OnCreate(savedInstanceState);

            LoginController controller = LoginController.GetInstance();

            if (Settings.Get("automatic_login").Equals("true") && controller.SetSavedCredentials())
            {
                controller.LoginTerminated += new LoginController.LoginTerminatedHandler(OnLoginTerminated);
                controller.RetryLoginCommand.OperationCompleted += OnLoginError;

                controller.RetryLoginCommand.Execute(null);
            }
            else
            {
                var newIntent = new Intent(this, typeof(LoginActivity));
                newIntent.AddFlags(ActivityFlags.ClearTop);
                newIntent.AddFlags(ActivityFlags.SingleTop);

                StartActivity(newIntent);
                Finish();
            }
		}

        private void OnLoginTerminated(LoginController controller, int result, string message, string email)
        {
            this.RunOnUiThread(() => {
                Intent newIntent;
                switch (result)
                {
                    case LoginController.LOGIN_SUCCESSFULL:
                        newIntent = new Intent(this, typeof(MainActivity));
                        StartActivity(newIntent);
                        this.Finish();
                        break;
                    case LoginController.EMAIL_NOT_CONFIRMED:
                        newIntent = new Intent(this, typeof(ConfirmCodeActivity));
                        newIntent.PutExtra("email", email);
                        StartActivity(newIntent);
                        break;
                    default:
                        newIntent = new Intent(this, typeof(LoginActivity));
                        newIntent.AddFlags(ActivityFlags.ClearTop);
                        newIntent.AddFlags(ActivityFlags.SingleTop);
                        break;
                }
                StartActivity(newIntent);
                Finish();
            });
        }

        private void OnLoginError(object sender, EventArgs a)
        {
            CommandCompletedEventArgs args = a as CommandCompletedEventArgs;
            if (args.Status == CommandStates.MaxRetryReached)
            {
                RunOnUiThread(() => {
                    var newIntent = new Intent(this, typeof(LoginActivity));
                    newIntent.AddFlags(ActivityFlags.ClearTop);
                    newIntent.AddFlags(ActivityFlags.SingleTop);

                    StartActivity(newIntent);
                    Finish();
                });
            }
        }
    }
}
