﻿using Android.OS;
using Android.Runtime;
using Android.Support.Design.Widget;
using Android.Support.V7.App;
using Android.Support.V7.Widget;
using Android.Views;
using Android.Widget;

using DynamoDBTest.Helpers;

namespace DynamoDBTest.Droid
{
	public class BaseActivity : AppCompatActivity
	{
		protected override void OnCreate(Bundle savedInstanceState)
		{
			base.OnCreate(savedInstanceState);
			SetContentView(LayoutResource);
			Toolbar = FindViewById<Android.Support.V7.Widget.Toolbar>(Resource.Id.toolbar);
			if (Toolbar != null)
			{
				SetSupportActionBar(Toolbar);
				SupportActionBar.SetDisplayHomeAsUpEnabled(true);
				SupportActionBar.SetHomeButtonEnabled(true);
                SupportActionBar.SetDisplayShowTitleEnabled(false);

			}

            AuthenticationLoopCheck.GetInstance().LoginLoopError += new AuthenticationLoopCheck.LoginLoopErrorHandler(OnLoginLoopError);
		}

        private void OnLoginLoopError(int errorType, string message)
        {
            Toast.MakeText(this, message, ToastLength.Short).Show();
        }

		public Android.Support.V7.Widget.Toolbar Toolbar
		{
			get;
			set;
		}

		protected virtual int LayoutResource
		{
			get;
		}

		protected int ActionBarIcon
		{
			set { Toolbar?.SetNavigationIcon(value); }
		}

        public override bool OnSupportNavigateUp()
        {
            this.Finish();
            return true;
        }

        public override bool OnKeyDown([GeneratedEnum] Keycode keyCode, KeyEvent e)
        {
            if(keyCode == Keycode.Back)
            {
                this.Finish();
                return true;
            }
            return base.OnKeyDown(keyCode, e);
        }

        public virtual AppBarLayout GetAppbar()
        {
            return null;
        }

    }
}
