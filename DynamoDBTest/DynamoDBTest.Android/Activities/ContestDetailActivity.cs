﻿using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.OS;
using Android.Widget;
using Android.Runtime;
using Android.Views;
using Android.Support.Design.Widget;
using Android.Support.V4.View;
using Android.Support.V4.App;
using Android.Support.V4.Content;
using Android.Support.V7.Widget;
using Android.Graphics;
using Android.Animation;

using DynamoDBTest.Models;
using DynamoDBTest.ViewModels;
using DynamoDBTest.Controllers;
using DynamoDBTest.Helpers;
using DynamoDBTest.Services;

using System;
using Refractored.Controls;
using Com.Theartofdev.Edmodo.Cropper;
using Uri = Android.Net.Uri;
using Android;
using Android.Views.Animations;
using System.IO;
using Com.Bumptech.Glide;
using Com.Bumptech.Glide.Request;
using Com.Bumptech.Glide.Request.Target;

namespace DynamoDBTest.Droid
{
    [Activity(Label = "Details")]
    //[MetaData("android.support.PARENT_ACTIVITY", Value = ".MainActivity")]
    public class ContestDetailActivity : BaseActivity
    {
        protected override int LayoutResource => Resource.Layout.activity_contest_detail;

        public ContestDetailViewModel viewModel;
        public PartecipationViewModel fragmentPartecipationViewModel;
        public PartecipationDetailViewModel victoryPartecipationViewModel;
        private UserViewModel um;

        private const int IMAGE_RESULT_CODE = 5514;
        private const int TEXT_RESULT_CODE = 5513;
        private Uri _cropImageUri;
        User user;
        TextView contestName;
        TextView PartecipationsNumber;
        string partecipationId;
        public Button partecipate1;
        public Button partecipate2;
        ImageButton Banner;
        TextView Desc;
        public AlphaAnimation inAnimation;
        public AlphaAnimation outAnimation;
        public FrameLayout ProgressB;
        ViewPager pager;
        TabLayout tabs;
        PartecipationTabsAdapter adapter;
        bool loggedUser;
        ushort requestCode;
        private int bannerHeight;
        private int screenHeight;
        private int screenWidth;
        private int newBannerHeight;
        private string resultText;
        public AppBarLayout appbar;

        int tabsAdapterPosition;
        bool contestEdited;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            var data = Intent.GetStringExtra("data");
            tabsAdapterPosition = Intent.GetIntExtra("tab", 0);
            contestEdited = false;
            base.OnCreate(savedInstanceState);

            appbar = FindViewById<AppBarLayout>(Resource.Id.appbar);
            contestName = FindViewById<TextView>(Resource.Id.contestname);
            Desc = FindViewById<TextView>(Resource.Id.desc);
            PartecipationsNumber = FindViewById<TextView>(Resource.Id.pnumber);
            Banner = FindViewById<ImageButton>(Resource.Id.banner);
            ProgressB = FindViewById<FrameLayout>(Resource.Id.progress);
            inAnimation = new AlphaAnimation(0f, 1f);
            inAnimation.Duration = 200;
            outAnimation = new AlphaAnimation(1f, 0f);
            outAnimation.Duration = 200;
            adapter = new PartecipationTabsAdapter(this, SupportFragmentManager);
            pager = FindViewById<ViewPager>(Resource.Id.viewpager);
            tabs = FindViewById<TabLayout>(Resource.Id.tabs);

            partecipate1 = FindViewById<Button>(Resource.Id.partecipateButton1);
            partecipate2 = FindViewById<Button>(Resource.Id.partecipateButton2);

            if (data != null)
            {
                viewModel = new ContestDetailViewModel(Newtonsoft.Json.JsonConvert.DeserializeObject<Contest>(data));
                BuildActivity();
            }
            else
            {
                viewModel = new ContestDetailViewModel();
                viewModel.RetryLoadItemCommand.OperationCompleted += OnItemLoaded;
                viewModel.RetryLoadItemCommand.Execute(Intent.GetStringExtra("contestId"));
            }
        }

        private void OnItemLoaded(object sender, EventArgs args)
        {
            RunOnUiThread(() =>
            {

                CommandCompletedEventArgs commandArgs = args as CommandCompletedEventArgs;
                if (commandArgs.Status == CommandStates.Completed)
                {
                    BuildActivity();
                }
                else
                {
                    Toast.MakeText(this, "Non c'è connessione, riprova", Android.Widget.ToastLength.Short).Show();
                }
            });
        }

        private void BuildActivity()
        {
            RunOnUiThread(() =>
            {
                loggedUser = (LoginController.GetInstance().CurrentUser.Email == viewModel.contest.CreatorUser);
                Intent.PutExtra("itemId", viewModel.contest.Id);
                Intent.PutExtra("type", ItemType.Contest.ToString());
                PartecipationsNumber.Text = "Partecipazioni: " + viewModel.contest.HasPartecipations.Count;
                contestName.Text = viewModel.contest.Name;

                if (viewModel.contest.Banner != null && !MediaHelper.GetInstance().HasBanner())
                {
                    RequestOptions options = new RequestOptions().Override(Target.SizeOriginal, Target.SizeOriginal);
                    Glide.With(this).Load(viewModel.contest.Banner).Apply(options).Into(Banner);
                }
                else if (MediaHelper.GetInstance().HasBanner())
                {
                    Banner.SetImageBitmap(MediaHelper.GetInstance().GetBanner());
                }

                if (!loggedUser)
                {
                    Banner.Enabled = false;
                    Desc.Enabled = false;
                }
                string cDesc = viewModel.contest.Description;

                if (!String.IsNullOrWhiteSpace(cDesc))
                {
                    Desc.Visibility = ViewStates.Visible;
                    Desc.Text = viewModel.contest.Description;
                }
                else if (loggedUser)
                {
                    Desc.Visibility = ViewStates.Visible;
                    Desc.Text = "Clicca qui per aggiungere una descrizione al tuo contest.";
                }

                user = LoginController.GetInstance().CurrentUser;
                partecipationId = user.Email + viewModel.contest.Id;

                if (DateTime.Compare(viewModel.contest.EndDate, DateTime.Now) <= 0)
                {
                    partecipate1.Visibility = ViewStates.Gone;
                    victoryPartecipationViewModel = new PartecipationDetailViewModel();
                    victoryPartecipationViewModel.ItemLoaded += (object sender, EventArgs e) =>
                    {
                        if (victoryPartecipationViewModel.victoryPartecipation != null)
                        {
                            fragmentPartecipationViewModel.Items.Insert(0, victoryPartecipationViewModel.victoryPartecipation);
                        }
                    };

                    if (viewModel.contest.VictoryPartecipation == null)
                    {
                        victoryPartecipationViewModel.RetryLoadVictoryPartecipationCommand.Execute(viewModel.contest.Id);
                    }
                    else
                    {
                        victoryPartecipationViewModel.RetryLoadItemCommand.Execute(viewModel.contest.VictoryPartecipation);
                    }
                }

                Banner.Click += (object sender, EventArgs e) =>
                {
                    var builder = new AlertDialog.Builder(this);
                    String[] options;

                    if (viewModel.contest.Banner != null)
                    {
                        options = new[] { "Cambia immagine", "Rimuovi Immagine" };
                    }
                    else
                    {
                        options = new[] { "Aggiungi immagine" };
                    }
                    builder.SetItems(options, new EventHandler<DialogClickEventArgs>(
                    (s, args) =>
                    {
                        if (args.Which == 0)
                        {
                            if (CropImage.IsExplicitCameraPermissionRequired(this))
                            {
                                RequestPermissions(new[] { Manifest.Permission.Camera }, CropImage.CameraCapturePermissionsRequestCode);
                            }
                            else
                            {
                                CropImage.StartPickImageActivity(this);
                            }
                        }
                        else
                        {
                            ProgressB.Animation = inAnimation;
                            ProgressB.Visibility = ViewStates.Visible;
                            viewModel.RetryDeleteBannerCommand.OperationCompleted += OnBannerDeleteCompleted;
                            viewModel.RetryDeleteBannerCommand.Execute(viewModel.contest);
                        };
                    }));
                    builder.Create().Show();
                };

                Desc.Click += (object sender, EventArgs e) =>
                {

                    if ((Desc.Text != "Clicca qui per aggiungere una descrizione al tuo contest.") && !String.IsNullOrWhiteSpace(Desc.Text))
                    {
                        var builder = new AlertDialog.Builder(this);
                        String[] options;

                        options = new[] { "Modifica descrizione", "Rimuovi descrizione" };
                        builder.SetItems(options, new EventHandler<DialogClickEventArgs>(
                        (s, args) =>
                        {
                            if (args.Which == 0)
                            {
                                var newIntent = new Intent(this, typeof(ModifyTextActivity));
                                if(Desc.Text != "Clicca qui per aggiungere una descrizione al tuo contest.")
                                {
                                    newIntent.PutExtra("text", Desc.Text);
                                }
                                StartActivityForResult(newIntent, TEXT_RESULT_CODE);
                                OverridePendingTransition(Resource.Animator.EnterBottomToTop, Resource.Animator.Still);
                            }
                            else
                            {
                                ProgressB.Animation = inAnimation;
                                ProgressB.Visibility = ViewStates.Visible;
                                viewModel.RetryDeleteTextCommand.OperationCompleted += OnTextDeleteCompleted;
                                viewModel.RetryDeleteTextCommand.Execute(viewModel.contest.Id);
                            };
                        }));
                        builder.Create().Show();
                    }
                    else
                    {
                        var newIntent = new Intent(this, typeof(ModifyTextActivity));
                        StartActivityForResult(newIntent, TEXT_RESULT_CODE);
                        OverridePendingTransition(Resource.Animator.EnterBottomToTop, Resource.Animator.Still);
                    }
                };


                partecipate1.Click += (object sender, EventArgs e) =>
               {
                   if (!partecipate1.Activated)
                   {
                       var newIntent = new Intent(this, typeof(AddPartecipationActivity));
                       newIntent.PutExtra("data", Newtonsoft.Json.JsonConvert.SerializeObject(viewModel.contest));
                       requestCode = (ushort)(new Random().Next(ushort.MinValue, ushort.MaxValue));
                       MediaHelper.GetInstance().SetBanner(null);
                       MediaHelper.GetInstance().SetResult(null);
                       StartActivityForResult(newIntent, requestCode);
                   }
                   else
                   {
                       AlertDialog.Builder builder = new AlertDialog.Builder(this, Resource.Style.WarningAlertDialogStyle);
                       builder.SetTitle("Vuoi annullare la tua partecipazione?");
                       builder.SetMessage("Una volta confermato la partecipazione sparirà per sempre e perderai tutti i voti legati ad essa.");
                       builder.SetPositiveButton("SI", (senderAlert, args) =>
                       {
                           ProgressB.Animation = inAnimation;
                           ProgressB.Visibility = ViewStates.Visible;
                           fragmentPartecipationViewModel.RetryDeletePartecipationCommand.OperationCompleted += OnPartecipationDeleted;
                           fragmentPartecipationViewModel.RetryDeletePartecipationCommand.Execute(partecipationId);
                       });
                       builder.SetNegativeButton("NO", (senderAlert, args) =>
                       {
                       });
                       builder.Create().Show();
                   }
               };

                pager.Adapter = adapter;
                tabs.SetupWithViewPager(pager);
                tabs.AddOnTabSelectedListener(adapter);
                pager.OffscreenPageLimit = 5;

                pager.PageSelected += (sender, args) =>
                {
                    adapter.currentposition = args.Position;
                    var fragment = adapter.InstantiateItem(pager, args.Position) as IFragmentVisible;
                    fragment?.BecameVisible();
                    if (args.Position == 1 && tabsAdapterPosition == 1)
                    {
                        tabsAdapterPosition = 0;
                    }
                };

                if (user.Partecipations.Contains(partecipationId))
                {
                    partecipate1.Activated = true;
                    partecipate1.Text = "Partecipando";
                    partecipate1.SetTextColor(Color.White);
                }
                else
                {
                    partecipate1.Activated = false;
                    partecipate1.Text = "Partecipa";
                    partecipate1.SetTextColor(ContextCompat.GetColorStateList(this, Resource.Color.mediumFont));
                }

                if (Resources.Configuration.Orientation == Android.Content.Res.Orientation.Landscape)
                {
                    Banner.Post(() =>
                    {
                        bannerHeight = Banner.Height;
                        screenHeight = MediaHelper.GetInstance().ScreenHeight();
                        screenWidth = MediaHelper.GetInstance().ScreenWidth();
                        newBannerHeight = screenHeight * bannerHeight / screenWidth;
                        Banner.LayoutParameters.Height = newBannerHeight;
                    });
                }

                tabs.Post(() =>
                {
                    if (tabsAdapterPosition == 1)
                    {
                        TabLayout.Tab tab = tabs.GetTabAt(tabsAdapterPosition);
                        tab.Select();
                    }
                });
            });
        }

        protected override void OnStart()
        {
            base.OnStart();
        }

        public override AppBarLayout GetAppbar()
        {
            return appbar;
        }

        protected override void OnActivityResult(int requestCode, [GeneratedEnum] Result resultCode, Intent data)
        {
            base.OnActivityResult(requestCode, resultCode, data);

            if (requestCode == this.requestCode)
            {
                if (resultCode == Result.Ok)
                {
                    Partecipation newPartecipation = GlobalCollections.GetInstance().GetPartecipation(data.GetStringExtra("newPartecipationId"));
                    fragmentPartecipationViewModel.PutPartecipationInCollection(newPartecipation);
                    AuthenticationLoopCheck.GetInstance().DoOne();
                    partecipate1.Activated = true;
                    partecipate1.Text = "Partecipando";
                    partecipate1.SetTextColor(Color.White);
                    PartecipationsNumber.Text = "Partecipazioni: " + viewModel.contest.HasPartecipations.Count.ToString();
                }
                else
                {
                    partecipate1.Activated = false;
                    partecipate1.Text = "Partecipa";
                    partecipate1.SetTextColor(ContextCompat.GetColorStateList(this, Resource.Color.mediumFont));
                }
            }
            else if (requestCode == CropImage.PickImageChooserRequestCode && resultCode == Result.Ok)
            {
                var imageUri = CropImage.GetPickImageResultUri(this, data);

                var requirePermissions = false;
                if (CropImage.IsReadExternalStoragePermissionsRequired(this, imageUri))
                {
                    requirePermissions = true;
                    _cropImageUri = imageUri;
                    RequestPermissions(new[] { Manifest.Permission.ReadExternalStorage }, CropImage.PickImagePermissionsRequestCode);
                }
                else
                {
                    Intent intent = new Intent(this, typeof(CropActivity));
                    intent.PutExtra("URI", imageUri.ToString());
                    intent.PutExtra("SHAPE", "CONTEST");
                    intent.PutExtra("WIDTH", Banner.Width);
                    intent.PutExtra("HEIGHT", Banner.Height);
                    StartActivityForResult(intent, IMAGE_RESULT_CODE);
                }
            }
            else if (requestCode == IMAGE_RESULT_CODE && resultCode == Result.Ok)
            {
                if (MediaHelper.GetInstance().HasBanner())
                {
                    MemoryStream memoryStream = new MemoryStream();
                    MediaHelper.GetInstance().GetBanner().Compress(Bitmap.CompressFormat.Jpeg, 90, memoryStream);
                    Contest c = viewModel.contest;
                    c.TempBanner = memoryStream;
                    ProgressB.Animation = inAnimation;
                    ProgressB.Visibility = ViewStates.Visible;
                    viewModel.RetryAddBannerCommand.OperationCompleted += OnBannerUpdateCompleted;
                    viewModel.RetryAddBannerCommand.Execute(c);
                }
            }
            else if (requestCode == TEXT_RESULT_CODE && resultCode == Result.Ok)
            {
                string newText = data.GetStringExtra("text");
                ProgressB.Animation = inAnimation;
                ProgressB.Visibility = ViewStates.Visible;
                if (String.IsNullOrWhiteSpace(newText))
                {
                    viewModel.RetryDeleteTextCommand.OperationCompleted += OnTextDeleteCompleted;
                    viewModel.RetryDeleteTextCommand.Execute(viewModel.contest.Id);
                }
                else
                {
                    viewModel.RetryUpdateTextCommand.OperationCompleted += OnTextUpdateCompleted;
                    var stringwrapper = new StringWrapper(viewModel.contest.Id, newText);
                    viewModel.RetryUpdateTextCommand.Execute(stringwrapper);
                    resultText = newText;
                }
            }
        }

        public override void OnRequestPermissionsResult(int requestCode, string[] permissions, [GeneratedEnum] Permission[] grantResults)
        {
            if (requestCode == CropImage.CameraCapturePermissionsRequestCode)
            {
                if (grantResults.Length > 0 && grantResults[0] == Permission.Granted)
                {
                    CropImage.StartPickImageActivity(this);
                }
                else
                {
                    Toast.MakeText(this, "Cancelling, required permissions are not granted", ToastLength.Long).Show();
                }
            }
            else
            if (requestCode == CropImage.PickImagePermissionsRequestCode)
            {
                if (_cropImageUri != null && grantResults.Length > 0 && grantResults[0] == Permission.Granted)
                {
                    Intent intent = new Intent(this, typeof(CropActivity));
                    intent.PutExtra("URI", _cropImageUri.ToString());
                    intent.PutExtra("SHAPE", "CONTEST");
                    intent.PutExtra("WIDTH", Banner.Width);
                    intent.PutExtra("HEIGHT", Banner.Height);
                    StartActivityForResult(intent, IMAGE_RESULT_CODE);
                }
                else
                {
                    Toast.MakeText(this, "Cancelling, required permissions are not granted", ToastLength.Long).Show();
                }
            }
        }

        protected override void OnSaveInstanceState(Bundle outState)
        {
            outState.PutBoolean("loggedUser", loggedUser);
            base.OnSaveInstanceState(outState);
        }

        protected override void OnRestoreInstanceState(Bundle savedInstanceState)
        {
            loggedUser = savedInstanceState.GetBoolean("loggedUser");
            base.OnRestoreInstanceState(savedInstanceState);
        }

        private void OnBannerUpdateCompleted(object sender, EventArgs args)
        {
            RunOnUiThread(() =>
            {
                ProgressB.Animation = outAnimation;
                ProgressB.Visibility = ViewStates.Gone;

                CommandCompletedEventArgs commandArgs = args as CommandCompletedEventArgs;
                if (commandArgs.Status == CommandStates.Completed)
                {
                    if (MediaHelper.GetInstance().HasBanner())
                    {
                        Banner.SetImageBitmap(MediaHelper.GetInstance().GetBanner());
                    }
                    contestEdited = true;
                }
                else
                {
                    Toast.MakeText(this, "Non c'è connessione, riprova", Android.Widget.ToastLength.Short).Show();
                }
            });
        }

        private void OnBannerDeleteCompleted(object sender, EventArgs args)
        {
            RunOnUiThread(() =>
            {
                ProgressB.Animation = outAnimation;
                ProgressB.Visibility = ViewStates.Gone;

                CommandCompletedEventArgs commandArgs = args as CommandCompletedEventArgs;
                if (commandArgs.Status == CommandStates.Completed)
                {
                    Banner.SetImageResource(Resource.Color.imageBlank);
                    MediaHelper.GetInstance().SetBanner(null);
                    contestEdited = true;
                }
                else
                {
                    Toast.MakeText(this, "Non c'è connessione, riprova", Android.Widget.ToastLength.Short).Show();
                }
            });
        }

        private void OnTextDeleteCompleted(object sender, EventArgs args)
        {
            RunOnUiThread(() =>
            {
                ProgressB.Animation = outAnimation;
                ProgressB.Visibility = ViewStates.Gone;

                CommandCompletedEventArgs commandArgs = args as CommandCompletedEventArgs;
                if (commandArgs.Status == CommandStates.Completed)
                {
                    Desc.Text = "Clicca qui per aggiungere una descrizione al tuo contest.";
                    contestEdited = true;
                }
                else
                {
                    Toast.MakeText(this, "Non c'è connessione, riprova", Android.Widget.ToastLength.Short).Show();
                }
            });
        }

        private void OnTextUpdateCompleted(object sender, EventArgs args)
        {
            RunOnUiThread(() =>
            {
                ProgressB.Animation = outAnimation;
                ProgressB.Visibility = ViewStates.Gone;

                CommandCompletedEventArgs commandArgs = args as CommandCompletedEventArgs;
                if (commandArgs.Status == CommandStates.Completed)
                {
                    Desc.Text = resultText;
                    contestEdited = true;
                }
                else
                {
                    Toast.MakeText(this, "Non c'è connessione, riprova", Android.Widget.ToastLength.Short).Show();
                }
            });
        }

        private void OnPartecipationDeleted(object sender, EventArgs args)
        {
            RunOnUiThread(() =>
            {
                ProgressB.Animation = outAnimation;
                ProgressB.Visibility = ViewStates.Gone;

                CommandCompletedEventArgs commandArgs = args as CommandCompletedEventArgs;
                if (commandArgs.Status == CommandStates.Completed)
                {
                    fragmentPartecipationViewModel.RemovePartecipationInCollection(partecipationId);
                    partecipate1.Activated = false;
                    PartecipationsNumber.Text = "Partecipazioni: "+ viewModel.contest.HasPartecipations.Count.ToString();
                }
                else
                {
                    Toast.MakeText(this, "Non c'è connessione, riprova", Android.Widget.ToastLength.Short).Show();
                }
            });
        }

        public override bool OnKeyDown([GeneratedEnum] Keycode keyCode, KeyEvent e)
        {
            if (keyCode == Keycode.Back)
            {
                Intent intent = new Intent();
                if (contestEdited)
                {
                    GlobalCollections.GetInstance().Add(viewModel.contest);
                    intent.PutExtra("contestUpdatedId", viewModel.contest.Id);
                    SetResult(0, intent);
                }
                else
                {
                    SetResult(Result.Ok, intent);
                }
                this.Finish();
                return true;
            }
            return base.OnKeyDown(keyCode, e);
        }
    }

    class PartecipationTabsAdapter : FragmentStatePagerAdapter, TabLayout.IOnTabSelectedListener
    {
        string[] titles;

        public override int Count => titles.Length;
        public int currentposition = 0;
        ContestPartecipationsFragment p;
        CommentsFragment c;

        public PartecipationTabsAdapter(Context context, Android.Support.V4.App.FragmentManager fm) : base(fm)
        {
            titles = context.Resources.GetTextArray(Resource.Array.contest_sections);
        }

        public override Java.Lang.ICharSequence GetPageTitleFormatted(int position) =>
                            new Java.Lang.String(titles[position]);

        public override Android.Support.V4.App.Fragment GetItem(int position)
        {
            if (position == 0) { p = ContestPartecipationsFragment.NewInstance(); return p; }
            else if (position == 1) { c = CommentsFragment.NewInstance(); return c; }
            else { return null; }
        }

        public override int GetItemPosition(Java.Lang.Object frag) => PositionNone;

        public override Java.Lang.Object InstantiateItem(ViewGroup container, int position)
        {
            Java.Lang.Object ret = base.InstantiateItem(container, position);

            if (position == 0)
            {
                p = (ContestPartecipationsFragment)ret;
            } else if (position == 1)
            {
                c = (CommentsFragment)ret;
            }
            return ret;
        }

        public void OnTabReselected(TabLayout.Tab tab)
        {
            if (currentposition == 0)
            {
                if (p != null)
                {
                    p.recyclerView.SmoothScrollToPosition(0);
                }
            }
            else if (currentposition == 1)
            {
                if (c != null)
                {
                    c.recyclerView.SmoothScrollToPosition(0);
                }
            }
        }

        public void OnTabSelected(TabLayout.Tab tab)
        {
        }

        public void OnTabUnselected(TabLayout.Tab tab)
        {
        }
    }




    class PartecipationsAdapter : ItemsAdapter<Partecipation>
    {
        public static ushort PARTECIPATE_REQUEST_CODE = 10;

        public int requestFromIndex;

        public PartecipationsAdapter(Activity activity, PartecipationViewModel viewModel) : base(activity, viewModel)
        {
            
        }

        public override RecyclerView.ViewHolder OnCreateViewHolder(ViewGroup parent, int viewType)
        {
            View itemView = null;
            var id = Resource.Layout.partecipation_browse;
            itemView = LayoutInflater.From(parent.Context).Inflate(id, parent, false);

            var vh = new PartecipationViewHolder(itemView, OnClick, OnLongClick);
            return vh;
        }

        public override void OnBindViewHolder(RecyclerView.ViewHolder holder, int position)
        {
            var partecipation = viewModel.Items[position];
            var user = LoginController.GetInstance().CurrentUser;
            var myHolder = holder as PartecipationViewHolder;

            myHolder.Username.Text = partecipation.CreatorUserName;
            myHolder.Date.Text = PrettyDate.GetPrettyDate(partecipation.SortingDate);
            myHolder.Contest.Text = partecipation.PartecipatingToContestName;
            if (partecipation.Content != null)
            {
                myHolder.Description.Visibility = ViewStates.Visible;
                myHolder.Description.Text = partecipation.Content;
            }
            else
            {
                myHolder.Description.Visibility = ViewStates.Gone;
            }

            if (DateTime.Compare(partecipation.ContestEndDate, DateTime.Now) <= 0)
            {
                myHolder.Partecipate.Visibility = ViewStates.Gone;
                myHolder.Partecipate.Clickable = false;
            }
            else
            {
                myHolder.Partecipate.Visibility = ViewStates.Visible;
                myHolder.Partecipate.Clickable = true;
            }

            if (partecipation.Media != null)
            {
                Glide.With(activity).Load(partecipation.Media).Into(myHolder.Media);
                myHolder.Media.Clickable = true;
            }
            else
            {
                myHolder.Media.SetImageResource(Resource.Color.imageBlank);
                myHolder.Media.Clickable = false;
            }

            if (partecipation.Avatar != null)
            {
                Glide.With(activity).Load(partecipation.Avatar).Into(myHolder.Avatar);
            }
            else
            {
                myHolder.Avatar.SetImageResource(Resource.Color.imageBlank);
            }

            if (partecipation.Winner == 1)
            {
                myHolder.Crown.Visibility = ViewStates.Visible;
                myHolder.Verb.Text = "Ha vinto il Contest:";
            }
            else
            {
                myHolder.Crown.Visibility = ViewStates.Gone;
                myHolder.Verb.Text = "Partecipa al Contest:";
            }

            myHolder.Votes.Text = partecipation.Votes.Count.ToString();
            myHolder.Shares.Text = partecipation.Shares.ToString();
            myHolder.Comments.Text = partecipation.Comments.Count.ToString();

            if (partecipation.Votes.Contains(user.Email))
            {
                myHolder.AddVote.Activated = true;
            }

            myHolder.OnAddVoteClick = (object sender, EventArgs e) =>
            {
                if (DateTime.Compare(partecipation.ContestEndDate, DateTime.Now) <= 0)
                {
                    Toast.MakeText(activity, "Il contest è finito!", ToastLength.Short).Show();
                }
                else
                {
                    myHolder.AddVote.Enabled = false;
                    if (!myHolder.AddVote.Activated)
                    {
                        ((PartecipationViewModel)viewModel).RetryAddVoteCommand.Execute(position);

                        if (partecipation.Votes.Contains(user.Email))
                        {
                            int newcount = partecipation.Votes.Count;
                            Int32.TryParse(myHolder.Votes.Text, out newcount);
                            myHolder.Votes.Text = (newcount + 1).ToString();
                            myHolder.AddVote.Activated = true;
                        }

                    }
                    else
                    {
                        ((PartecipationViewModel)viewModel).RetryRemoveVoteCommand.Execute(position);

                        if (!partecipation.Votes.Contains(user.Email))
                        {
                            int newcount = partecipation.Votes.Count;
                            Int32.TryParse(myHolder.Votes.Text, out newcount);
                            myHolder.Votes.Text = (newcount - 1).ToString();
                            myHolder.AddVote.Activated = false;
                        }
                    }
                    myHolder.AddVote.Enabled = true;
                }
            };
            myHolder.AddVote.Click += myHolder.OnAddVoteClick;

            myHolder.OnCommentClick = (object sender, EventArgs e) =>
            {
                myHolder.Comment.Clickable = false;
                var intent = new Intent(activity, typeof(PartecipationCommentActivity));
                intent.PutExtra("partecipationId", partecipation.Id);
                activity.StartActivity(intent);
                activity.OverridePendingTransition(Resource.Animator.EnterBottomToTop, Resource.Animator.Still);
                myHolder.Comment.Clickable = true;
            };
            myHolder.Comment.Click += myHolder.OnCommentClick;

            myHolder.OnMediaClick = (object sender, EventArgs e) =>
            {
                fragment.ImageZoom(myHolder.Media, partecipation.Media);
            };
            myHolder.Media.Click += myHolder.OnMediaClick;

            myHolder.OnShareClick = (object sender, EventArgs e) =>
            {
                var intent = new Intent(activity, typeof(AddShareActivity));
                intent.PutExtra("partecipation", partecipation.Id);
                activity.StartActivity(intent);
            };
            myHolder.Share.Click += myHolder.OnShareClick;

            if (user.Partecipations.Contains(user.Email + partecipation.PartecipatingToContest))
            {
                myHolder.Partecipate.Activated = true;
                myHolder.Partecipate.Text = "Partecipando";
                myHolder.Partecipate.SetTextColor(Color.White);
            }
            else
            {
                myHolder.Partecipate.Activated = false;
                myHolder.Partecipate.Text = "Partecipa";
                myHolder.Partecipate.SetTextColor(ContextCompat.GetColorStateList(activity, Resource.Color.mediumFont));
                if (activity.GetType().Equals(typeof(ContestDetailActivity)))
                {
                    ((ContestDetailActivity)activity).partecipate1.Activated = false;
                    ((ContestDetailActivity)activity).partecipate1.Text = "Partecipa";
                    ((ContestDetailActivity)activity).partecipate1.SetTextColor(ContextCompat.GetColorStateList(activity, Resource.Color.mediumFont));
                }
            }

            myHolder.OnPartecipateClick = (object sender, EventArgs e) =>
            {
                myHolder.Partecipate.Clickable = false;
                if (!myHolder.Partecipate.Activated)
                {
                    var newIntent = new Intent(activity, typeof(AddPartecipationActivity));
                    newIntent.PutExtra("partecipation", Newtonsoft.Json.JsonConvert.SerializeObject(partecipation));
                    requestFromIndex = position;
                    MediaHelper.GetInstance().SetBanner(null);
                    MediaHelper.GetInstance().SetResult(null);
                    activity.StartActivityForResult(newIntent, PARTECIPATE_REQUEST_CODE);
                }
                else
                {
                    AlertDialog.Builder builder = new AlertDialog.Builder(activity, Resource.Style.WarningAlertDialogStyle);
                    builder.SetTitle("Vuoi annullare la tua partecipazione?");
                    builder.SetMessage("Una volta confermato la partecipazione sparirà per sempre e perderai tutti i voti legati ad essa.");
                    builder.SetPositiveButton("SI", (senderAlert, args) => {
                        if (activity.GetType().Equals(typeof(UserPageActivity)))
                        {
                            ((UserPageActivity)activity).ProgressB.Animation = ((UserPageActivity)activity).outAnimation;
                            ((UserPageActivity)activity).ProgressB.Visibility = ViewStates.Gone;
                        }
                        else if (activity.GetType().Equals(typeof(PersonalUserPageActivity)))
                        {
                            ((PersonalUserPageActivity)activity).ProgressB.Animation = ((PersonalUserPageActivity)activity).outAnimation;
                            ((PersonalUserPageActivity)activity).ProgressB.Visibility = ViewStates.Gone;
                        }
                        else if (activity.GetType().Equals(typeof(MainActivity)))
                        {
                            ((MainActivity)activity).ProgressB.Animation = ((MainActivity)activity).outAnimation;
                            ((MainActivity)activity).ProgressB.Visibility = ViewStates.Gone;
                        }
                        else if (activity.GetType().Equals(typeof(ContestDetailActivity)))
                        {
                            ((ContestDetailActivity)activity).ProgressB.Animation = ((ContestDetailActivity)activity).outAnimation;
                            ((ContestDetailActivity)activity).ProgressB.Visibility = ViewStates.Gone;
                        }
                        PartecipationViewModel partecipationViewModel = new PartecipationViewModel();
                        partecipationViewModel.RetryDeletePartecipationCommand.Execute(user.Email + partecipation.PartecipatingToContest);
                    });
                    builder.SetNegativeButton("NO", (senderAlert, args) => {
                    });
                    builder.Create().Show();
                }
                myHolder.Partecipate.Clickable = true;
            };
            myHolder.Partecipate.Click += myHolder.OnPartecipateClick;

            myHolder.OnUserClick = (object sender, EventArgs e) =>
            {
                if (user.Email == partecipation.CreatorUser)
                {
                    var intent = new Intent(activity, typeof(PersonalUserPageActivity));
                    MediaHelper.GetInstance().ClearBitmaps();
                    activity.StartActivity(intent);
                }
                else
                {
                    var intent = new Intent(activity, typeof(UserPageActivity));
                    intent.PutExtra("userId", partecipation.CreatorUser);
                    intent.PutExtra("username", partecipation.CreatorUserName);
                    intent.PutExtra("avatar", partecipation.Avatar);
                    activity.StartActivity(intent);
                }
            };
            myHolder.Username.Click += myHolder.OnUserClick;
            myHolder.Avatar.Click += myHolder.OnUserClick;

            myHolder.OnContestClick = (object sender, EventArgs e) =>
            {
                if (!activity.GetType().Equals(typeof(ContestDetailActivity)))
                {
                    var intent = new Intent(activity, typeof(ContestDetailActivity));
                    MediaHelper.GetInstance().ClearBitmaps();
                    intent.PutExtra("contestId", partecipation.PartecipatingToContest);
                    activity.StartActivity(intent);
                }
            };
            myHolder.Contest.Click += myHolder.OnContestClick;
        }

        public override void OnViewRecycled(Java.Lang.Object holder)
        {
            base.OnViewRecycled(holder);

            PartecipationViewHolder viewHolder = holder as PartecipationViewHolder;
            viewHolder.AddVote.Activated = false;
            viewHolder.Username.Click -= viewHolder.OnUserClick;
            viewHolder.Avatar.Click -= viewHolder.OnUserClick;
            viewHolder.Contest.Click -= viewHolder.OnContestClick;
            viewHolder.Media.Click -= viewHolder.OnMediaClick;
            viewHolder.AddVote.Click -= viewHolder.OnAddVoteClick;
            viewHolder.Comment.Click -= viewHolder.OnCommentClick;
            viewHolder.Share.Click -= viewHolder.OnShareClick;
            viewHolder.Partecipate.Click -= viewHolder.OnPartecipateClick;
            viewHolder.Description.Text = null;
        }
    }
}
