﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Views.InputMethods;
using Android.Widget;
using DynamoDBTest.Controllers;
using DynamoDBTest.Models;
using DynamoDBTest.Services;

namespace DynamoDBTest.Droid
{
    [Activity(Label = "POIApplication")]
    public class ConfirmPassResetActivity : BaseActivity
    {
        TextView confirm;
        TextView sendNewEmail;
        protected override int LayoutResource => Resource.Layout.activity_confirm_code;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            Window.SetSoftInputMode(SoftInput.AdjustPan);
            confirm = FindViewById<TextView>(Resource.Id.confirm);
            sendNewEmail = FindViewById<TextView>(Resource.Id.resend);
            EditText code = FindViewById<EditText>(Resource.Id.confirmCode);
            String email = Intent.GetStringExtra("email");
            TextView message = FindViewById<TextView>(Resource.Id.confirmCodeEmail);
            message.Append(email);

            confirm.Click += (object sender, EventArgs e) =>
            {
                confirm.Enabled = false;


                ConfirmController controller = new ConfirmController();
                controller.ConfirmationTerminated += new ConfirmController.ConfirmationTerminatedHandler(OnConfirmationTerminated);
                controller.CheckCodeCommand.Execute(code.Text);

            };

            sendNewEmail.Click += (object sender, EventArgs e) =>
            {
                sendNewEmail.Enabled = false;
                ConfirmController controller = new ConfirmController();
                controller.SendNewCodeForResetCommand.Execute(email);
                Android.Widget.Toast.MakeText(this, "È stata inviata una email con un nuovo codice", Android.Widget.ToastLength.Long).Show();
                sendNewEmail.Enabled = true;
            };
        }

        private void OnConfirmationTerminated(ConfirmController controller, bool confirmationSuccessfull, string message)
        {
 
            if (confirmationSuccessfull)
            {
                confirm.Enabled = false;
                Intent newIntent = new Intent(this, typeof(ChangePassActivity));
                StartActivity(newIntent);
                OverridePendingTransition(Resource.Animator.EnterRightToLeft, Resource.Animator.ExitRightToLeft);
                Finish();
            }
            else
            {
                Android.Widget.Toast.MakeText(this, message, Android.Widget.ToastLength.Long).Show();
                confirm.Enabled = true;
            }
        }

        public override bool OnKeyDown(Keycode keycode, KeyEvent e)
        {
            if (keycode == Keycode.Back)
            {
                Finish();
                OverridePendingTransition(Resource.Animator.Still, Resource.Animator.ExitTopToBottom);
                return true;
            }
            return base.OnKeyDown(keycode, e);
        }

        public override void OnBackPressed()
        {
            Finish();
            OverridePendingTransition(Resource.Animator.Still, Resource.Animator.ExitTopToBottom);
        }

        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            switch (item.ItemId)
            {
                case Android.Resource.Id.Home:
                    View view = CurrentFocus;
                    if (view != null)
                    {
                        InputMethodManager imm = (InputMethodManager)GetSystemService(InputMethodService);
                        imm.HideSoftInputFromWindow(view.WindowToken, 0);
                    }
                    Finish();
                    OverridePendingTransition(Resource.Animator.Still, Resource.Animator.ExitTopToBottom);
                    return true;

                default:
                    return base.OnOptionsItemSelected(item);
            }
        }


    }


}