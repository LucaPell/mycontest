﻿using System;
using System.Collections.Generic;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Views;
using Android.Widget;
using Android.Views.Animations;

using DynamoDBTest.Helpers;
using DynamoDBTest.Controllers;
using Android.Support.V7.Widget;
using Android.Support.V7.App;

namespace DynamoDBTest.Droid
{
    [Activity(Label = "LoginActivity")]
    public class LoginActivity : AppCompatActivity
    {
        Button login;
        Button signup;
        TextView passRecovery;
        FrameLayout progress;
        AlphaAnimation inAnimation;
        AlphaAnimation outAnimation;

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            SetContentView(Resource.Layout.activity_login);

            login = FindViewById<Button>(Resource.Id.login);
            signup = FindViewById<Button>(Resource.Id.signupButton);
            passRecovery = FindViewById<TextView>(Resource.Id.passRecovery);
            EditText email = FindViewById<EditText>(Resource.Id.loginEmail);
            EditText password = FindViewById<EditText>(Resource.Id.loginPassword);
            progress = FindViewById<FrameLayout>(Resource.Id.progressLoginHolder);

            inAnimation = new AlphaAnimation(0f, 1f);
            inAnimation.Duration = 200;
            outAnimation = new AlphaAnimation(1f, 0f);
            outAnimation.Duration = 200;

            LoginController controller = LoginController.GetInstance();

            //if (Settings.Get("automatic_login").Equals("true"))
            //{
            //    saveCredsSwitch.Checked = true;
            //}
            //else
            //{
            //    saveCredsSwitch.Checked = false;
            //}

            login.Click += (object sender, EventArgs e) =>
            {

                Validator validator = new Validator(email.Text.ToLower().Trim(), password.Text);
                if (validator.ValidateLogin())
                {
                    controller.LoginTerminated += new LoginController.LoginTerminatedHandler(OnLoginTerminated);
                    controller.RetryLoginCommand.OperationCompleted += OnLoginError;
                    login.Enabled = false;
                    controller = LoginController.GetInstance();
                    controller.SetCredentials(email.Text.ToLower().Trim(), password.Text/*, saveCredsSwitch.Checked*/);
                    controller.RetryLoginCommand.Execute(null);
                    progress.Animation = inAnimation;
                    progress.Visibility = Android.Views.ViewStates.Visible;
                    UpdateSettings(true);
                }
                else
                    foreach (string message in validator.ErrorMessages)
                    {
                        Android.Widget.Toast.MakeText(this, message, Android.Widget.ToastLength.Short).Show();
                    }
            };

            signup.Click += (object sender, EventArgs e) =>
            {
                signup.Enabled = false;
                var newIntent = new Intent(this, typeof(SignUpActivity));                
                StartActivity(newIntent);
                OverridePendingTransition(Resource.Animator.EnterRightToLeft, Resource.Animator.ExitRightToLeft);
                signup.Enabled = true;
            };

            passRecovery.Click += (object sender, EventArgs e) =>
            {
                passRecovery.Enabled = false;
                var newIntent = new Intent(this, typeof(SendEmailForPasswordResetActivity));
                StartActivity(newIntent);
                OverridePendingTransition(Resource.Animator.EnterBottomToTop, Resource.Animator.Still);
                passRecovery.Enabled = true;
            };
        }

        private void UpdateSettings(bool saveCreds)
        {
            string setting = Settings.Get("automatic_login");
            if (setting.Equals("true") && !saveCreds)
            {
                Settings.Set("automatic_login", "false");
            }
            else
            {
                if (setting.Equals("false") && saveCreds)
                {
                    Settings.Set("automatic_login", "true");
                }
                else
                {
                    if (setting.Equals(""))
                    {
                        if (saveCreds)
                        {
                            Settings.Set("automatic_login", "true");
                        }
                        else
                        {
                            Settings.Set("automatic_login", "false");
                        }
                    }
                }
            }
        }

        private void OnLoginTerminated(LoginController controller, int result, string message, string email)
        {
            this.RunOnUiThread(() =>
            {
                if (message != null)
                {
                  Toast.MakeText(this, message, ToastLength.Long).Show();
                }
                progress.Animation = outAnimation;
                progress.Visibility = Android.Views.ViewStates.Gone;
                Intent newIntent;
                switch (result)
                {
                    case LoginController.LOGIN_SUCCESSFULL:
                        login.Enabled = true;
                        newIntent = new Intent(this, typeof(MainActivity));
                        StartActivity(newIntent);
                        this.Finish();
                        break;
                    case LoginController.EMAIL_NOT_CONFIRMED:
                        login.Enabled = true;
                        newIntent = new Intent(this, typeof(ConfirmCodeActivity));
                        newIntent.PutExtra("email", email);
                        StartActivity(newIntent);
                        break;
                    case LoginController.BAD_CREDENTIALS:
                        login.Enabled = true;
                        break;
                    default:
                        login.Enabled = true;
                        break;
                }
            });
        }

        private void OnLoginError(object sender, EventArgs a)
        {
            CommandCompletedEventArgs args = a as CommandCompletedEventArgs;
            if(args.Status == CommandStates.MaxRetryReached)
            {
                this.RunOnUiThread(() =>
                {
                    Android.Widget.Toast.MakeText(this, "Connessione ad Internet assente", Android.Widget.ToastLength.Short).Show();
                    progress.Animation = outAnimation;
                    progress.Visibility = Android.Views.ViewStates.Gone;
                    login.Enabled = true;
                });
            }
        }

        public override bool OnKeyDown(Keycode keycode, KeyEvent e)
        {
            if (keycode == Keycode.Back)
            {
                this.Finish();
                return true;
            }
            return base.OnKeyDown(keycode, e);
        }

       

    }
}