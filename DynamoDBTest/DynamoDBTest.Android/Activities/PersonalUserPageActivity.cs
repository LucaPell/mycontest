﻿using System;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Widget;
using Android.Support.V4.App;
using Android.Support.V4.View;
using Android.Support.Design.Widget;
using DynamoDBTest.ViewModels;
using DynamoDBTest.Controllers;
using Refractored.Controls;
using DynamoDBTest.Models;
using Com.Theartofdev.Edmodo.Cropper;
using Android;
using Android.Runtime;
using Android.Content.PM;
using Uri = Android.Net.Uri;
using System.Linq;
using Com.Bumptech.Glide;
using Com.Bumptech.Glide.Request;
using Android.Graphics;
using Com.Bumptech.Glide.Request.Target;
using Android.Views.Animations;
using Android.Views;
using DynamoDBTest.Helpers;

namespace DynamoDBTest.Droid
{
    [Activity(Label = "PersonalUserPageActivity")]
    public class PersonalUserPageActivity : BaseActivity
    {
        private const int BIO_RESULT_CODE = 3598;
        private const int NAME_RESULT_CODE = 3597;
        protected override int LayoutResource => Resource.Layout.activity_personal_userpage;
        private Uri _cropImageUri;
        private string shape;
        private int bannerWidth;
        private int bannerHeight;
        UserViewModel viewModel;
        TextView username;
        TextView usernameToolbar;
        TextView bio;
        TextView follows;
        TextView followers;
        CircleImageView toolbarprofileavatar;
        CircleImageView profileavatar;
        ImageButton profilebanner;
        public AppBarLayout appbar;
        ViewPager pager;
        PersonalUserTabsAdapter adapter;
        Button modifyButton;
        TabLayout tabs;
        User user;
        private int profilebannerHeight;
        private int screenHeight;
        private int screenWidth;
        private int newProfilebannerHeight;
        private string banner;
        private string resultText;
        public AlphaAnimation inAnimation;
        public AlphaAnimation outAnimation;
        public FrameLayout ProgressB;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            user = LoginController.GetInstance().CurrentUser;
            Intent.PutExtra("userId", user.Email);
            base.OnCreate(savedInstanceState);
            viewModel = new UserViewModel();
            viewModel.UserLoaded += new UserViewModel.UserLoadedHandler(OnUserLoaded);
            viewModel.LoadUserCommand.Execute(user.Email);
            username = FindViewById<TextView>(Resource.Id.profilename);
            usernameToolbar = FindViewById<TextView>(Resource.Id.usernamePage);
            follows = FindViewById<TextView>(Resource.Id.followingNumber);
            followers = FindViewById<TextView>(Resource.Id.followsNumber);
            follows.Text = (Math.Max(user.FollowsUsers.Count-1,0)).ToString();
            followers.Text = user.FollowedByUsers.Count.ToString();
            username.Text = user.Username;
            usernameToolbar.Text = user.Username;
            modifyButton = FindViewById<Button>(Resource.Id.modifyButton);
            adapter = new PersonalUserTabsAdapter(this, SupportFragmentManager);
            pager = FindViewById<ViewPager>(Resource.Id.user_viewpager);
            tabs = FindViewById<TabLayout>(Resource.Id.tabs);
            pager.Adapter = adapter;
            tabs.SetupWithViewPager(pager);
            tabs.AddOnTabSelectedListener(adapter);
            pager.OffscreenPageLimit = 5;
            toolbarprofileavatar = FindViewById<CircleImageView>(Resource.Id.toolbar_profile_image);
            profileavatar = FindViewById<CircleImageView>(Resource.Id.profileavatar);
            profilebanner = FindViewById<ImageButton>(Resource.Id.banner);
            bio = FindViewById<TextView>(Resource.Id.biotext);
            appbar = FindViewById<AppBarLayout>(Resource.Id.appbar);

            inAnimation = new AlphaAnimation(0f, 1f);
            inAnimation.Duration = 200;
            outAnimation = new AlphaAnimation(1f, 0f);
            outAnimation.Duration = 200;
            ProgressB = FindViewById<FrameLayout>(Resource.Id.progress);

            string avatar = user.Avatar;
            banner = user.Banner;

            if (avatar != null)
            {
                Glide.With(this).Load(avatar).Into(profileavatar);
            }
            if (banner != null)
            {
                //necessario a causa del resizing dinamico e del fatto che glide fa magie random taglia e cuci con la grandezza dell'immagine se l'imageview e' piccola 
                //causando un casino se si allarga l'imageview in un secondo momento
                RequestOptions options = new RequestOptions().Override(Target.SizeOriginal, Target.SizeOriginal);
                Glide.With(this).Load(banner).Apply(options).Into(profilebanner);
            }

            bio.Text = user.Bio;
            if (bio.Text == null || String.IsNullOrWhiteSpace(bio.Text))
            {
                bio.Text = "Clicca qui per scrivere qualcosa su di te.";
            }

            pager.PageSelected += (sender, args) =>
            {
                adapter.currentposition = args.Position;
                var fragment = adapter.InstantiateItem(pager, args.Position) as IFragmentVisible;

                fragment?.BecameVisible();
            };


            profileavatar.Click += (object sender, EventArgs e) =>
            {
                var builder = new AlertDialog.Builder(this);
                String[] options;

                if (user.Avatar != null)
                {
                    options = new[] { "Cambia immagine", "Rimuovi Immagine" };
                }
                else
                {
                    options = new[] { "Aggiungi immagine" };
                }
                builder.SetItems(options, new EventHandler<DialogClickEventArgs>(
                (s, args) =>
                {
                    if (args.Which == 0)
                    {
                        shape = "CIRCLE";
                        if (CropImage.IsExplicitCameraPermissionRequired(this))
                        {
                            RequestPermissions(new[] { Manifest.Permission.Camera }, CropImage.CameraCapturePermissionsRequestCode);
                        }
                        else
                        {
                            CropImage.StartPickImageActivity(this);
                        }

                    }
                    else
                    {
                        ProgressB.Animation = inAnimation;
                        ProgressB.Visibility = ViewStates.Visible;
                        viewModel.RetryDeleteAvatarCommand.OperationCompleted += OnAvatarDeleteCompleted;
                        viewModel.RetryDeleteAvatarCommand.Execute();
                    };
                }));
                builder.Create().Show();
            };

            profilebanner.Click += (object sender, EventArgs e) =>
            {
                var builder = new AlertDialog.Builder(this);
                String[] options;

                if (user.Banner != null)
                {
                    options = new[] { "Cambia immagine", "Rimuovi Immagine" };
                }
                else
                {
                    options = new[] { "Aggiungi immagine" };
                }
                builder.SetItems(options, new EventHandler<DialogClickEventArgs>(
                (s, args) =>
                {
                    if (args.Which == 0)
                    {
                        shape = "BANNER";
                        bannerHeight = profilebanner.Height;
                        bannerWidth = profilebanner.Width;
                        if (CropImage.IsExplicitCameraPermissionRequired(this))
                        {
                            RequestPermissions(new[] { Manifest.Permission.Camera }, CropImage.CameraCapturePermissionsRequestCode);
                        }
                        else
                        {
                            CropImage.StartPickImageActivity(this);
                        }
                    }
                    else
                    {
                        ProgressB.Animation = inAnimation;
                        ProgressB.Visibility = ViewStates.Visible;
                        viewModel.RetryDeleteBannerCommand.OperationCompleted += OnBannerDeleteCompleted;
                        viewModel.RetryDeleteBannerCommand.Execute();
                    };
                }));
                builder.Create().Show();
            };


            bio.Click += (object sender, EventArgs e) =>
            {
                if ((bio.Text != "Clicca qui per scrivere qualcosa su di te.") && !String.IsNullOrWhiteSpace(bio.Text))
                {
                    var builder = new AlertDialog.Builder(this);
                    String[] options;

                    options = new[] { "Modifica descrizione", "Rimuovi descrizione" };
                    builder.SetItems(options, new EventHandler<DialogClickEventArgs>(
                    (s, args) =>
                    {
                        if (args.Which == 0)
                        {
                            var newIntent = new Intent(this, typeof(ModifyTextActivity));
                            if(bio.Text != "Clicca qui per scrivere qualcosa su di te.")
                            {
                                newIntent.PutExtra("text", bio.Text);
                            }
                            StartActivityForResult(newIntent, BIO_RESULT_CODE);
                            OverridePendingTransition(Resource.Animator.EnterBottomToTop, Resource.Animator.Still);
                        }
                        else
                        {
                            ProgressB.Animation = inAnimation;
                            ProgressB.Visibility = ViewStates.Visible;
                            viewModel.RetryDeleteBioCommand.OperationCompleted += OnBioDeleteCompleted;
                            viewModel.RetryDeleteBioCommand.Execute(user.Email);
                        };
                    }));
                    builder.Create().Show();
                }
                else
                {
                    var newIntent = new Intent(this, typeof(ModifyTextActivity));
                    StartActivityForResult(newIntent, BIO_RESULT_CODE);
                    OverridePendingTransition(Resource.Animator.EnterBottomToTop, Resource.Animator.Still);
                }
            };

            if (Resources.Configuration.Orientation == Android.Content.Res.Orientation.Landscape)
            {
                profilebanner.Post(() =>
                {
                    profilebannerHeight = profilebanner.Height;
                    screenHeight = MediaHelper.GetInstance().ScreenHeight();
                    screenWidth = MediaHelper.GetInstance().ScreenWidth();
                    newProfilebannerHeight = screenHeight * profilebannerHeight / screenWidth;
                    profilebanner.LayoutParameters.Height = newProfilebannerHeight;
                });
            }

            Toolbar.MenuItemClick += (sender, e) =>
            {
                if (e.Item.ItemId == Resource.Id.logout)
                {
                    var intent = new Intent(this, typeof(LoginActivity));
                    FinishAffinity();
                    LoginController.GetInstance().Logout(); 
                    StartActivity(intent);
                }
            };

        }

        public override bool OnCreateOptionsMenu(IMenu menu)
        {
            MenuInflater.Inflate(Resource.Menu.top_menus, menu);
            return base.OnCreateOptionsMenu(menu);
        }

        private void OnUserLoaded(bool loaded)
        {

        }

        public override AppBarLayout GetAppbar()
        {
            return appbar;
        }

        //se si vuole fare partire l'image picker nella schermata attuale prima che il cropper parta purtroppo si deve scrivere questi due muretti di codice
        protected override void OnActivityResult(int requestCode, [GeneratedEnum] Result resultCode, Intent data)
        {
            base.OnActivityResult(requestCode, resultCode, data);

            if (requestCode == CropImage.PickImageChooserRequestCode && resultCode == Result.Ok)
            {
                var imageUri = CropImage.GetPickImageResultUri(this, data);

                // For API >= 23 we need to check specifically that we have permissions to read external storage,
                // but we don't know if we need to for the URI so the simplest is to try open the stream and see if we get error.
                var requirePermissions = false;
                if (CropImage.IsReadExternalStoragePermissionsRequired(this, imageUri))
                {

                    // request permissions and handle the result in onRequestPermissionsResult()
                    requirePermissions = true;
                    _cropImageUri = imageUri;
                    RequestPermissions(new[] { Manifest.Permission.ReadExternalStorage }, CropImage.PickImagePermissionsRequestCode);
                }
                else
                {
                    Intent intent = new Intent(this, typeof(CropActivity));
                    intent.PutExtra("URI", imageUri.ToString());
                    intent.PutExtra("SHAPE", shape);
                    if (shape.Equals("BANNER"))
                    {
                        intent.PutExtra("WIDTH", bannerWidth);
                        intent.PutExtra("HEIGHT", bannerHeight);
                    }
                    StartActivity(intent);
                }
            }
            else if (requestCode == BIO_RESULT_CODE && resultCode == Result.Ok)
            {
                string newText = data.GetStringExtra("text");
                ProgressB.Animation = inAnimation;
                ProgressB.Visibility = ViewStates.Visible;
                if (String.IsNullOrWhiteSpace(newText))
                {
                    viewModel.RetryDeleteBioCommand.OperationCompleted += OnBioDeleteCompleted;
                    viewModel.RetryDeleteBioCommand.Execute(user.Email);
                }
                else
                {
                    viewModel.RetryUpdateBioCommand.OperationCompleted += OnBioUpdateCompleted;
                    var stringwrapper = new StringWrapper(user.Email, newText);
                    viewModel.RetryUpdateBioCommand.Execute(stringwrapper);
                    resultText = newText;
                }
            }
            else if (requestCode == NAME_RESULT_CODE && resultCode == Result.Ok)
            {
                string newText = data.GetStringExtra("text");
                ProgressB.Animation = inAnimation;
                ProgressB.Visibility = ViewStates.Visible;
                if (!String.IsNullOrWhiteSpace(newText))
                {
                    viewModel.RetryUpdateBioCommand.OperationCompleted += OnNameUpdateCompleted;
                    var stringwrapper = new StringWrapper(user.Email, newText);
                    viewModel.RetryUpdateBioCommand.Execute(stringwrapper);
                    resultText = newText;
                }
            }
        }

        public override void OnRequestPermissionsResult(int requestCode, string[] permissions, [GeneratedEnum] Permission[] grantResults)
        {
            if (requestCode == CropImage.CameraCapturePermissionsRequestCode)
            {
                if (grantResults.Length > 0 && grantResults[0] == Permission.Granted)
                {
                    CropImage.StartPickImageActivity(this);
                }
                else
                {
                    Toast.MakeText(this, "Cancelling, required permissions are not granted", ToastLength.Long).Show();
                }
            }
            else
            if (requestCode == CropImage.PickImagePermissionsRequestCode)
            {
                if (_cropImageUri != null && grantResults.Length > 0 && grantResults[0] == Permission.Granted)
                {
                    Intent intent = new Intent(this, typeof(CropActivity));
                    intent.PutExtra("URI", _cropImageUri.ToString());
                    intent.PutExtra("SHAPE", shape);
                    if (shape.Equals("BANNER"))
                    {
                        intent.PutExtra("WIDTH", bannerWidth);
                        intent.PutExtra("HEIGHT", bannerHeight);
                    }
                    StartActivity(intent);
                }
                else
                {
                    Toast.MakeText(this, "Cancelling, required permissions are not granted", ToastLength.Long).Show();
                }
            }
        }

        protected override void OnResume()
        {
            if (MediaHelper.GetInstance().HasAvatar())
            {
                profileavatar.SetImageBitmap(MediaHelper.GetInstance().GetAvatar());
            }

            if (MediaHelper.GetInstance().HasBanner())
            {
                profilebanner.SetImageBitmap(MediaHelper.GetInstance().GetBanner());
            }
            base.OnResume();
        }

        protected override void OnSaveInstanceState(Bundle outState)
        {
            outState.PutString("shape", shape);
            outState.PutInt("WIDTH", bannerWidth);
            outState.PutInt("HEIGHT", bannerHeight);
            base.OnSaveInstanceState(outState);
        }

        protected override void OnRestoreInstanceState(Bundle savedInstanceState)
        {
            shape = savedInstanceState.GetString("shape");
            bannerWidth = savedInstanceState.GetInt("WIDTH");
            bannerHeight = savedInstanceState.GetInt("HEIGHT");
            base.OnRestoreInstanceState(savedInstanceState);
        }

        private void OnAvatarDeleteCompleted(object sender, EventArgs args)
        {
            RunOnUiThread(() =>
            {
                ProgressB.Animation = outAnimation;
                ProgressB.Visibility = ViewStates.Gone;

                CommandCompletedEventArgs commandArgs = args as CommandCompletedEventArgs;
                if (commandArgs.Status == CommandStates.Completed)
                {
                    profileavatar.SetImageResource(Resource.Color.imageBlank);
                    MediaHelper.GetInstance().SetAvatar(null);
                    LoginController.GetInstance().CurrentUser.DeleteHashAvatar = null;
                    LoginController.GetInstance().CurrentUser.Avatar = null;
                }
                else
                {
                    Toast.MakeText(this, "Non c'è connessione, riprova", Android.Widget.ToastLength.Short).Show();
                }
            });
        }

        private void OnBannerDeleteCompleted(object sender, EventArgs args)
        {
            RunOnUiThread(() =>
            {
                ProgressB.Animation = outAnimation;
                ProgressB.Visibility = ViewStates.Gone;

                CommandCompletedEventArgs commandArgs = args as CommandCompletedEventArgs;
                if (commandArgs.Status == CommandStates.Completed)
                {
                    profilebanner.SetImageResource(Resource.Color.imageBlank);
                    MediaHelper.GetInstance().SetBanner(null);
                    LoginController.GetInstance().CurrentUser.DeleteHashBanner = null;
                    LoginController.GetInstance().CurrentUser.Banner = null;
                }
                else
                {
                    Toast.MakeText(this, "Non c'è connessione, riprova", Android.Widget.ToastLength.Short).Show();
                }
            });
        }



        private void OnBioDeleteCompleted(object sender, EventArgs args)
        {
            RunOnUiThread(() => {
                ProgressB.Animation = outAnimation;
                ProgressB.Visibility = ViewStates.Gone;

                CommandCompletedEventArgs commandArgs = args as CommandCompletedEventArgs;
                if (commandArgs.Status == CommandStates.Completed)
                {
                    bio.Text = "Clicca qui per scrivere qualcosa su di te.";
                }
                else
                {
                    Toast.MakeText(this, "Non c'è connessione, riprova", Android.Widget.ToastLength.Short).Show();
                }
            });
        }

        private void OnBioUpdateCompleted(object sender, EventArgs args)
        {
            RunOnUiThread(() => {
                ProgressB.Animation = outAnimation;
                ProgressB.Visibility = ViewStates.Gone;

                CommandCompletedEventArgs commandArgs = args as CommandCompletedEventArgs;
                if (commandArgs.Status == CommandStates.Completed)
                {
                    bio.Text = resultText;
                }
                else
                {
                    Toast.MakeText(this, "Non c'è connessione, riprova", Android.Widget.ToastLength.Short).Show();
                }
            });
        }

        private void OnNameUpdateCompleted(object sender, EventArgs args)
        {
            RunOnUiThread(() => {
                ProgressB.Animation = outAnimation;
                ProgressB.Visibility = ViewStates.Gone;

                CommandCompletedEventArgs commandArgs = args as CommandCompletedEventArgs;
                if (commandArgs.Status == CommandStates.Completed)
                {
                    username.Text = resultText;
                }
                else
                {
                    Toast.MakeText(this, "Non c'è connessione, riprova", Android.Widget.ToastLength.Short).Show();
                }
            });
        }

    }



    class PersonalUserTabsAdapter : FragmentStatePagerAdapter, TabLayout.IOnTabSelectedListener
    {
        string[] titles;

        public override int Count => titles.Length;
        public int currentposition = 0;
        UserPartecipationsFragment partecipationsF;
        UserContestsFragment contestF;
        //GlobalContestFragment globalF;
        //MessagesFragment messagesF;

        public PersonalUserTabsAdapter(Context context, Android.Support.V4.App.FragmentManager fm) : base(fm)
        {
            titles = context.Resources.GetTextArray(Resource.Array.user_sections);
        }

        public override Java.Lang.ICharSequence GetPageTitleFormatted(int position) =>
                            new Java.Lang.String(titles[position]);

        public override Android.Support.V4.App.Fragment GetItem(int position)
        {
            if (position == 0) { partecipationsF = UserPartecipationsFragment.NewInstance(); return partecipationsF; }
            else if (position == 1) { contestF = UserContestsFragment.NewInstance(); return contestF; }
            //else if (position == 2) { globalF = GlobalContestFragment.NewInstance(); return globalF; }
            //else if (position == 3) { messagesF = MessagesFragment.NewInstance(); return messagesF; }
            else { return null; }
        }

        public override int GetItemPosition(Java.Lang.Object frag) => PositionNone;

        public void OnTabReselected(TabLayout.Tab tab)
        {
            if (currentposition == 0)
            {
                if (partecipationsF!= null)
                {
                    partecipationsF.recyclerView.SmoothScrollToPosition(0);
                }
            }
            else if (currentposition == 1)
            {
                if (contestF != null)
                {
                    contestF.recyclerView.SmoothScrollToPosition(0);
                }
            }
        }

        public void OnTabSelected(TabLayout.Tab tab)
        {
        }

        public void OnTabUnselected(TabLayout.Tab tab)
        {
        }
    }
}