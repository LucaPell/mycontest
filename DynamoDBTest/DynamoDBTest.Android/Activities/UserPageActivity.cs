﻿using System;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Views;
using Android.Widget;
using Android.Support.V4.App;
using Android.Support.V4.View;
using Android.Support.Design.Widget;
using DynamoDBTest.Services;
using DynamoDBTest.ViewModels;
using DynamoDBTest.Helpers;
using DynamoDBTest.Controllers;
using Refractored.Controls;
using System.Linq;
using Android.Graphics;
using Android.Support.V4.Content;
using Com.Bumptech.Glide.Request;
using Com.Bumptech.Glide;
using Com.Bumptech.Glide.Request.Target;
using Android.Views.Animations;

namespace DynamoDBTest.Droid
{
    [Activity(Label = "UserPageActivity")]
    public class UserPageActivity : BaseActivity
    {
        protected override int LayoutResource => Resource.Layout.activity_userpage;
        UserViewModel viewModel;
        TextView username;
        TextView usernameToolbar;
        TextView bio;
        TextView follows;
        TextView followers;
        CircleImageView toolbarprofileavatar;
        CircleImageView profileavatar;
        ImageButton profilebanner;
        public AppBarLayout appbar;
        ViewPager pager;
        UserTabsAdapter adapter;
        Button followButton1;
        Button followButton2;
        TabLayout tabs;
        private int profilebannerHeight;
        private int screenHeight;
        private int screenWidth;
        private int newProfilebannerHeight;
        string avatar;
        public AlphaAnimation inAnimation;
        public AlphaAnimation outAnimation;
        public FrameLayout ProgressB;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            viewModel = new UserViewModel();
            viewModel.UserLoaded += new UserViewModel.UserLoadedHandler(OnUserLoaded);
            viewModel.LoadUserCommand.Execute(Intent.GetStringExtra("userId"));
            username = FindViewById<TextView>(Resource.Id.profilename);
            usernameToolbar = FindViewById<TextView>(Resource.Id.usernamePage);
            username.Text = Intent.GetStringExtra("username");
            usernameToolbar.Text = Intent.GetStringExtra("username");
            follows = FindViewById<TextView>(Resource.Id.followingNumber);
            followers = FindViewById<TextView>(Resource.Id.followsNumber);
            followButton1 = FindViewById<Button>(Resource.Id.followButton1);
            followButton2 = FindViewById<Button>(Resource.Id.followButton2);
            adapter = new UserTabsAdapter(this, SupportFragmentManager);
            pager = FindViewById<ViewPager>(Resource.Id.user_viewpager);
            tabs = FindViewById<TabLayout>(Resource.Id.tabs);
            pager.Adapter = adapter;
            tabs.SetupWithViewPager(pager);
            tabs.AddOnTabSelectedListener(adapter);
            pager.OffscreenPageLimit = 5;
            toolbarprofileavatar = FindViewById<CircleImageView>(Resource.Id.toolbar_profile_image);
            profileavatar = FindViewById<CircleImageView>(Resource.Id.profileavatar);
            profilebanner = FindViewById<ImageButton>(Resource.Id.banner);
            bio = FindViewById<TextView>(Resource.Id.biotext);
            appbar = FindViewById<AppBarLayout>(Resource.Id.appbar);

            inAnimation = new AlphaAnimation(0f, 1f);
            inAnimation.Duration = 200;
            outAnimation = new AlphaAnimation(1f, 0f);
            outAnimation.Duration = 200;
            ProgressB = FindViewById<FrameLayout>(Resource.Id.progress);

            avatar = Intent.GetStringExtra("avatar");
            if (avatar != null)
            {
                Glide.With(this).Load(avatar).Into(profileavatar);
            }

            pager.PageSelected += (sender, args) =>
            {
                adapter.currentposition = args.Position;
                var fragment = adapter.InstantiateItem(pager, args.Position) as IFragmentVisible;
                fragment?.BecameVisible();
            };

            bio.Text = Intent.GetStringExtra("bio");
            if (bio.Text != null && !bio.Text.ToString().All(c => c.Equals(" "))) { 
               
                bio.Visibility = ViewStates.Visible;
            }


            if (LoginController.GetInstance().CurrentUser.FollowsUsers.ContainsKey(Intent.GetStringExtra("userId")))
            {
                followButton1.Text = "Following";
                followButton1.SetTextColor(Color.White);
                followButton1.Activated = true;
            }
            else
            {
                followButton1.Text = "Follow";
                followButton1.SetTextColor(ContextCompat.GetColorStateList(this, Resource.Color.mediumFont));
                followButton1.Activated = false;
            }

            followButton1.Click += (object sender, EventArgs e) =>
            {
                followButton1.Enabled = false;
                if (followButton1.Activated)
                {
                    UserService service = new UserService();

                    service.SetUser(LoginController.GetInstance().CurrentUser);
                    service.RemoveFollowsUserCommand.Execute(viewModel.LoadedUser.Email);

                    service.SetUser(viewModel.LoadedUser);
                    service.RemoveFollowedByUserCommand.Execute(LoginController.GetInstance().CurrentUser.Email);

                    followButton1.Text = "Follow";
                    followButton1.SetTextColor(ContextCompat.GetColorStateList(this, Resource.Color.mediumFont));
                    followButton1.Activated = false;
                    int newcount = Math.Max(viewModel.LoadedUser.FollowedByUsers.Count() - 1, 0);
                    followers.Text = (newcount).ToString();

                }
                else
                {
                    UserService service = new UserService();

                    service.SetUser(LoginController.GetInstance().CurrentUser);
                    service.AddFollowsUserCommand.Execute(viewModel.LoadedUser);

                    service.SetUser(viewModel.LoadedUser);
                    service.AddFollowedByUserCommand.Execute(LoginController.GetInstance().CurrentUser);

                    followButton1.Text = "Following";
                    followButton1.SetTextColor(Color.White);
                    followButton1.Activated = true;
                    int newcount = Math.Max(viewModel.LoadedUser.FollowedByUsers.Count() - 1, 0);
                    followers.Text = (newcount).ToString();
                }
                followButton1.Enabled = true;
            };
            profilebanner.SetScaleType(ImageView.ScaleType.CenterCrop);
            if (Resources.Configuration.Orientation == Android.Content.Res.Orientation.Landscape)
            {
                profilebanner.Post(() =>
                {
                    profilebannerHeight = profilebanner.Height;
                    screenHeight = MediaHelper.GetInstance().ScreenHeight();
                    screenWidth = MediaHelper.GetInstance().ScreenWidth();
                    newProfilebannerHeight = screenHeight * profilebannerHeight / screenWidth;
                    profilebanner.LayoutParameters.Height = newProfilebannerHeight;

                });
            }
        }

        private void OnUserLoaded(bool loaded)
        {
            followers.Text= (viewModel.LoadedUser.FollowedByUsers.Count()-1).ToString();

            follows.Text = viewModel.LoadedUser.FollowsUsers.Count().ToString();
            string banner = viewModel.LoadedUser.Banner;
            if (banner != null)
            {
                RequestOptions options = new RequestOptions().Override(Target.SizeOriginal, Target.SizeOriginal);
                Glide.With(this).Load(banner).Apply(options).Into(profilebanner);
            }
            bio.Text = viewModel.LoadedUser.Bio;
        }

        protected override void OnStart()
        {
            base.OnStart();

        }

        public override AppBarLayout GetAppbar()
        {
            return appbar;
        }



    }



    class UserTabsAdapter : FragmentStatePagerAdapter, TabLayout.IOnTabSelectedListener
    {
        string[] titles;

        public override int Count => titles.Length;
        public int currentposition = 0;
        UserPartecipationsFragment partecipationsF;
        UserContestsFragment contestF;
        //GlobalContestFragment globalF;
        //MessagesFragment messagesF;

        public UserTabsAdapter(Context context, Android.Support.V4.App.FragmentManager fm) : base(fm)
        {
            titles = context.Resources.GetTextArray(Resource.Array.user_sections);
        }

        public override Java.Lang.ICharSequence GetPageTitleFormatted(int position) =>
                            new Java.Lang.String(titles[position]);

        public override Android.Support.V4.App.Fragment GetItem(int position)
        {
            if (position == 0) { partecipationsF = UserPartecipationsFragment.NewInstance(); return partecipationsF; }
            else if (position == 1) { contestF = UserContestsFragment.NewInstance(); return contestF; }
            //else if (position == 2) { globalF = GlobalContestFragment.NewInstance(); return globalF; }
            //else if (position == 3) { messagesF = MessagesFragment.NewInstance(); return messagesF; }
            else { return null; }
        }

        public override int GetItemPosition(Java.Lang.Object frag) => PositionNone;

        public void OnTabReselected(TabLayout.Tab tab)
        {
            if (currentposition == 0)
            {
                if (partecipationsF != null)
                {
                    partecipationsF.recyclerView.SmoothScrollToPosition(0);
                }
            }
            else if (currentposition == 1)
            {
                if (contestF != null)
                {
                    contestF.recyclerView.SmoothScrollToPosition(0);
                }
            }
        }

        public void OnTabSelected(TabLayout.Tab tab)
        {
        }

        public void OnTabUnselected(TabLayout.Tab tab)
        {
        }
    }
}