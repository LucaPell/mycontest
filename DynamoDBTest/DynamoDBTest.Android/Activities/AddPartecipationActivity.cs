﻿using System;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Graphics;
using Uri = Android.Net.Uri;
using DynamoDBTest.Services;
using DynamoDBTest.Models;
using DynamoDBTest.Controllers;
using Refractored.Controls;
using Com.Theartofdev.Edmodo.Cropper;
using Android;
using Android.Content.PM;
using System.IO;
using System.Linq;
using Android.Views.Animations;
using DynamoDBTest.ViewModels;
using DynamoDBTest.Helpers;

namespace DynamoDBTest.Droid
{
    [Activity(Label = "AddPartecipationActivity")]
    public class AddPartecipationActivity : BaseActivity
    {
        protected override int LayoutResource => Resource.Layout.activity_new_partecipation;
        private const int IMAGE_RESULT_CODE = 5514;
        private Bitmap image;
        private MemoryStream memoryStream;
        private Uri _cropImageUri;
        Partecipation newPartecipation;
        ImageButton Preview;
        CircleImageView Delete;
        EditText Description;
        TextView Confirm;
        AlphaAnimation inAnimation;
        AlphaAnimation outAnimation;
        FrameLayout ProgressB;
        private string contestId;



        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            Plugin.CurrentActivity.CrossCurrentActivity.Current.Activity = this;
            Delete = FindViewById<CircleImageView>(Resource.Id.remove);
            Description = FindViewById<EditText>(Resource.Id.descText);
            Confirm = FindViewById<TextView>(Resource.Id.confirm);
            Preview = FindViewById<ImageButton>(Resource.Id.preview1);
            ProgressB = FindViewById<FrameLayout>(Resource.Id.progress);
            inAnimation = new AlphaAnimation(0f, 1f);
            inAnimation.Duration = 200;
            outAnimation = new AlphaAnimation(1f, 0f);
            outAnimation.Duration = 200;
            Contest contest = null;
            Partecipation partecipation = null;
            Share share = null;

            var data = Intent.GetStringExtra("data");
            if (data != null)
            {
                contest = Newtonsoft.Json.JsonConvert.DeserializeObject<Contest>(data);
                contestId = contest.Id;
            }
            else {
                data = Intent.GetStringExtra("partecipation");
                if (data != null)
                {
                    partecipation = Newtonsoft.Json.JsonConvert.DeserializeObject<Partecipation>(data);
                    contestId = partecipation.PartecipatingToContest;
                }
                else
                {
                    data = Intent.GetStringExtra("share");
                    if (data != null)
                    {
                        share = Newtonsoft.Json.JsonConvert.DeserializeObject<Share>(data);
                        contestId = share.ContestId;
                    }
                }
            }
        
            Preview.Click += (object sender, EventArgs args) =>
            {
                if (CropImage.IsExplicitCameraPermissionRequired(this))
                {
                    RequestPermissions(new[] { Manifest.Permission.Camera }, CropImage.CameraCapturePermissionsRequestCode);
                }
                else
                {
                    CropImage.StartPickImageActivity(this);
                }

            };

            Delete.Click += (object sender, EventArgs e) =>
            {
                image = null;
                Preview.SetImageBitmap(image);
                Delete.Visibility = ViewStates.Gone;
                MediaHelper.GetInstance().SetResult(null);
            };


            Confirm.Click += (object sender, EventArgs e) =>
            {
                Confirm.Enabled = false;
                if (!LoginController.GetInstance().CurrentUser.Partecipations.Contains(LoginController.GetInstance().CurrentUser.Email + contestId))
                {
                    if (image == null)
                    {
                        Toast.MakeText(this, "Devi inserire una immagine per poter partecipare", ToastLength.Long).Show();
                    }
                    else
                    {

                        if (String.IsNullOrWhiteSpace(Description.Text))
                        {
                            Description.Text = null;
                        }
                        if (contest != null)
                        {
                            newPartecipation = new Partecipation(Description.Text, LoginController.GetInstance().CurrentUser, contest);
                        }
                        else if (partecipation != null)
                        {
                            newPartecipation = new Partecipation(Description.Text, LoginController.GetInstance().CurrentUser, partecipation);
                        }
                        else if (share != null)
                        {
                            newPartecipation = new Partecipation(Description.Text, LoginController.GetInstance().CurrentUser, share);
                        }
                        //newPartecipation.TempMedia = new MemoryStream();
                        newPartecipation.TempMedia = memoryStream;
                        ProgressB.Animation = inAnimation;
                        ProgressB.Visibility = ViewStates.Visible;
                        PartecipationViewModel ViewModel = new PartecipationViewModel();
                        ViewModel.RetryAddPartecipationCommand.OperationCompleted += OnUploadCompleted;
                        ViewModel.RetryAddPartecipationCommand.Execute(newPartecipation);
                    }  
                }
                else
                {
                    Toast.MakeText(this, "Stai già partecipando", Android.Widget.ToastLength.Short).Show();
                }

                Confirm.Enabled = true;
            };
        }


        protected override void OnActivityResult(int requestCode, [GeneratedEnum] Result resultCode, Intent data)
        {
            base.OnActivityResult(requestCode, resultCode, data);

            if (requestCode == CropImage.PickImageChooserRequestCode && resultCode == Result.Ok)
            {
                var imageUri = CropImage.GetPickImageResultUri(this, data);

                var requirePermissions = false;
                if (CropImage.IsReadExternalStoragePermissionsRequired(this, imageUri))
                {
                    requirePermissions = true;
                    _cropImageUri = imageUri;
                    RequestPermissions(new[] { Manifest.Permission.ReadExternalStorage }, CropImage.PickImagePermissionsRequestCode);
                }
                else
                {
                    Intent intent = new Intent(this, typeof(CropActivity));
                    intent.PutExtra("URI", imageUri.ToString());
                    intent.PutExtra("SHAPE", "OTHER");
                    StartActivityForResult(intent, IMAGE_RESULT_CODE);
                }
            }
            else if (requestCode == IMAGE_RESULT_CODE && resultCode == Result.Ok)
            {
                if (image == null)
                {
                    image = MediaHelper.GetInstance().GetResult();
                }
                memoryStream = new MemoryStream();
                image.Compress(Bitmap.CompressFormat.Jpeg, 90, memoryStream);
            }
        }

        public override void OnRequestPermissionsResult(int requestCode, string[] permissions, [GeneratedEnum] Permission[] grantResults)
        {
            if (requestCode == CropImage.CameraCapturePermissionsRequestCode)
            {
                if (grantResults.Length > 0 && grantResults[0] == Permission.Granted)
                {
                    CropImage.StartPickImageActivity(this);
                }
                else
                {
                    Toast.MakeText(this, "Cancelling, required permissions are not granted", ToastLength.Long).Show();
                }
            }
            else
            if (requestCode == CropImage.PickImagePermissionsRequestCode)
            {
                if (_cropImageUri != null && grantResults.Length > 0 && grantResults[0] == Permission.Granted)
                {
                    Intent intent = new Intent(this, typeof(CropActivity));
                    intent.PutExtra("URI", _cropImageUri.ToString());
                    intent.PutExtra("SHAPE", "OTHER");
                    StartActivityForResult(intent, IMAGE_RESULT_CODE);
                }
                else
                {
                    Toast.MakeText(this, "Cancelling, required permissions are not granted", ToastLength.Long).Show();
                }
            }
        }

        protected override void OnResume()
        {
            if (MediaHelper.GetInstance().HasResult())
            {
                image = MediaHelper.GetInstance().GetResult();
            }
            if(image != null)
            {
                Preview.SetImageBitmap(image);
                Delete.Visibility = ViewStates.Visible;
            }
            base.OnResume();
        }


        private void OnUploadCompleted(object sender, EventArgs args)
        {
            RunOnUiThread(() => {
                ProgressB.Animation = outAnimation;
                ProgressB.Visibility = ViewStates.Gone;

                CommandCompletedEventArgs commandArgs = args as CommandCompletedEventArgs;
                if (commandArgs.Status == CommandStates.Completed)
                {
                    GlobalCollections.GetInstance().Add(newPartecipation);
                    LoginController.GetInstance().CurrentUser.Partecipations.Add(newPartecipation.Id);
                    Intent intent = new Intent();
                    intent.PutExtra("newPartecipationId", newPartecipation.Id);
                    if (Intent.GetStringExtra("fragment") == "OK"){
                        SetResult(0, intent);
                    }
                    else {
                        SetResult(Result.Ok, intent);
                    }
                    Finish();
                    OverridePendingTransition(Resource.Animator.Still, Resource.Animator.ExitTopToBottom);
                }
                else
                {
                    Toast.MakeText(this, "Non c'è connessione, riprova", Android.Widget.ToastLength.Short).Show();
                }
            });
        }

    }


}
