﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Text;
using Android.Views;
using Android.Views.InputMethods;
using Android.Widget;

namespace DynamoDBTest.Droid
{
    [Activity(Label = "ModifyDescActivity")]
    public class ModifyTextActivity : BaseActivity
    {
        protected override int LayoutResource => Resource.Layout.activity_modifytext;
        EditText Text;
        TextView Confirm;
        string text;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            text = Intent.GetStringExtra("text");
            Text = FindViewById<EditText>(Resource.Id.descText);
            Confirm = FindViewById<TextView>(Resource.Id.confirm);
            if(Intent.GetStringExtra("TYPE") == "Username")
            {
                Text.Hint = "Modifica il tuo username";
                int maxLengthofEditText = 4;
                Text.SetFilters(new IInputFilter[] { new InputFilterLengthFilter(25) });
            }


            if (!String.IsNullOrWhiteSpace(text))
            {
                Text.Text = text;
            }

            Confirm.Click += (object sender, EventArgs e) =>
            {
                if (text == null || Text.Text != text.Trim())
                {
                    Intent intent = new Intent();
                    intent.PutExtra("text", Text.Text);
                    SetResult(Result.Ok, intent);
                }
                Finish();

            };
        }

        public override bool OnKeyDown(Keycode keycode, KeyEvent e)
        {
            if (keycode == Keycode.Back)
            {
                Finish();
                OverridePendingTransition(Resource.Animator.Still, Resource.Animator.ExitTopToBottom);
                return true;
            }
            return base.OnKeyDown(keycode, e);
        }


        public override void OnBackPressed()
        {
            Finish();
            OverridePendingTransition(Resource.Animator.Still, Resource.Animator.ExitTopToBottom);
        }

        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            switch (item.ItemId)
            {
                case Android.Resource.Id.Home:
                    View view = CurrentFocus;
                    if (view != null)
                    {
                        InputMethodManager imm = (InputMethodManager)GetSystemService(InputMethodService);
                        imm.HideSoftInputFromWindow(view.WindowToken, 0);
                    }
                    Finish();
                    OverridePendingTransition(Resource.Animator.Still, Resource.Animator.ExitTopToBottom);
                    return true;

                default:
                    return base.OnOptionsItemSelected(item);
            }
        }
    }
}