﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Views.Animations;
using Android.Views.InputMethods;
using Android.Widget;
using DynamoDBTest.Controllers;
using DynamoDBTest.Helpers;
using DynamoDBTest.Services;

namespace DynamoDBTest.Droid
{

    [Activity(Label = "POIApplication")]
    public class SendEmailForPasswordResetActivity : BaseActivity
    {
        TextView reset;
        protected override int LayoutResource => Resource.Layout.activity_sendEmailForPassReset;

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
            reset = FindViewById<TextView>(Resource.Id.confirm);
            EditText sendEmail = FindViewById<EditText>(Resource.Id.sendEmail);

            reset.Click += (object sender, EventArgs e) =>
            {
                reset.Enabled = false;
                string email = sendEmail.Text.ToLower().Trim();
                Validator validator = new Validator(email);
                if (validator.ValidateE())
                {
                    //Controller controlla se l'user esiste e in caso manda un'email per pass reset
                    ResetPassController controller = new ResetPassController();
                    controller.CheckTerminated += new ResetPassController.CheckTerminatedHandler(OnCheckTerminated);
                    controller.SendEmailCommand.Execute(email);

                    var newIntent = new Intent(this, typeof(ConfirmPassResetActivity));
                    newIntent.PutExtra("email", email);
                    reset.Enabled = true;
                    StartActivity(newIntent);
                    OverridePendingTransition(Resource.Animator.EnterRightToLeft, Resource.Animator.ExitRightToLeft);
                    Finish();
                }
                else
                {
                    foreach (string message in validator.ErrorMessages)
                    {
                        Android.Widget.Toast.MakeText(this, message, Android.Widget.ToastLength.Short).Show();
                    }
                   reset.Enabled = true;
                }
            };
        }

        private void OnCheckTerminated(ResetPassController controller, bool checkSuccessful, string message, string email)
        {
            if (!checkSuccessful)
            {
                Android.Widget.Toast.MakeText(this, message, Android.Widget.ToastLength.Short).Show();
                reset.Enabled = true;
            }
        }



        public override bool OnKeyDown(Keycode keycode, KeyEvent e)
        {
            if (keycode == Keycode.Back)
            {
                Finish();
                OverridePendingTransition(Resource.Animator.Still, Resource.Animator.ExitTopToBottom);
                return true;
            }
            return base.OnKeyDown(keycode, e);
        }


        public override void OnBackPressed()
        {
            Finish();
            OverridePendingTransition(Resource.Animator.Still, Resource.Animator.ExitTopToBottom);
        }

        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            switch (item.ItemId)
            {
                case Android.Resource.Id.Home:
                    View view = CurrentFocus;
                    if (view != null)
                    {
                        InputMethodManager imm = (InputMethodManager)GetSystemService(InputMethodService);
                        imm.HideSoftInputFromWindow(view.WindowToken, 0);
                    }
                    Finish();
                    OverridePendingTransition(Resource.Animator.Still, Resource.Animator.ExitTopToBottom);
                    return true;

                default:
                    return base.OnOptionsItemSelected(item);
            }
        }
    }
}