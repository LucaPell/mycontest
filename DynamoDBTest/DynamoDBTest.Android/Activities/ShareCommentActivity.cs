﻿using Android.App;
using Android.Content;
using Android.OS;

using DynamoDBTest.Services;
using DynamoDBTest.ViewModels;
using DynamoDBTest.Models;

using System;
using Android.Views;
using Android.Views.InputMethods;

namespace DynamoDBTest.Droid
{
    [Activity(Label = "ShareCommentActivity")]
    public class ShareCommentActivity : BaseActivity
    {
        protected override int LayoutResource => Resource.Layout.activity_comments;

        ShareDetailViewModel viewModel;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            string shareId = Intent.GetStringExtra("shareId");

            Intent.PutExtra("itemId", shareId);
            Intent.PutExtra("type", ItemType.Share.ToString());

            base.OnCreate(savedInstanceState);

            Share share = GlobalCollections.GetInstance().GetShare(shareId);

            if (share != null)
            {
                viewModel = new ShareDetailViewModel(share);
                UpdateUI();
            }
            else
            {
                viewModel = new ShareDetailViewModel();
                viewModel.ItemLoaded += OnItemLoaded;
                viewModel.RetryLoadItemCommand.Execute(shareId);
            }
        }

        private void UpdateUI()
        {
            //SupportActionBar.Title = "Post di " + viewModel.share.CreatorUserName;
        }

        private void OnItemLoaded(object sender, EventArgs e)
        {
            UpdateUI();
        }

        public override bool OnKeyDown(Keycode keycode, KeyEvent e)
        {
            if (keycode == Keycode.Back)
            {
                Finish();
                OverridePendingTransition(Resource.Animator.Still, Resource.Animator.ExitTopToBottom);
                return true;
            }
            return base.OnKeyDown(keycode, e);
        }


        public override void OnBackPressed()
        {
            Finish();
            OverridePendingTransition(Resource.Animator.Still, Resource.Animator.ExitTopToBottom);
        }

        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            switch (item.ItemId)
            {
                case Android.Resource.Id.Home:
                    View view = CurrentFocus;
                    if (view != null)
                    {
                        InputMethodManager imm = (InputMethodManager)GetSystemService(InputMethodService);
                        imm.HideSoftInputFromWindow(view.WindowToken, 0);
                    }
                    Finish();
                    OverridePendingTransition(Resource.Animator.Still, Resource.Animator.ExitTopToBottom);
                    return true;

                default:
                    return base.OnOptionsItemSelected(item);
            }
        }
    }
}