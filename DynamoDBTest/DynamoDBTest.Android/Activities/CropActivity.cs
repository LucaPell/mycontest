﻿using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using Com.Theartofdev.Edmodo.Cropper;
using Uri = Android.Net.Uri;

namespace DynamoDBTest.Droid
{
    //[Register("App.Droid.CropImageView")]
    [Activity(Label = "CropActivity")]
    public class CropActivity : BaseActivity, CropImageView.IOnSetImageUriCompleteListener, CropImageView.IOnCropImageCompleteListener
    {
        protected override int LayoutResource => Resource.Layout.activity_crop;
        private string _Uri;
        CropImageView cropImageView;
        CropImageView cropImageViewB;
        CropImageView cropImageViewG;
        string shape;
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            shape = Intent.GetStringExtra("SHAPE");
            cropImageView = FindViewById<CropImageView>(Resource.Id.cropImageView);
            cropImageViewB = FindViewById<CropImageView>(Resource.Id.cropImageViewB);
            cropImageViewG = FindViewById<CropImageView>(Resource.Id.cropImageViewG);
            _Uri = Intent.GetStringExtra("URI");

            if (shape.Equals("CIRCLE"))
            {
                cropImageView.Visibility = ViewStates.Visible;
                cropImageView.SetOnSetImageUriCompleteListener(this);
                cropImageView.SetOnCropImageCompleteListener(this);
                cropImageView.SetImageUriAsync(Uri.Parse(_Uri));
            }
            else if (shape.Equals("BANNER") || shape.Equals("CONTEST"))
            {
                cropImageViewB.Visibility = ViewStates.Visible;
                cropImageViewB.SetOnSetImageUriCompleteListener(this);
                cropImageViewB.SetOnCropImageCompleteListener(this);
                cropImageViewB.SetImageUriAsync(Uri.Parse(_Uri));
                cropImageViewB.SetAspectRatio(Intent.GetIntExtra("WIDTH", 2000), Intent.GetIntExtra("HEIGHT", 348));
                //cropImageViewB.SetMinCropResultSize(Intent.GetIntExtra("WIDTH", 2000), Intent.GetIntExtra("HEIGHT", 348));
                //cropImageViewB.SetMaxCropResultSize(20000000, Intent.GetIntExtra("HEIGHT", 348));
            }
            else
            {
                cropImageViewG.Visibility = ViewStates.Visible;
                cropImageViewG.SetOnSetImageUriCompleteListener(this);
                cropImageViewG.SetOnCropImageCompleteListener(this);
                cropImageViewG.SetImageUriAsync(Uri.Parse(_Uri));
            }

            SupportActionBar.SetDisplayHomeAsUpEnabled(true);
            SupportActionBar.SetHomeButtonEnabled(true);
        }

        public override bool OnCreateOptionsMenu(IMenu menu)
        {
            MenuInflater inflater = MenuInflater;
            inflater.Inflate(Resource.Menu.crop_menu, menu);
            return true;
        }

        protected override void OnStart()
        {
            base.OnStart();
        }

        protected override void OnActivityResult(int requestCode, [GeneratedEnum] Result resultCode, Intent data)
        {
            base.OnActivityResult(requestCode, resultCode, data);

            if (requestCode == CropImage.CropImageActivityRequestCode)
            {
                CropImage.ActivityResult result = CropImage.GetActivityResult(data);
                HandleCropResult(result);
            }
        }

        public override bool OnOptionsItemSelected(IMenuItem item)
        {

            if (item.ItemId == Resource.Id.main_action_crop)
            {
                if (shape.Equals("CIRCLE"))
                {
                    cropImageView.GetCroppedImageAsync();
                    return true;
                }else if (shape.Equals("BANNER") || shape.Equals("CONTEST"))
                {
                    cropImageViewB.GetCroppedImageAsync();
                    return true;
                }
                else
                {
                    cropImageViewG.GetCroppedImageAsync();
                    return true;
                }
            }
            else if (item.ItemId == Resource.Id.main_action_rotate)
            {
                if (shape.Equals("CIRCLE"))
                {
                    cropImageView.RotateImage(90);
                    return true;
                }
                else if (shape.Equals("BANNER")|| shape.Equals("CONTEST"))
                {
                    cropImageViewB.RotateImage(90);
                    return true;
                }
                else
                {
                    cropImageViewG.RotateImage(90);
                    return true;
                }
            } 
            return base.OnOptionsItemSelected(item);
        }
           

   

        public void OnSetImageUriComplete(CropImageView p0, Uri p1, Java.Lang.Exception error)
        {
            if (error != null)
            {
                Log.Error("AIC", "Failed to load image by URI", error);
                Toast.MakeText(this, "C'è un problema con l'immagine: " + error.Message, ToastLength.Long).Show();
            }
        }

        public void OnCropImageComplete(CropImageView p0, CropImageView.CropResult result)
        {
            HandleCropResult(result);
        }

        private void HandleCropResult(CropImageView.CropResult result)
        {
            if (result.Error == null)
            {
                Intent intent = new Intent(this, typeof(NewPictureActivity));
                if (result.Uri != null)
                {
                    intent.PutExtra("ORIGINALURI", _Uri);
                    intent.PutExtra("RESULTURI", result.Uri);
                    intent.PutExtra("SHAPE", shape);
                    if (shape.Equals("OTHER")|| shape.Equals("CONTEST"))
                    {
                        intent.PutExtra("WIDTH", Intent.GetIntExtra("WIDTH", 2000));
                        intent.PutExtra("HEIGHT", Intent.GetIntExtra("HEIGHT", 348));
                        intent.AddFlags(ActivityFlags.ForwardResult);
                    }
                    StartActivity(intent);
                    Finish();
                }
                else
                {
                    MediaHelper.GetInstance().SetResult(result.Bitmap);
                    intent.PutExtra("ORIGINALURI", _Uri);
                    intent.PutExtra("SHAPE", shape);
                    if (shape.Equals("OTHER") || shape.Equals("CONTEST"))
                    {
                        intent.AddFlags(ActivityFlags.ForwardResult);
                    }
                    StartActivity(intent);
                    Finish();
                }
            }
            else
            {
                Log.Error("AIC", "Failed to crop image", result.Error);
                Toast.MakeText(this, "Image crop failed: " + result.Error.Message, ToastLength.Long).Show();
            }
        }

    }
}