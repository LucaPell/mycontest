﻿using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.OS;
using Android.Views;
using Android.Widget;
using Android.Support.V4.App;
using Android.Support.V4.View;
using Android.Support.Design.Widget;
using DynamoDBTest.Controllers;
using Refractored.Controls;
using System.Threading.Tasks;
using Android.Graphics;
using DynamoDBTest.Services;
using Com.Bumptech.Glide;
using System;
using Android.Views.Animations;
using Android.Support.V7.Widget;
using Android.Animation;
using Com.Bumptech.Glide.Request;
using Com.Bumptech.Glide.Request.Target;

namespace DynamoDBTest.Droid
{
	[Activity(Label = "@string/app_name", Icon = "@mipmap/icon",		
		ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation,
		ScreenOrientation = ScreenOrientation.Portrait)]
	public class MainActivity : BaseActivity
	{
		protected override int LayoutResource => Resource.Layout.activity_main;

		ViewPager pager;
		TabsAdapter adapter;
        CircleImageView avatar;
        TextView ToolbarTitle;
        AppCompatImageView AddContest;
        public AlphaAnimation inAnimation;
        public AlphaAnimation outAnimation;
        public FrameLayout ProgressB;

        protected override void OnCreate(Bundle savedInstanceState)
		{
			base.OnCreate(savedInstanceState);
            ToolbarTitle = FindViewById<TextView>(Resource.Id.title);
            ToolbarTitle.Text = "Home";
            adapter = new TabsAdapter(this, SupportFragmentManager);
			pager = FindViewById<ViewPager>(Resource.Id.viewpager);
			var tabs = FindViewById<TabLayout>(Resource.Id.tabs);
            tabs.AddOnTabSelectedListener(adapter);
			pager.Adapter = adapter;
			tabs.SetupWithViewPager(pager);
            tabs.GetTabAt(0).SetIcon(Resource.Drawable.home_tab);
            tabs.GetTabAt(1).SetIcon(Resource.Drawable.gpart_tab);
            tabs.GetTabAt(2).SetIcon(Resource.Drawable.gcontest_tab);
            tabs.GetTabAt(3).SetIcon(Resource.Drawable.mail_tab);
            tabs.GetTabAt(4).SetIcon(Resource.Drawable.bell_tab);
            AddContest = FindViewById<AppCompatImageView>(Resource.Id.AddContest);
            ProgressB = FindViewById<FrameLayout>(Resource.Id.progress);
            inAnimation = new AlphaAnimation(0f, 1f);
            inAnimation.Duration = 200;
            outAnimation = new AlphaAnimation(1f, 0f);
            outAnimation.Duration = 200;

            pager.OffscreenPageLimit = 5;
            avatar = FindViewById<CircleImageView>(Resource.Id.profile_image);
            string avatarUrl = LoginController.GetInstance().CurrentUser.Avatar;
            if (avatarUrl != null)
            {
                Glide.With(this).Load(avatarUrl).Into(avatar);
            }


            pager.PageSelected += (sender, args) =>
			{
                adapter.currentposition = args.Position;
                if(args.Position == 0)
                {
                    ToolbarTitle.Text = "Home";
                }
                else if(args.Position == 1)
                {
                    ToolbarTitle.Text = "Partecipazioni Globali";
                }
                else if (args.Position == 2)
                {
                    ToolbarTitle.Text = "Contest Globali";
                }
                else if (args.Position == 3)
                {
                    ToolbarTitle.Text = "Messaggi";
                }
                else if (args.Position == 4)
                {
                    ToolbarTitle.Text = "Notifiche";
                }
                var fragment = adapter.InstantiateItem(pager, args.Position) as IFragmentVisible;
				fragment?.BecameVisible();
            };

            AddContest.Click += (sender, e) =>
            {
                MediaHelper.GetInstance().SetBanner(null);
                var intent = new Intent(this, typeof(AddContestActivity));
                StartActivity(intent);
            };

            avatar.Click += (sender, e) =>
            {
                var intent = new Intent(this, typeof(PersonalUserPageActivity));
                MediaHelper.GetInstance().SetBanner(null);
                MediaHelper.GetInstance().SetAvatar(null);
                StartActivity(intent);
            };


			SupportActionBar.SetDisplayHomeAsUpEnabled(false);
			SupportActionBar.SetHomeButtonEnabled(false);

		}

        public override bool OnKeyDown(Keycode keycode, KeyEvent e)
        {
            if (keycode == Keycode.Back)
            {
                this.Finish();
                return true;
            }
            return base.OnKeyDown(keycode, e);
        }

        protected override void OnResume()
        {
            string avatarUrl = LoginController.GetInstance().CurrentUser.Avatar;
            if (avatarUrl != null)
            {
                Glide.With(this).Load(avatarUrl).Into(avatar);
            }
            else
            {
                avatar.SetImageResource(Resource.Color.imageBlank);
            }
            base.OnResume();
        }
    }

	class TabsAdapter : FragmentStatePagerAdapter, TabLayout.IOnTabSelectedListener
    {
        public override int Count => 5;
        public int currentposition = 0;
        HomeFragment home;
        GlobalPartecipationFragment globalpartecipation;
        GlobalContestFragment globalcontest;
        MessagesFragment messages;
        NotificationsFragment notifications;

        public TabsAdapter(Context context, Android.Support.V4.App.FragmentManager fm) : base(fm)
		{
		}

        public override Android.Support.V4.App.Fragment GetItem(int position)
        {
            if (position == 0) { home = HomeFragment.NewInstance(); return home; }
            else if (position == 1) { globalpartecipation = GlobalPartecipationFragment.NewInstance(); return globalpartecipation; }
            else if (position == 2) { globalcontest = GlobalContestFragment.NewInstance(); return globalcontest; }
            else if (position == 3) { messages = MessagesFragment.NewInstance(); return messages; }
            else if (position == 4) { notifications =  NotificationsFragment.NewInstance(); return notifications; }
            else{ return null; }
        }

        public override int GetItemPosition(Java.Lang.Object frag) => PositionNone;

        public void OnTabReselected(TabLayout.Tab tab)
        {
            if (currentposition == 0)
            {
                if (home!= null)
                {
                    home.recyclerView.SmoothScrollToPosition(0);
                }
            }else if (currentposition == 1)
            {
                if (globalpartecipation!= null)
                {
                    globalpartecipation.recyclerView.SmoothScrollToPosition(0);
                }
            }
            else if (currentposition == 2)
            {
                if (globalcontest!= null)
                {
                    globalcontest.recyclerView.SmoothScrollToPosition(0);
                }
            }
        }
        public void OnTabSelected(TabLayout.Tab tab)
        {
        }

        public void OnTabUnselected(TabLayout.Tab tab)
        {
        }
    }
}
