﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.Graphics;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Views.InputMethods;
using Android.Widget;
using Com.Bumptech.Glide;
using Com.Bumptech.Glide.Request;
using Com.Bumptech.Glide.Request.Target;
using ImageViews.Photo;

namespace DynamoDBTest.Droid
{
    [Activity(Label = "ZoomActivity")]
    public class ZoomActivity : BaseActivity
    {
        protected override int LayoutResource => Resource.Layout.activity_zoom;

        PhotoView ZImage;
        private string imageUrl;


        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            ZImage = FindViewById<PhotoView>(Resource.Id.zoomimg);
            imageUrl = Intent.GetStringExtra("Image");

            if (Build.VERSION.SdkInt >= BuildVersionCodes.Lollipop)
            {
                Window window = Window;
                window.SetStatusBarColor(Color.Black);
            }

            if (imageUrl != null)
            {
                RequestOptions options = new RequestOptions().Override(Target.SizeOriginal, Target.SizeOriginal);
                Glide.With(this).AsBitmap().Load(imageUrl).Apply(options).Into(ZImage);
            }
        }

        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            switch (item.ItemId)
            {
                case Android.Resource.Id.Home:
                    Finish();
                    OverridePendingTransition(Resource.Animator.Still, Resource.Animation.abc_fade_out);
                    return true;

                default:
                    return base.OnOptionsItemSelected(item);
            }
        }

        public override bool OnKeyDown([GeneratedEnum] Keycode keyCode, KeyEvent e)
        {
            if (keyCode == Keycode.Back)
            {
                Finish();
                OverridePendingTransition(Resource.Animator.Still, Resource.Animation.abc_fade_out);
                return true;
            }
            return base.OnKeyDown(keyCode, e);
        }


        public override void OnBackPressed()
        {
            Finish();
            OverridePendingTransition(Resource.Animator.Still, Resource.Animation.abc_fade_out);
        }

    }

}