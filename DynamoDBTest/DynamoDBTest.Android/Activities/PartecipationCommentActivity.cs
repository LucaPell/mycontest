﻿using Android.App;
using Android.Content;
using Android.OS;

using DynamoDBTest.Services;
using DynamoDBTest.ViewModels;
using DynamoDBTest.Models;

using System;
using Android.Views;
using Android.Views.InputMethods;

namespace DynamoDBTest.Droid
{
    [Activity(Label = "PartecipationCommentActivity")]
    public class PartecipationCommentActivity : BaseActivity
    {
        protected override int LayoutResource => Resource.Layout.activity_comments;

        PartecipationDetailViewModel viewModel;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            string partecipationId = Intent.GetStringExtra("partecipationId");

            Intent.PutExtra("itemId", partecipationId);
            Intent.PutExtra("type", ItemType.Partecipation.ToString());

            base.OnCreate(savedInstanceState);

            Partecipation partecipation = GlobalCollections.GetInstance().GetPartecipation(partecipationId);

            if(partecipation != null)
            {
                viewModel = new PartecipationDetailViewModel(partecipation);
                UpdateUI();
            }
            else
            {
                viewModel = new PartecipationDetailViewModel();
                viewModel.ItemLoaded += OnItemLoaded;
                viewModel.RetryLoadItemCommand.Execute(partecipationId);
            }
        }

        private void UpdateUI()
        {
        }

        private void OnItemLoaded(object sender, EventArgs e)
        {
            UpdateUI();
        }

        public override bool OnKeyDown(Keycode keycode, KeyEvent e)
        {
            if (keycode == Keycode.Back)
            {
                Finish();
                OverridePendingTransition(Resource.Animator.Still, Resource.Animator.ExitTopToBottom);
                return true;
            }
            return base.OnKeyDown(keycode, e);
        }


        public override void OnBackPressed()
        {
            Finish();
            OverridePendingTransition(Resource.Animator.Still, Resource.Animator.ExitTopToBottom);
        }

        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            switch (item.ItemId)
            {
                case Android.Resource.Id.Home:
                    View view = CurrentFocus;
                    if (view != null)
                    {
                        InputMethodManager imm = (InputMethodManager)GetSystemService(InputMethodService);
                        imm.HideSoftInputFromWindow(view.WindowToken, 0);
                    }
                    Finish();
                    OverridePendingTransition(Resource.Animator.Still, Resource.Animator.ExitTopToBottom);
                    return true;

                default:
                    return base.OnOptionsItemSelected(item);
            }
        }
    }
}
