﻿using System;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Views.InputMethods;
using Android.Widget;
using DynamoDBTest.Controllers;
using DynamoDBTest.Models;

namespace DynamoDBTest.Droid
{
    [Activity(Label = "POIApplication")]
    public class ConfirmCodeActivity : BaseActivity
    {
        TextView confirm;
        TextView sendNewEmail;
        protected override int LayoutResource => Resource.Layout.activity_confirm_code;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);           
            confirm = FindViewById<TextView>(Resource.Id.confirm);
            sendNewEmail = FindViewById<TextView>(Resource.Id.resend);
            EditText code = FindViewById<EditText>(Resource.Id.confirmCode);
            String email = Intent.GetStringExtra("email");
            TextView message = FindViewById<TextView>(Resource.Id.confirmCodeEmail);
            message.Append(email);            

            confirm.Click += (object sender, EventArgs e) =>
            {
                confirm.Enabled = false;
                ConfirmController controller = new ConfirmController();
                controller.ConfirmationTerminated += new ConfirmController.ConfirmationTerminatedHandler(OnConfirmationTerminated);
                controller.CheckCodeCommand.Execute(code.Text);
            };

            sendNewEmail.Click += (object sender, EventArgs e) =>
            {
                sendNewEmail.Enabled = false;
                ConfirmController controller = new ConfirmController();
                controller.SendNewCodeCommand.Execute(null);
                Android.Widget.Toast.MakeText(this, "È stata inviata una nuova e-mail con un nuovo codice", Android.Widget.ToastLength.Long).Show();
                sendNewEmail.Enabled = true;
            };
        }

        private void OnConfirmationTerminated(ConfirmController controller, bool confirmationSuccessfull, string message)
        {
            Android.Widget.Toast.MakeText(this, message, Android.Widget.ToastLength.Long).Show();
            if (confirmationSuccessfull)
            {
                confirm.Enabled = true;
                Intent newIntent = new Intent(this, typeof(MainActivity));
                StartActivity(newIntent);
                this.Finish();
            } else
            {
                confirm.Enabled = true;
            }
        }

        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            switch (item.ItemId)
            {
                case Android.Resource.Id.Home:
                    View view = CurrentFocus;
                    if (view != null)
                    {
                        InputMethodManager imm = (InputMethodManager)GetSystemService(Context.InputMethodService);
                        imm.HideSoftInputFromWindow(view.WindowToken, 0);
                    }
                    Finish();
                    OverridePendingTransition(Resource.Animator.EnterLeftToRight, Resource.Animator.ExitLeftToRight);
                    return true;

                default:
                    return base.OnOptionsItemSelected(item);
            }
        }

        public override bool OnKeyDown([GeneratedEnum] Keycode keyCode, KeyEvent e)
        { 
            if (keyCode == Keycode.Back)
            {
                Finish();
                OverridePendingTransition(Resource.Animator.EnterLeftToRight, Resource.Animator.ExitLeftToRight);
                return true;
            }
            return base.OnKeyDown(keyCode, e);
        }


        public override void OnBackPressed()
        {
            Finish();
            OverridePendingTransition(Resource.Animator.EnterLeftToRight, Resource.Animator.ExitLeftToRight);
        }
    }
}