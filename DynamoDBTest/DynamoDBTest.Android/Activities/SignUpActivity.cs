﻿using System;
using System.Collections.Generic;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Views;
using Android.Widget;
using Android.Views.Animations;

using DynamoDBTest.Helpers;
using DynamoDBTest.Controllers;
using Android.Runtime;
using Android.Views.InputMethods;

namespace DynamoDBTest.Droid
{
    [Activity(Label = "POIApplication")]
    public class SignUpActivity : BaseActivity
    {
        TextView signup;
        protected override int LayoutResource => Resource.Layout.activity_signup;

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
            Window.SetSoftInputMode(SoftInput.AdjustPan);
            signup = FindViewById<TextView>(Resource.Id.signup);
            EditText username = FindViewById<EditText>(Resource.Id.signupUsername);
            EditText email = FindViewById<EditText>(Resource.Id.signupEmail);
            EditText password = FindViewById<EditText>(Resource.Id.signupPassword);
            EditText passwordConfirm = FindViewById<EditText>(Resource.Id.signupPasswordConfirm);

            signup.Click += (object sender, EventArgs e) => {
                signup.Enabled = false;
                string emailR = email.Text.ToLower().Trim();
                Validator validator = new Validator(emailR, username.Text, password.Text, passwordConfirm.Text);
                if(validator.Validate())
                {
                    SignUpController auth = new SignUpController(emailR, username.Text, password.Text);
                    auth.RegistrationTerminated += new SignUpController.RegistrationTerminatedHandler(OnRegistrationTerminated);
                    auth.RetryRegisterCommand.OperationCompleted += OnRegistrationError;
                    auth.RetryRegisterCommand.Execute(auth.CurrentUser);
                    var newIntent = new Intent(this, typeof(ConfirmCodeActivity));
                    newIntent.PutExtra("email", emailR);
                    signup.Enabled = true;
                    StartActivity(newIntent);
                    OverridePendingTransition(Resource.Animator.EnterRightToLeft, Resource.Animator.ExitRightToLeft);
                    Finish();
                }
                else
                {
                    foreach(string message in validator.ErrorMessages)
                    {
                        Android.Widget.Toast.MakeText(this, message, Android.Widget.ToastLength.Short).Show();
                    }
                    signup.Enabled = true;
                }
            };
        }

        private void OnRegistrationTerminated(SignUpController controller, bool registrationSuccessful, string message, string email)
        {
            if (!registrationSuccessful)
            {
                Toast.MakeText(this, message, ToastLength.Short).Show();
                signup.Enabled = true;
            }
        }



        private void OnRegistrationError(object sender, EventArgs a)
        {
            CommandCompletedEventArgs args = a as CommandCompletedEventArgs;
            if (args.Status == CommandStates.MaxRetryReached)
            {
                RunOnUiThread(() => {
                    Android.Widget.Toast.MakeText(this, "Connessione ad Internet assente", Android.Widget.ToastLength.Short).Show();
                    signup.Enabled = true;
                });
            }
        }

        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            switch (item.ItemId)
            {
                case Android.Resource.Id.Home:
                    View view = CurrentFocus;
                    if (view != null)
                    {
                        InputMethodManager imm = (InputMethodManager)GetSystemService(Context.InputMethodService);
                        imm.HideSoftInputFromWindow(view.WindowToken, 0);
                    }
                    Finish();
                    OverridePendingTransition(Resource.Animator.EnterLeftToRight, Resource.Animator.ExitLeftToRight);
                    return true;

                default:
                    return base.OnOptionsItemSelected(item);
            }
        }

        public override bool OnKeyDown([GeneratedEnum] Keycode keyCode, KeyEvent e)
        {
            if (keyCode == Keycode.Back)
            {
                Finish();
                OverridePendingTransition(Resource.Animator.EnterLeftToRight, Resource.Animator.ExitLeftToRight);
                return true;
            }
            return base.OnKeyDown(keyCode, e);
        }


        public override void OnBackPressed()
        {
            Finish();
            OverridePendingTransition(Resource.Animator.EnterLeftToRight, Resource.Animator.ExitLeftToRight);
        }
    }
}